//
//  AppDelegate.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/11/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseInstanceID
import UserNotifications
import FirebaseMessaging
import Fabric

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if #available(iOS 13.0, *) {
                window?.overrideUserInterfaceStyle = .light
            }
        //1 FireBase & Register Push Notification
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
        
        self.registerForRemoteNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        
        deleteAllRecords(strEntity:"ContactData") // 2 CoreData
        deleteAllRecords(strEntity:"AboutData")
        deleteAllRecords(strEntity:"TermsCondition")
        deleteAllRecords(strEntity:"DropDownData")
        deleteAllRecords(strEntity:"Dashboard")
        deleteAllRecords(strEntity:"SavingCalculator")
        deleteAllRecords(strEntity:"Queries")
        deleteAllRecords(strEntity:"TermsConditionReferCode")
        

        
        FTIndicator.setIndicatorStyle(UIBlurEffect.Style.dark)
        if DeviceType.IS_IPAD {
            mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        }else{
            mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        }
        
        if nsud.value(forKey: "WelcomeScreen") != nil {
            if (nsud.value(forKey: "WelcomeScreen") as! String == "Yes"){
                
                let obj_ISVC : DashBoardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
                
            }else{
                let obj_ISVC : WelcomeScreenVC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenVC") as! WelcomeScreenVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
                
            }
        }else{
            let obj_ISVC : WelcomeScreenVC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenVC") as! WelcomeScreenVC
            let navigationController = UINavigationController(rootViewController: obj_ISVC)
            rootViewController(nav: navigationController)
            
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if DeviceType.IS_IPAD {
            mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        }else{
            mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "GNGPL")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    class func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    
    //MARK: RegisterPushNotification
    
    func registerForRemoteNotifications()  {
        if #available(iOS 10, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                if granted == true
                {
                    print("Notification Allow")
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    Messaging.messaging().delegate = self
                }
                else
                {
                    print("Notification Don't Allow")
                }
            }
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            UIApplication.shared.applicationIconBadgeNumber = 1
        }
    }
}


// MARK: --------UNNotification/Messaging Delegate Method----------

extension AppDelegate : UNUserNotificationCenterDelegate , MessagingDelegate{
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        let state: UIApplication.State = UIApplication.shared.applicationState
        if state == .active {
                        let dict = (userInfo as AnyObject)as! NSDictionary
                        let aps = (dict.value(forKey: "aps")as! NSDictionary)
                        let alert = (aps.value(forKey: "alert")as! NSDictionary)
                        let strTitle = (alert.value(forKey: "title"))
                        let strbody = (alert.value(forKey: "body"))
            FTIndicator.showNotification(withTitle: strTitle as? String, message: strbody as? String)
        }
        else  {
            hendelNotidata(userInfo: userInfo as NSDictionary)
        }
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let dictApsData : NSDictionary = response.notification.request.content.userInfo as NSDictionary
        hendelNotidata(userInfo: dictApsData)
    }
    
    func hendelNotidata(userInfo :NSDictionary){
        let obj_ISVC : DashBoardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        obj_ISVC.dictNotificationData = userInfo.mutableCopy()as! NSMutableDictionary
        let navigationController = UINavigationController(rootViewController: obj_ISVC)
        rootViewController(nav: navigationController)
    }
    
    func rootViewController(nav : UINavigationController)  {
        self.window!.rootViewController = nav
        self.window!.makeKeyAndVisible()
        nav.setNavigationBarHidden(true, animated: true)
    }
    
    
    // MARK: ---------GET TOKEN Func----------
    // MARK: -
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            nsud.setValue(uuid, forKey: "GNGPL_DEVICE_ID")
            print("DEVICE_ID-------------------- : \(uuid)")
        }
        if let token = InstanceID.instanceID().token() {
            nsud.setValue("\(token)", forKey: "GNGPL_FCM_Token")
            nsud.synchronize()
            print("FCM_Token----------------- : \(String(describing: token))")
            
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        nsud.setValue("5437788560965635874098656heyfget87", forKey: "GNGPL_FCM_Token")
        nsud.setValue("AHDkh5445", forKey: "GNGPL_DEVICE_ID")
        nsud.synchronize()
    }
    
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        if let token = InstanceID.instanceID().token() {
            nsud.setValue("\(token)", forKey: "GNGPL_FCM_Token")
            nsud.synchronize()
        }
        
    }
}
