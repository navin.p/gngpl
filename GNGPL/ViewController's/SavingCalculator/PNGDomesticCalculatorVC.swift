//
//  SavingCalculatorVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/8/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class PNGDomesticCalculatorVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCalculator: UIButton!
    @IBOutlet weak var btnCurrentPrice: UIButton!
    @IBOutlet weak var lblCalculator: UILabel!
    @IBOutlet weak var lblCurrentPrice: UILabel!
    
    
    @IBOutlet weak var scrollVIew: UIScrollView!
    @IBOutlet var viewForCalculate: UIView!
    @IBOutlet weak var txtnoOfCylinder: ACFloatingTextfield!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnCalculate: UIButton!
    @IBOutlet weak var viewForResult: CardView!
    @IBOutlet weak var lblRupee_LPG_Subsidized: UILabel!
    @IBOutlet weak var lblRupee_LPG_NonSubsidized: UILabel!
    @IBOutlet weak var lblPercentage_LPG_Subsidized: UILabel!
    @IBOutlet weak var lblPercentage_NonSubsidized: UILabel!
    
    @IBOutlet var viewForCurrentPrice: UIView!
    @IBOutlet weak var tvList: UITableView!
    var aryCurrentPriceList = NSMutableArray()
    var dictSavingCalculatorData = NSMutableDictionary()
    
    var selectionType = 1
    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        
        buttonEnable_OnView(tag: 1, btn: btnReset)
        buttonEnable_OnView(tag: 1, btn: btnCalculate)
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
        setViewOnScroll()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        if(getSavingCalculatorDataFromLocal().count != 0){
            print(getSavingCalculatorDataFromLocal())
            dictSavingCalculatorData = NSMutableDictionary()
            dictSavingCalculatorData = (((getSavingCalculatorDataFromLocal().object(at: 0)as! NSDictionary).value(forKey: "data")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(dictSavingCalculatorData)
            
            self.setDataOnView(dictData: dictSavingCalculatorData)
            
        }else{
            call_SavingCalculator_API()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.endEditing(true)
        self.setViewOnScroll()
        self.viewForResult.isHidden = true
        lblRupee_LPG_Subsidized.text = "-"
        lblRupee_LPG_NonSubsidized.text = "-"
        lblPercentage_LPG_Subsidized.text = "-"
        lblPercentage_NonSubsidized.text = "-"
    }
    // MARK: - --------------Local Data Base
    // MARK: -
    func getSavingCalculatorDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "SavingCalculator", strkey: "savingCalculator")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "savingCalculator") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    
    func setViewOnScroll()  {
        viewForCalculate.removeFromSuperview()
        viewForCurrentPrice.removeFromSuperview()
        
        if (selectionType == 1 ){
            viewForCalculate.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewForCalculate.frame.height)
            scrollVIew.addSubview(viewForCalculate)
            lblCalculator.backgroundColor = hexStringToUIColor(hex: orangeColor)
            btnCalculator.setTitleColor(hexStringToUIColor(hex: orangeColor), for: .normal)
            lblCurrentPrice.backgroundColor = UIColor.groupTableViewBackground
            btnCurrentPrice.setTitleColor(UIColor.black, for: .normal)
            scrollVIew.contentSize = CGSize(width: self.view.frame.size.width, height: viewForCalculate.frame.height)
            
        }
        else{
            viewForCurrentPrice.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.scrollVIew.frame.height)
            scrollVIew.addSubview(viewForCurrentPrice)
            lblCurrentPrice.backgroundColor = hexStringToUIColor(hex: orangeColor)
            btnCurrentPrice.setTitleColor(hexStringToUIColor(hex: orangeColor), for: .normal)
            lblCalculator.backgroundColor = UIColor.groupTableViewBackground
            btnCalculator.setTitleColor(UIColor.black, for: .normal)
            scrollVIew.contentSize = CGSize(width: self.view.frame.size.width, height: scrollVIew.frame.height)
            
            self.reloadWithAnimation()
        }
        
    }
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    
    @IBAction func actionOnButtons(_ sender: UIButton) {
        sender.alpha = 0.4
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.0, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            if(sender.tag == 2){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "PNGCommercialCalculatorVC")as! PNGCommercialCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }else if(sender.tag == 3){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "CNGForSavingCalculatorVC")as! CNGForSavingCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }else if(sender.tag == 4){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "IndustrialSavingCalculatorVC")as! IndustrialSavingCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }
            
            
        }
        
    }
    @IBAction func actionTopButtons(_ sender: UIButton) {
        selectionType = sender.tag // 1 == Calculator , 2 == Current Price
        self.setViewOnScroll()
    }
    @IBAction func actionOnCalCulator_Reset_Calculate(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if(sender.tag == 1){ // Reset
            clearView()
        }else{ // Calculate
            if(validation()){
                viewForResult.isHidden = true
                
                self.call_Calculate_API()
            }
        }
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_SavingCalculator_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("saving_cal_dropdowns", forKey: "request")
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            
            WebService.callAPIBYPOST(parameter: dictData, url: URL_SavingCalculator) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    print(responce)
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let arySaveData = NSMutableArray()
                        arySaveData.add(dict)
                        deleteAllRecords(strEntity:"SavingCalculator")
                        saveDataInLocalArray(strEntity: "SavingCalculator", strKey: "savingCalculator", data: arySaveData)
                        if(self.getSavingCalculatorDataFromLocal().count != 0){
                            print(self.getSavingCalculatorDataFromLocal())
                            self.dictSavingCalculatorData = NSMutableDictionary()
                            self.dictSavingCalculatorData = (((self.getSavingCalculatorDataFromLocal().object(at: 0)as! NSDictionary).value(forKey: "data")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            print(self.dictSavingCalculatorData)
                            self.setDataOnView(dictData: self.dictSavingCalculatorData)
                            
                        }
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    
    func call_Calculate_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            
            let dictData = NSMutableDictionary()
            dictData.setValue("saving_calculator", forKey: "request")
            dictData.setValue("1", forKey: "type")
            dictData.setValue(txtnoOfCylinder.text!, forKey: "no_of_cynilder")
            dictData.setValue("", forKey: "no_of_cynilder_two")
            dictData.setValue("", forKey: "no_of_cynilder_one")
            dictData.setValue("", forKey: "fule_id")
            dictData.setValue("", forKey: "quantity_used_per_day")
            dictData.setValue("", forKey: "cost_of_fuel")
            
            // for edit
            var current_price_LPG = "0"
            var current_price_PNG = "0"
            if(aryCurrentPriceList.count >= 2){
                current_price_LPG = "\((aryCurrentPriceList.object(at: 0) as! NSDictionary).value(forKey: "price") ?? "0")"
                current_price_PNG = "\((aryCurrentPriceList.object(at: 1) as! NSDictionary).value(forKey: "price") ?? "0")"
            }
            dictData.setValue("\(current_price_LPG)", forKey: "current_price_LPG")
            dictData.setValue("\(current_price_PNG)", forKey: "current_price_PNG")
            
            dotLoader(sender: btnCalculate , controller : self, viewContain: viewForCalculate, onView: "Button")
            
            WebService.callAPIBYPOST(parameter: dictData, url: URL_saving_calculator_Calculate_New) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    print(responce)
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        self.viewForResult.isHidden = false
                        let dictResponceValue  = (dict.value(forKey: "data")as! NSDictionary)
                        self.lblRupee_LPG_Subsidized.text = "\(dictResponceValue.value(forKey: "totalCylinderCost")!)"
                        self.lblRupee_LPG_NonSubsidized.text = "\(dictResponceValue.value(forKey: "totalPNGCost")!)"
                        self.lblPercentage_NonSubsidized.text = "\(dictResponceValue.value(forKey: "savingPercentage")!)%"
                        //self.lblPercentage_LPG_Subsidized.text = "\(dictResponceValue.value(forKey: "savingPercentage")!)%"
                        self.lblPercentage_LPG_Subsidized.text = "-"
                        
                        if( self.lblPercentage_LPG_Subsidized.text == "%"){
                            self.lblPercentage_LPG_Subsidized.text = ""
                        }
                        if( self.lblPercentage_NonSubsidized.text == "%"){
                            self.lblPercentage_NonSubsidized.text = ""
                        }
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    //MARK:  - ---------------Clear View
    // MARK: -
    func clearView()  {
        txtnoOfCylinder.text = ""
        lblRupee_LPG_Subsidized.text = "-"
        lblRupee_LPG_NonSubsidized.text = "-"
        lblPercentage_LPG_Subsidized.text = "-"
        lblPercentage_NonSubsidized.text = "-"
        
        self.viewForResult.isHidden = true
        
    }
    
    
    // MARK: - ---------------Data Set On View
    // MARK: -
    func setDataOnView(dictData : NSDictionary)  {
        print(dictData)
        if(dictData.count != 0){
            aryCurrentPriceList = NSMutableArray()
            aryCurrentPriceList = (dictData.value(forKey: "png_domestic")as! NSArray).mutableCopy() as! NSMutableArray
            reloadWithAnimation()
        }
        
        
    }
    // MARK: - --------------Validation
    // MARK: -
    func validation()-> Bool{
        self.view.endEditing(true)
        
        if txtnoOfCylinder.text?.count == 0 {
            
            txtnoOfCylinder.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtnoOfCylinder.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            
            return false
        }
        return true
    }
    
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PNGDomesticCalculatorVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryCurrentPriceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "PNGDemesticCurrentPriceCell", for: indexPath as IndexPath) as! PNGDemesticCurrentPriceCell
        let dict = (aryCurrentPriceList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary
        cell.lblTitle.text = "\(dict.value(forKey: "name")!)"
        cell.lblAmount.text = "\u{20B9} \(dict.value(forKey: "price")!)"
        cell.txtAmount.text = "\(dict.value(forKey: "price")!)"
        cell.txtAmount.tag = indexPath.row + 1
        cell.lblAmount.backgroundColor = hexStringToUIColor(hex: primaryColor)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func reloadWithAnimation() {
        tvList.reloadData()
        let tableViewHeight = tvList.bounds.size.height
        let cells = tvList.visibleCells
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.0, delay: 0.05 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
}


// MARK: - ----------------UserDashBoardCell
// MARK: -
class PNGDemesticCurrentPriceCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var txtAmount: ACFloatingTextfield!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension PNGDomesticCalculatorVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as! ACFloatingTextfield).errorLineColor = UIColor.lightGray
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 4)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag != 0{
            let obj = (aryCurrentPriceList.object(at: textField.tag - 1) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            var strNumber  = textField.text?.replacingOccurrences(of: " " , with: "")
            strNumber = strNumber!.replacingOccurrences(of: "" , with: "0")
            obj.setValue("\(strNumber ?? "0")", forKey: "price")
            aryCurrentPriceList.replaceObject(at: textField.tag - 1, with: obj)
            self.tvList.reloadData()
        }
    }
}
