//
//  IndustrialSavingCalculatorVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/14/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData


//MARK:
//MARK: ---------------Protocol-----------------

protocol refreshIndustrialSavingCalculatorr : class{
    func refreshviewIndustrialSavingCalculator(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ----------RefreshCng_Png_Com_Ind_View Delegate Methods----------

extension IndustrialSavingCalculatorVC: refreshIndustrialSavingCalculatorr {
    func refreshviewIndustrialSavingCalculator(dictData: NSDictionary, tag: Int)
    {
        if tag == 51 {
            txtFualUsed.text = (dictData.value(forKey: "name")as! String)
            txtFualUsed.tag = Int("\(dictData.value(forKey: "id")!)")!
        }else if tag == 52 {
            txtQuantityUsedPerDay.text = (dictData.value(forKey: "name")as! String)
            txtQuantityUsedPerDay.tag = Int("\(dictData.value(forKey: "id")!)")!
        }
    }
    
}

class IndustrialSavingCalculatorVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCalculator: UIButton!
    @IBOutlet weak var btnCurrentPrice: UIButton!
    @IBOutlet weak var lblCalculator: UILabel!
    @IBOutlet weak var lblCurrentPrice: UILabel!
    
    @IBOutlet weak var txtCostFualPerDay: ACFloatingTextfield!
    @IBOutlet weak var txtQuantityUsedPerDay: ACFloatingTextfield!

    
    @IBOutlet weak var txtFualUsed: ACFloatingTextfield!
    @IBOutlet weak var scrollVIew: UIScrollView!
    @IBOutlet var viewForCalculate: UIView!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnCalculate: UIButton!
    @IBOutlet weak var viewForResult: CardView!
    
    @IBOutlet weak var lblCostSavingPerday: UILabel!
    @IBOutlet weak var lblCostSavingPerAnnum: UILabel!

    
    @IBOutlet var viewForCurrentPrice: UIView!

    @IBOutlet weak var tvList: UITableView!
    var aryCurrentPriceList = NSMutableArray()
    var selectionType = 1
    var dictSavingCalculatorData = NSMutableDictionary()

    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        buttonEnable_OnView(tag: 1, btn: btnReset)
        buttonEnable_OnView(tag: 1, btn: btnCalculate)
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
        setViewOnScroll()
        self.viewForResult.isHidden = true
        if(getSavingCalculatorDataFromLocal().count != 0){
            print(getSavingCalculatorDataFromLocal())
            dictSavingCalculatorData = NSMutableDictionary()
            dictSavingCalculatorData = (((getSavingCalculatorDataFromLocal().object(at: 0)as! NSDictionary).value(forKey: "data")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(dictSavingCalculatorData)
            
            self.setDataOnView(dictData: dictSavingCalculatorData)
            
        }
    }
    // MARK: - --------------Local Data Base
    // MARK: -
    func getSavingCalculatorDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "SavingCalculator", strkey: "savingCalculator")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "savingCalculator") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    // MARK: - ---------------Data Set On View
    // MARK: -
    func setDataOnView(dictData : NSDictionary)  {
        print(dictData)
        if(dictData.count != 0){
            aryCurrentPriceList = NSMutableArray()
            aryCurrentPriceList = (dictData.value(forKey: "industrial_data")as! NSArray).mutableCopy() as! NSMutableArray
            reloadWithAnimation()
        }
        
        
    }
    func setViewOnScroll()  {
        viewForCalculate.removeFromSuperview()
        viewForCurrentPrice.removeFromSuperview()
        if (selectionType == 1 ){
            viewForCalculate.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewForCalculate.frame.height )
            scrollVIew.addSubview(viewForCalculate)
            lblCalculator.backgroundColor = hexStringToUIColor(hex: orangeColor)
            btnCalculator.setTitleColor(hexStringToUIColor(hex: orangeColor), for: .normal)
            lblCurrentPrice.backgroundColor = UIColor.groupTableViewBackground
            btnCurrentPrice.setTitleColor(UIColor.black, for: .normal)
            scrollVIew.contentSize = CGSize(width: self.view.frame.size.width, height: viewForCalculate.frame.height)
        }
        else{
            viewForCurrentPrice.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.scrollVIew.frame.height)
           scrollVIew.addSubview(viewForCurrentPrice)
            lblCurrentPrice.backgroundColor = hexStringToUIColor(hex: orangeColor)
            btnCurrentPrice.setTitleColor(hexStringToUIColor(hex: orangeColor), for: .normal)
            lblCalculator.backgroundColor = UIColor.groupTableViewBackground
            btnCalculator.setTitleColor(UIColor.black, for: .normal)
            scrollVIew.contentSize = CGSize(width: self.view.frame.size.width, height: scrollVIew.frame.height)
            
            self.reloadWithAnimation()
        }
        
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func actionOnDropDownButtons(_ sender: UIButton) {
        var aryData = NSMutableArray()
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        if(sender.tag == 1){
            aryData = (dictSavingCalculatorData.value(forKey: "fuel_used")as! NSArray).mutableCopy()as! NSMutableArray
            vc.strTag = 51
        }else if(sender.tag == 2){
            aryData = (dictSavingCalculatorData.value(forKey: "quantity_used")as! NSArray).mutableCopy()as! NSMutableArray
            vc.strTag = 52
        }
        vc.strTitle = ""
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleIndustrialSavingCalculatorr = self
            vc.aryTBL = aryData
            self.present(vc, animated: true, completion: {})
        }
    }
    
    @IBAction func actionOnButtons(_ sender: UIButton) {
        sender.alpha = 0.4
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.0, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            if(sender.tag == 1){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "PNGDomesticCalculatorVC")as! PNGDomesticCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }else if(sender.tag == 2){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "PNGCommercialCalculatorVC")as! PNGCommercialCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }else if(sender.tag == 3){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "CNGForSavingCalculatorVC")as! CNGForSavingCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }
        }
    }
    @IBAction func actionTopButtons(_ sender: UIButton) {
        selectionType = sender.tag
        // 1 == Calculator , 2 == Current Price
        self.setViewOnScroll()
    }
    
    @IBAction func actionOnCalCulator_Reset_Calculate(_ sender: UIButton) {
        self.view.endEditing(true)
        if(sender.tag == 1){ // Reset
            clearView()
        }else{ // Calculate
            if(validation()){
                viewForResult.isHidden = true
                call_Calculate_API()
            }
        }
    }
    
    // MARK: - --------------Validation
    // MARK: -
    func validation()-> Bool{
        self.view.endEditing(true)
        if txtFualUsed.text?.count == 0 {
            txtFualUsed.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_FuleUsed, viewcontrol: self)
            return false
        }else if txtQuantityUsedPerDay.text?.count == 0 {
            txtQuantityUsedPerDay.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: (Alert_quantityused), viewcontrol: self)
            return false
        }else if txtCostFualPerDay.text?.count == 0 {
            txtCostFualPerDay.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: (Alert_fuelperday), viewcontrol: self)
            return false
        }
        return true
    }
    // MARK: - --------------API Call
    // MARK: -
    func call_Calculate_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            
            let dictData = NSMutableDictionary()
            dictData.setValue("saving_calculator", forKey: "request")
            dictData.setValue("4", forKey: "type")
            dictData.setValue("", forKey: "no_of_cynilder")
            dictData.setValue("", forKey: "no_of_cynilder_two")
            dictData.setValue("", forKey: "no_of_cynilder_one")
            dictData.setValue("\(txtFualUsed.tag)", forKey: "fule_id")
            dictData.setValue(txtQuantityUsedPerDay.text!, forKey: "quantity_used_per_day")
            dictData.setValue(txtCostFualPerDay.text!, forKey: "cost_of_fuel")
            
            var fuel_price = "0"
            if(aryCurrentPriceList.count >= 1){
                fuel_price = "\((aryCurrentPriceList.object(at: 0) as! NSDictionary).value(forKey: "price") ?? "0")"
            }
            dictData.setValue(fuel_price, forKey: "fuel_price")
         
            dotLoader(sender: btnCalculate , controller : self, viewContain: viewForCalculate, onView: "Button")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_saving_calculator_Calculate) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    print(responce)
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        self.viewForResult.isHidden = false
                        let dictResponceValue  = (dict.value(forKey: "data")as! NSDictionary)
                        
                        self.lblCostSavingPerday.text = "\(dictResponceValue.value(forKey: "cost_saving_per_day")!)"
                        self.lblCostSavingPerAnnum.text = "\(dictResponceValue.value(forKey: "cost_saving_anual")!)"
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    //MARK:  - ---------------Clear View
    // MARK: -
    func clearView()  {
        lblCostSavingPerday.text = "-"
        lblCostSavingPerday.text = "-"
        
        txtFualUsed.text = ""
        txtFualUsed.tag = 0
        txtQuantityUsedPerDay.text = ""
        txtQuantityUsedPerDay.tag = 0
        viewForResult.isHidden = true
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension IndustrialSavingCalculatorVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryCurrentPriceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "IndustrialSavingCalculatorCell", for: indexPath as IndexPath) as! IndustrialSavingCalculatorCell
        let dict = (aryCurrentPriceList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary
        cell.lblTitle.text = "\(dict.value(forKey: "name")!)"
        cell.lblAmount.text = "\u{20B9} \(dict.value(forKey: "price") ?? "")"
        cell.txtAmount.text = "\(dict.value(forKey: "price") ?? "")"
        cell.txtAmount.tag = indexPath.row + 1
        cell.lblAmount.backgroundColor = hexStringToUIColor(hex: primaryColor)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func reloadWithAnimation() {
        tvList.reloadData()
        let tableViewHeight = tvList.bounds.size.height
        let cells = tvList.visibleCells
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.0, delay: 0.05 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
}

// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension IndustrialSavingCalculatorVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as! ACFloatingTextfield).errorLineColor = UIColor.lightGray
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtCostFualPerDay {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 4)

        }else if (textField == txtQuantityUsedPerDay){
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 6)

        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag != 0{
            let obj = (aryCurrentPriceList.object(at: textField.tag - 1) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            var strNumber  = textField.text?.replacingOccurrences(of: " " , with: "")
            strNumber = strNumber!.replacingOccurrences(of: "" , with: "0")
            obj.setValue("\(strNumber ?? "0")", forKey: "price")
            aryCurrentPriceList.replaceObject(at: textField.tag - 1, with: obj)
            self.tvList.reloadData()
        }
    }
}

// MARK: - ----------------UserDashBoardCell
// MARK: -
class IndustrialSavingCalculatorCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var txtAmount: ACFloatingTextfield!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
