//
//  CNGForSavingCalculatorVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/14/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData


//MARK:
//MARK: ---------------Protocol-----------------

protocol refreshCNGServiceCalculator : class{
    func refreshviewCNGServiceCalculator(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ----------RefreshCng_Png_Com_Ind_View Delegate Methods----------

extension CNGForSavingCalculatorVC: refreshCNGServiceCalculator {
    func refreshviewCNGServiceCalculator(dictData: NSDictionary, tag: Int)
    {
        if tag == 41 {
            txtCarModel.text = (dictData.value(forKey: "name")as! String)
            txtCarModel.tag = Int("\(dictData.value(forKey: "id")!)")!
        }else if tag == 42 {
            txtTypeOffual.text = (dictData.value(forKey: "name")as! String)
            txtTypeOffual.tag = Int("\(dictData.value(forKey: "id")!)")!
        
            txtTypeOffual.tag == 1 ? lblStaticRunningCostPerKmPetrolORDeasal.text = "Running Cost Per Km (Petrol)" : (lblStaticRunningCostPerKmPetrolORDeasal.text = "Running Cost Per Km (Diesel)")
            
            
        }else if tag == 43 {
            txtCurrentMileage.text = (dictData.value(forKey: "name")as! String)
            txtCurrentMileage.tag = Int("\(dictData.value(forKey: "id")!)")!
        }else if tag == 44 {
            txtDailyRunning.text = (dictData.value(forKey: "name")as! String)
            txtDailyRunning.tag = Int("\(dictData.value(forKey: "id")!)")!
        }

    }
    
}




class CNGForSavingCalculatorVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCalculator: UIButton!
    @IBOutlet weak var btnCurrentPrice: UIButton!
    @IBOutlet weak var lblCalculator: UILabel!
    @IBOutlet weak var lblCurrentPrice: UILabel!
    
    
    @IBOutlet weak var scrollVIew: UIScrollView!
    @IBOutlet var viewForCalculate: UIView!
    
    @IBOutlet weak var txtCarModel: ACFloatingTextfield!
    @IBOutlet weak var txtTypeOffual: ACFloatingTextfield!
    @IBOutlet weak var txtCurrentMileage: ACFloatingTextfield!
    @IBOutlet weak var txtDailyRunning: ACFloatingTextfield!
    
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnCalculate: UIButton!
    @IBOutlet weak var viewForResult: CardView!
    @IBOutlet weak var lblStaticRunningCostPerKmPetrolORDeasal: UILabel!

    @IBOutlet weak var lblRunningCostPerKmPetrol: UILabel!
    
    
    @IBOutlet weak var lblRunningCostPerKmCNG: UILabel!
    @IBOutlet weak var lblDailySavingONCNG: UILabel!
    @IBOutlet weak var lblMonthlySavingONCNG: UILabel!
    @IBOutlet weak var lblAnnualSavingONCNG: UILabel!

    @IBOutlet var viewForCurrentPrice: UIView!
    @IBOutlet weak var tvList: UITableView!
     var aryCurrentPriceList = NSMutableArray()
     var selectionType = 1
       var dictSavingCalculatorData = NSMutableDictionary()
    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        buttonEnable_OnView(tag: 1, btn: btnReset)
        buttonEnable_OnView(tag: 1, btn: btnCalculate)
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
        setViewOnScroll()
        self.viewForResult.isHidden = true
        if(getSavingCalculatorDataFromLocal().count != 0){
            print(getSavingCalculatorDataFromLocal())
            dictSavingCalculatorData = NSMutableDictionary()
            dictSavingCalculatorData = (((getSavingCalculatorDataFromLocal().object(at: 0)as! NSDictionary).value(forKey: "data")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(dictSavingCalculatorData)
            
            self.setDataOnView(dictData: dictSavingCalculatorData)
            
        }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.endEditing(true)
        self.setViewOnScroll()
    }
    //MARK:  - ---------------Clear View
    // MARK: -
    func clearView()  {
        lblRunningCostPerKmPetrol.text = ""
        lblRunningCostPerKmCNG.text = ""
        lblDailySavingONCNG.text = ""
        lblMonthlySavingONCNG.text = ""
        lblAnnualSavingONCNG.text = ""
        txtCarModel.text = ""
        txtCarModel.tag = 0
        txtTypeOffual.text = ""
        txtTypeOffual.tag = 0
        txtCurrentMileage.text = ""
        txtCurrentMileage.tag = 0
        txtDailyRunning.text = ""
        txtDailyRunning.tag = 0
        viewForResult.isHidden = true

        
    }
    //MARK:  - ---------------CALL API
    // MARK: -
    func call_Calculate_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            
            let dictData = NSMutableDictionary()
            dictData.setValue("saving_calculator", forKey: "request")
       
            dictData.setValue("3", forKey: "type")
            dictData.setValue("", forKey: "no_of_cynilder")
            dictData.setValue("", forKey: "no_of_cynilder_one")
            dictData.setValue("", forKey: "no_of_cynilder_two")
            dictData.setValue("", forKey: "fule_id")
            dictData.setValue("", forKey: "quantity_used_per_day")
            dictData.setValue("", forKey: "cost_of_fuel")
         
            dictData.setValue("\(txtCarModel.tag)", forKey: "car_model")
            dictData.setValue("\(txtTypeOffual.tag)", forKey: "type_of_fuel")
            dictData.setValue(txtCurrentMileage.text!, forKey: "current_mileage")
            dictData.setValue(txtDailyRunning.text!, forKey: "daily_running")

            var natural_gas_price = "0"
            var petrol_price = "0"
            var diesel_price = "0"
            if(aryCurrentPriceList.count >= 3){
                natural_gas_price = "\((aryCurrentPriceList.object(at: 0) as! NSDictionary).value(forKey: "price") ?? "0")"
                petrol_price = "\((aryCurrentPriceList.object(at: 1) as! NSDictionary).value(forKey: "price") ?? "0")"
                diesel_price = "\((aryCurrentPriceList.object(at: 2) as! NSDictionary).value(forKey: "price") ?? "0")"
            }
            dictData.setValue(natural_gas_price, forKey: "natural_gas_price")
            dictData.setValue(petrol_price, forKey: "petrol_price")
            dictData.setValue(diesel_price, forKey: "diesel_price")

            dotLoader(sender: btnCalculate , controller : self, viewContain: viewForCalculate, onView: "Button")
            
            WebService.callAPIBYPOST(parameter: dictData, url: URL_saving_calculator_Calculate) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    print(responce)
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        self.viewForResult.isHidden = false
                        let dictResponceValue  = (dict.value(forKey: "data")as! NSDictionary)
                        self.lblDailySavingONCNG.text = "\(dictResponceValue.value(forKey: "daily_saving_in_cng")!)"
                        self.lblAnnualSavingONCNG.text = "\(dictResponceValue.value(forKey: "anual_saving_in_cng")!)"
                        self.lblMonthlySavingONCNG.text = "\(dictResponceValue.value(forKey: "monthly_saving_in_cng")!)"
                        self.lblRunningCostPerKmCNG.text = "\(dictResponceValue.value(forKey: "running_cost_per_km_cng")!)"
                        
                        dictResponceValue["running_cost_per_km_petrol"] != nil ?   self.lblRunningCostPerKmPetrol.text = "\(dictResponceValue.value(forKey: "running_cost_per_km_petrol")!)" :   (self.lblRunningCostPerKmPetrol.text = "\(dictResponceValue.value(forKey: "running_cost_per_km_diesel")!)")
                        
                      
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    // MARK: - --------------Local Data Base
    // MARK: -
    func getSavingCalculatorDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "SavingCalculator", strkey: "savingCalculator")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "savingCalculator") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    // MARK: - ---------------Data Set On View
    // MARK: -
    func setDataOnView(dictData : NSDictionary)  {
        print(dictData)
        if(dictData.count != 0){
            aryCurrentPriceList = NSMutableArray()
            aryCurrentPriceList = (dictData.value(forKey: "cng")as! NSArray).mutableCopy() as! NSMutableArray
            reloadWithAnimation()
        }
        
        
    }
    func setViewOnScroll()  {
        viewForCalculate.removeFromSuperview()
        viewForCurrentPrice.removeFromSuperview()
        
        if (selectionType == 1 ){
            viewForCalculate.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewForCalculate.frame.height)
            scrollVIew.addSubview(viewForCalculate)
            lblCalculator.backgroundColor = hexStringToUIColor(hex: orangeColor)
            btnCalculator.setTitleColor(hexStringToUIColor(hex: orangeColor), for: .normal)
            lblCurrentPrice.backgroundColor = UIColor.groupTableViewBackground
            btnCurrentPrice.setTitleColor(UIColor.black, for: .normal)
            scrollVIew.contentSize = CGSize(width: self.view.frame.size.width, height: viewForCalculate.frame.height)
            
        }
        else{
            viewForCurrentPrice.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.scrollVIew.frame.height)
            scrollVIew.addSubview(viewForCurrentPrice)
            lblCurrentPrice.backgroundColor = hexStringToUIColor(hex: orangeColor)
            btnCurrentPrice.setTitleColor(hexStringToUIColor(hex: orangeColor), for: .normal)
            lblCalculator.backgroundColor = UIColor.groupTableViewBackground
            btnCalculator.setTitleColor(UIColor.black, for: .normal)
            scrollVIew.contentSize = CGSize(width: self.view.frame.size.width, height: scrollVIew.frame.height)
            
            self.reloadWithAnimation()
        }
        
    }
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    
    @IBAction func actionOnDropDownButtons(_ sender: UIButton) {
        var aryData = NSMutableArray()
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        if(dictSavingCalculatorData.count != 0){
            if(sender.tag == 1){
                aryData = (dictSavingCalculatorData.value(forKey: "car_model")as! NSArray).mutableCopy()as! NSMutableArray
                vc.strTag = 41
            }else if(sender.tag == 2){
                aryData = (dictSavingCalculatorData.value(forKey: "type_of_fuel")as! NSArray).mutableCopy()as! NSMutableArray
                vc.strTag = 42
            }else if(sender.tag == 3){
                aryData = (dictSavingCalculatorData.value(forKey: "current_mileage")as! NSArray).mutableCopy()as! NSMutableArray
                vc.strTag = 43
            }else if(sender.tag == 4){
                aryData = (dictSavingCalculatorData.value(forKey: "daily_running")as! NSArray).mutableCopy()as! NSMutableArray
                vc.strTag = 44
            }
            vc.strTitle = ""
            if aryData.count != 0{
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = .coverVertical
                vc.handleCNGSaving_View = self
                vc.aryTBL = aryData
                self.present(vc, animated: true, completion: {})
            }        }
      
    }
    
    @IBAction func actionOnButtons(_ sender: UIButton) {
        sender.alpha = 0.4
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.0, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            if(sender.tag == 1){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "PNGDomesticCalculatorVC")as! PNGDomesticCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }else if(sender.tag == 2){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "PNGCommercialCalculatorVC")as! PNGCommercialCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }else if(sender.tag == 4){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "IndustrialSavingCalculatorVC")as! IndustrialSavingCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }
            
            
        }
        
    }
    @IBAction func actionTopButtons(_ sender: UIButton) {
        selectionType = sender.tag // 1 == Calculator , 2 == Current Price
        self.setViewOnScroll()
    }
    @IBAction func actionOnCalCulator_Reset_Calculate(_ sender: UIButton) {
        self.view.endEditing(true)
         if(sender.tag == 1){ // Reset
              clearView()
        }else{ // Calculate
            if(validation()){
                viewForResult.isHidden = true

                self.call_Calculate_API()
            }
        }
    }
    
    // MARK: - --------------Validation
    // MARK: -
    func validation()-> Bool{
        self.view.endEditing(true)

        if txtCarModel.text?.count == 0 {
            txtCarModel.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtCarModel.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if txtTypeOffual.text?.count == 0 {
            txtTypeOffual.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtTypeOffual.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if txtCurrentMileage.text?.count == 0 {
            txtCurrentMileage.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtCurrentMileage.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            return false
        }else if txtDailyRunning.text?.count == 0 {
            txtDailyRunning.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtDailyRunning.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            return false
        }
        return true
    }
    
    
    
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension CNGForSavingCalculatorVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as! ACFloatingTextfield).errorLineColor = UIColor.lightGray
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtCurrentMileage {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 2)
            
        }else if (textField == txtDailyRunning){
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 4)
            
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag != 0{
            let obj = (aryCurrentPriceList.object(at: textField.tag - 1) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            var strNumber  = textField.text?.replacingOccurrences(of: " " , with: "")
            strNumber = strNumber!.replacingOccurrences(of: "" , with: "0")
            obj.setValue("\(strNumber ?? "0")", forKey: "price")
            aryCurrentPriceList.replaceObject(at: textField.tag - 1, with: obj)
            self.tvList.reloadData()
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension CNGForSavingCalculatorVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryCurrentPriceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "CNGForSavingCalculatorCell", for: indexPath as IndexPath) as! CNGForSavingCalculatorCell
        let dict = (aryCurrentPriceList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary
        cell.lblTitle.text = "\(dict.value(forKey: "name")!)"
        cell.lblAmount.text = "\u{20B9} \(dict.value(forKey: "price") ?? "")"
        cell.txtAmount.text = "\(dict.value(forKey: "price") ?? "")"
        cell.txtAmount.tag = indexPath.row + 1
        cell.lblAmount.backgroundColor = hexStringToUIColor(hex: primaryColor)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func reloadWithAnimation() {
        tvList.reloadData()
        let tableViewHeight = tvList.bounds.size.height
        let cells = tvList.visibleCells
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.0, delay: 0.05 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
}


// MARK: - ----------------UserDashBoardCell
// MARK: -
class CNGForSavingCalculatorCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var txtAmount: ACFloatingTextfield!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
