//
//  PNGCommercialCalculatorVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/10/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class PNGCommercialCalculatorVC: UIViewController {
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCalculator: UIButton!
    @IBOutlet weak var btnCurrentPrice: UIButton!
    @IBOutlet weak var lblCalculator: UILabel!
    @IBOutlet weak var lblCurrentPrice: UILabel!
    
    @IBOutlet weak var scrollVIew: UIScrollView!
    @IBOutlet var viewForCalculate: UIView!
    @IBOutlet weak var txtnoOfCylinder19kg: ACFloatingTextfield!
    @IBOutlet weak var txtnoOfCylinder47kg: ACFloatingTextfield!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnCalculate: UIButton!
    @IBOutlet weak var viewForResult: CardView!
    @IBOutlet weak var lblRupee_PerMonth: UILabel!
    @IBOutlet weak var lblRupee_PerAnnum: UILabel!
    @IBOutlet weak var lblPercentage_PerMonths: UILabel!
    @IBOutlet weak var lblPercentage_PerAnnum: UILabel!
    
    @IBOutlet var viewForCurrentPrice: UIView!
    @IBOutlet weak var tvList: UITableView!
    var aryCurrentPriceList = NSMutableArray()
    var dictSavingCalculatorData = NSMutableDictionary()

    var selectionType = 1
    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        buttonEnable_OnView(tag: 1, btn: btnReset)
        buttonEnable_OnView(tag: 1, btn: btnCalculate)
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
        setViewOnScroll()
        if(getSavingCalculatorDataFromLocal().count != 0){
            print(getSavingCalculatorDataFromLocal())
            dictSavingCalculatorData = NSMutableDictionary()
            dictSavingCalculatorData = (((getSavingCalculatorDataFromLocal().object(at: 0)as! NSDictionary).value(forKey: "data")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(dictSavingCalculatorData)
            
            self.setDataOnView(dictData: dictSavingCalculatorData)
            
        }
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.endEditing(true)
        self.setViewOnScroll()
        viewForResult.isHidden = true
    }
    // MARK: - --------------Local Data Base
    // MARK: -
    func getSavingCalculatorDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "SavingCalculator", strkey: "savingCalculator")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "savingCalculator") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    // MARK: - ---------------Data Set On View
    // MARK: -
    func setDataOnView(dictData : NSDictionary)  {
        print(dictData)
        if(dictData.count != 0){
            aryCurrentPriceList = NSMutableArray()
            aryCurrentPriceList = (dictData.value(forKey: "png_commercial")as! NSArray).mutableCopy() as! NSMutableArray
            reloadWithAnimation()
        }
        
        
    }
    func setViewOnScroll()  {
        viewForCalculate.removeFromSuperview()
        viewForCurrentPrice.removeFromSuperview()
        
        if (selectionType == 1 ){
            viewForCalculate.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewForCalculate.frame.height)
            scrollVIew.addSubview(viewForCalculate)
            lblCalculator.backgroundColor = hexStringToUIColor(hex: orangeColor)
            btnCalculator.setTitleColor(hexStringToUIColor(hex: orangeColor), for: .normal)
            lblCurrentPrice.backgroundColor = UIColor.groupTableViewBackground
            btnCurrentPrice.setTitleColor(UIColor.black, for: .normal)
            scrollVIew.contentSize = CGSize(width: self.view.frame.size.width, height: viewForCalculate.frame.height)
            
        }
        else{
            viewForCurrentPrice.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.scrollVIew.frame.height)
            scrollVIew.addSubview(viewForCurrentPrice)
            lblCurrentPrice.backgroundColor = hexStringToUIColor(hex: orangeColor)
            btnCurrentPrice.setTitleColor(hexStringToUIColor(hex: orangeColor), for: .normal)
            lblCalculator.backgroundColor = UIColor.groupTableViewBackground
            btnCalculator.setTitleColor(UIColor.black, for: .normal)
            scrollVIew.contentSize = CGSize(width: self.view.frame.size.width, height: scrollVIew.frame.height)
            
            self.reloadWithAnimation()
        }
        
    }
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    
    @IBAction func actionOnButtons(_ sender: UIButton) {
        sender.alpha = 0.4
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.0, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            if(sender.tag == 1){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "PNGDomesticCalculatorVC")as! PNGDomesticCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }else if(sender.tag == 3){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "CNGForSavingCalculatorVC")as! CNGForSavingCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }else if(sender.tag == 4){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "IndustrialSavingCalculatorVC")as! IndustrialSavingCalculatorVC
                self.navigationController?.pushViewController(testController, animated: false)
            }
        }
    }
    
    @IBAction func actionTopButtons(_ sender: UIButton) {
        selectionType = sender.tag // 1 == Calculator , 2 == Current Price
        self.setViewOnScroll()
    }
    //MARK:  - ---------------Clear View
    // MARK: -
    func clearView()  {
        txtnoOfCylinder19kg.text = ""
        txtnoOfCylinder47kg.text = ""
        viewForResult.isHidden = true
        lblRupee_PerAnnum.text = "-"
        lblRupee_PerMonth.text = "-"
        lblPercentage_PerMonths.text = "-"
        lblPercentage_PerAnnum.text = "-"
        
    }
    // MARK: - --------------Validation
    // MARK: -
    @IBAction func actionOnCalCulator_Reset_Calculate(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if(sender.tag == 1){ // Reset
          clearView()
        }else{ // Calculate
            if(validation()){
                viewForResult.isHidden = true

               call_Calculate_API()
            }
        }
    }
    func call_Calculate_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            
            let dictData = NSMutableDictionary()
            dictData.setValue("saving_calculator", forKey: "request")
            dictData.setValue("2", forKey: "type")
            dictData.setValue("", forKey: "no_of_cynilder")
            
            txtnoOfCylinder19kg.text! == "" ? dictData.setValue("0", forKey: "no_of_cynilder_one") : dictData.setValue(txtnoOfCylinder19kg.text!, forKey: "no_of_cynilder_one")
            
            txtnoOfCylinder47kg.text! == "" ? dictData.setValue("0", forKey: "no_of_cynilder_two") : dictData.setValue(txtnoOfCylinder47kg.text!, forKey: "no_of_cynilder_two")
       
            dictData.setValue("", forKey: "fule_id")
            dictData.setValue("", forKey: "quantity_used_per_day")
            dictData.setValue("", forKey: "cost_of_fuel")
            
            // for edit
            var cost_of_fourty_five_kg_cylinder = "0"
            var cost_of_nineteen_kg_cylinder = "0"
            if(aryCurrentPriceList.count >= 2){
               
                cost_of_nineteen_kg_cylinder = "\((aryCurrentPriceList.object(at: 0) as! NSDictionary).value(forKey: "price") ?? "0")"
                cost_of_fourty_five_kg_cylinder = "\((aryCurrentPriceList.object(at: 1) as! NSDictionary).value(forKey: "price") ?? "0")"
            }
            dictData.setValue("\(cost_of_fourty_five_kg_cylinder)", forKey: "cost_of_fourty_five_kg_cylinder")
            dictData.setValue("\(cost_of_nineteen_kg_cylinder)", forKey: "cost_of_nineteen_kg_cylinder")

            

            dotLoader(sender: btnCalculate , controller : self, viewContain: viewForCalculate, onView: "Button")
            
            WebService.callAPIBYPOST(parameter: dictData, url: URL_saving_calculator_Calculate) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    print(responce)
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        self.viewForResult.isHidden = false
                        let dictResponceValue  = (dict.value(forKey: "data")as! NSDictionary)
                        

                   self.lblRupee_PerAnnum.text = "\(dictResponceValue.value(forKey: "saving_per_annum")!)"
                     self.lblRupee_PerMonth.text = "\(dictResponceValue.value(forKey: "saving_per_month")!)"
                       self.lblPercentage_PerMonths.text = "\(dictResponceValue.value(forKey: "saving_per_month_percent")!)%"
                        self.lblPercentage_PerAnnum.text = "\(dictResponceValue.value(forKey: "saving_per_annum_percent")!)%"

                        if( self.lblPercentage_PerAnnum.text == "%"){
                            self.lblPercentage_PerAnnum.text = ""
                        }
                        if( self.lblPercentage_PerMonths.text == "%"){
                            self.lblPercentage_PerMonths.text = ""
                        }
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    // MARK: - --------------Validation
    // MARK: -
    func validation()-> Bool{
        self.view.endEditing(true)

        if txtnoOfCylinder19kg.text?.count == 0 && txtnoOfCylinder47kg.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtnoOfCylinder19kg.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
                return false
        }
        return true
    }
    
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PNGCommercialCalculatorVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryCurrentPriceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "PNGCommercialCurrentPriceCell", for: indexPath as IndexPath) as! PNGCommercialCurrentPriceCell
        let dict = (aryCurrentPriceList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary
        cell.lblTitle.text = "\(dict.value(forKey: "name")!)"
        cell.lblAmount.text = "\u{20B9} \(dict.value(forKey: "price")!)"
        cell.txtAmount.text = "\(dict.value(forKey: "price")!)"
        cell.txtAmount.tag = indexPath.row + 1
        
        cell.lblAmount.backgroundColor = hexStringToUIColor(hex: primaryColor)

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func reloadWithAnimation() {
        tvList.reloadData()
        let tableViewHeight = tvList.bounds.size.height
        let cells = tvList.visibleCells
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.0, delay: 0.05 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
}


// MARK: - ----------------UserDashBoardCell
// MARK: -
class PNGCommercialCurrentPriceCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var txtAmount: ACFloatingTextfield!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension PNGCommercialCalculatorVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as! ACFloatingTextfield).errorLineColor = UIColor.lightGray
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 4)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag != 0{
            let obj = (aryCurrentPriceList.object(at: textField.tag - 1) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            var strNumber = textField.text?.replacingOccurrences(of: " " , with: "")
            strNumber = strNumber!.replacingOccurrences(of: "" , with: "0")
            obj.setValue("\(strNumber ?? "0")", forKey: "price")
            aryCurrentPriceList.replaceObject(at: textField.tag - 1, with: obj)
            self.tvList.reloadData()
        }
    }
}
