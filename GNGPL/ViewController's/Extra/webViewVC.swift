//
//  webViewVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/18/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class webViewVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewWeb: UIWebView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var activityloader: UIActivityIndicatorView!
    @IBOutlet weak var lblDownload: UILabel!
    @IBOutlet weak var btnshare: UIButton!

    var dictBillData = NSMutableDictionary()
    var strViewComeFrom = String()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        activityloader.isHidden = true
        lblDownload.isHidden = true
        
        
        if(strViewComeFrom == "ViewBill"){
            if(self.dictBillData.count != 0){
                viewWeb.loadRequest(URLRequest(url: URL(string: "\(self.dictBillData.value(forKey: "bill_path")!)")!))
                lblTitle.text = "\(self.dictBillData.value(forKey: "bill_date")!)"
                viewWeb.scrollView.maximumZoomScale = 8
                viewWeb.scrollView.minimumZoomScale = 1
                viewWeb.delegate = self
                let bill_date = "\(self.dictBillData.value(forKey: "bill_date")!)"
                let fileNameToDelete = "\(bill_date.replacingOccurrences(of: " ", with: "")).pdf"
                var filePath = ""
                // Fine documents directory on device
                let dirs : [String] = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
                if dirs.count > 0 {
                    let dir = dirs[0] //documents directory
                    filePath = dir.appendingFormat("/" + fileNameToDelete)
                    print("Local path = \(filePath)")
                    do {
                        let fileManager = FileManager.default
                        // Check if file exists
                        if fileManager.fileExists(atPath: filePath) {
                            // Delete file
                            try fileManager.removeItem(atPath: filePath)
                            print("File Deleted")
                        } else {
                            print("File does not exist")
                        }
                    }
                    catch let error as NSError {
                        print("An error took place: \(error)")
                    }
                }
            }
        }
        
        else if (strViewComeFrom == "Complaint"){
            lblTitle.text = strViewComeFrom
            btnshare.isHidden = true
            let request = NSMutableURLRequest(url: URL(string: gngpl_complaint)!)
            viewWeb.loadRequest(request as URLRequest)
        }
        
        
        else if (strViewComeFrom == ""){
            
            let request = NSMutableURLRequest(url: URL(string: "http://www.goanaturalgas.com/sbi_epay/sbi_epay.php")!)
            request.timeoutInterval = 50.0
            
            request.setValue("fBc5628ybRQf88f/aqDUOQ==", forHTTPHeaderField: "key")
            request.setValue("fBc5628ybRQf88f/aqDUOQ==", forHTTPHeaderField: "key")


            
//            request.setValue("fBc5628ybRQf88f/aqDUOQ==", forKey: "key")
//            request.setValue("44", forKey: "amount")
//            request.setValue("123!qwet", forKey: "gngpl_id")
            viewWeb.loadRequest(request as URLRequest)
     
        }
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actiononShare(_ sender: UIButton) {
      
        let url = URL(string: "\(self.dictBillData.value(forKey: "bill_path")!)")
       

        do {
            let imageData = try Data(contentsOf: url!)
            do {
                
                if let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                    activityloader.isHidden = false
                    lblDownload.isHidden = false

                    sender.isHidden = true
                    let bill_date = "\(self.dictBillData.value(forKey: "bill_date")!)"

                    let fileURL = documentDirectory.appendingPathComponent("\(bill_date.replacingOccurrences(of: " ", with: "")).pdf")
                    try imageData.write(to: fileURL)

                    print("saving was successful")
                   // let pdfData = try Data(contentsOf: fileURL)
                    let activityVC = UIActivityViewController(activityItems: [fileURL], applicationActivities: nil)
                    activityVC.popoverPresentationController?.sourceView = self.view
                  
                  
              
                    activityloader.isHidden = true
                    lblDownload.isHidden = true
                    sender.isHidden = false

                    self.present(activityVC, animated: true, completion: nil)
                }
            } catch {
                print("error:", error)
            }
      
        } catch {
            print("Unable to load data: \(error)")
        }
  
    }
    
    
    
}
// MARK :
//MARK : UIWebViewDelegate
extension webViewVC : UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        remove_dotLoader(controller: self)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        remove_dotLoader(controller: self)
    }
}
