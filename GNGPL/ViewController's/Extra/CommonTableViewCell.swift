//
//  CommonTableViewCell.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/11/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class CommonTableViewCell: UITableViewCell {
  
    
    
    //List Screen
    @IBOutlet weak var List_Image: UIImageView!
    @IBOutlet weak var List_lbl_Title: UILabel!
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }

}
