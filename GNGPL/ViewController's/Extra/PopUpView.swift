//
//  PopUpView.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/21/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

//CNG/PNG
//0 == Prefix 1 == SelectSociety , 2 == Constituency , 3 == city/District , 4 taluka ,

//Comm/Ind
//5 == prefix 6 == Constituency , 7 == city  8 = taluka,

//Bulider
//9 == prefix 10 == Constituency ,11 == city , 12 = taluka

//15 for Select Type

// Set Profile
// 21 = Society , 22= City , 23 Taluka

// Feedback
// 31 = typeFeedback

//CNG SAVing Calculator
// 41 =

import UIKit

class PopUpView: UIViewController {
    
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var searchBar: UISearchBar!

    
    var strTitle = String()
    var aryTBL = NSMutableArray()
    var strTag = Int()
    var aryFilterData = NSMutableArray()

    
    weak var handleCng_Png_Com_Ind_View: refreshCng_Png_Com_Ind_View?
    weak var handleSetProfile_View: refreshSetProfile_View?
    weak var handleFeedBack_View: refreshFeedback_View?

    weak var handleCNGSaving_View: refreshCNGServiceCalculator?
    weak var handleIndustrialSavingCalculatorr: refreshIndustrialSavingCalculatorr?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvForList.estimatedRowHeight = 50.0
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        tvForList.tableFooterView = UIView()
        tvForList.dataSource = self
        tvForList.delegate = self
        self.view.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
        aryFilterData = NSMutableArray()
        aryFilterData = aryTBL
    }
    
    override func viewWillLayoutSubviews() {
     
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
      self.dismiss(animated: false) { }
        
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PopUpView : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryFilterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvForList.dequeueReusableCell(withIdentifier: "popupCell", for: indexPath as IndexPath) as! popupCell
        let dict = aryFilterData.object(at: indexPath.row)as? NSDictionary
        cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "name")as! String)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryFilterData.object(at: indexPath.row)as? NSDictionary
            DispatchQueue.main.async {
                if ( self.strTag == 15  ){
                    self.handleCng_Png_Com_Ind_View?.refreshviewSelectApplySection(dictData: dict!, tag: self.strTag)
                }
              else  if (self.strTag == 0 || self.strTag == 1 || self.strTag == 2 || self.strTag == 3  || self.strTag == 4 ){
                    self.handleCng_Png_Com_Ind_View?.refreshviewCng_Png(dictData: dict!, tag: self.strTag)
                }
                else  if (self.strTag == 5 || self.strTag == 6 || self.strTag == 7  || self.strTag == 8){
                    self.handleCng_Png_Com_Ind_View?.refreshviewCom_Ind(dictData: dict!, tag: self.strTag)
                } else  if (self.strTag == 8 || self.strTag == 9 || self.strTag == 10 || self.strTag == 11 || self.strTag == 12  ){
                    self.handleCng_Png_Com_Ind_View?.refreshviewBulider(dictData: dict!, tag: self.strTag)
                }
                else if (self.strTag == 21 || self.strTag == 22 || self.strTag == 23){
                    self.handleSetProfile_View?.refreshview(dictData: dict!, tag: self.strTag)
                } else if (self.strTag == 31){
                    self.handleFeedBack_View?.refreshview(dictData: dict!, tag: self.strTag)
                }else if (self.strTag == 41) || (self.strTag == 42) || (self.strTag == 43) || (self.strTag == 44){
                    self.handleCNGSaving_View?.refreshviewCNGServiceCalculator(dictData: dict!, tag: self.strTag)
                }
                else if (self.strTag == 51) || (self.strTag == 52){
                    self.handleIndustrialSavingCalculatorr?.refreshviewIndustrialSavingCalculator(dictData: dict!, tag: self.strTag)
                }
            }
     
        self.dismiss(animated: false) {}
    }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 0.4) {
//            cell.transform = CGAffineTransform.identity
//        }
//    }
    
}
class popupCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var popupCell_lbl_Title: UILabel!
}


// MARK: - -------------UISearchBarDelegate
// MARK: -
extension  PopUpView : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        
        let resultPredicate = NSPredicate(format: "name contains[c]", argumentArray: [Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryTBL).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryFilterData = NSMutableArray()
            self.aryFilterData = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForList.reloadData()
        }
        else{
            self.aryFilterData = NSMutableArray()
            self.aryFilterData = self.aryTBL.mutableCopy() as! NSMutableArray
            self.tvForList.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        
    }
}
