//
//  PayOnlineVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 6/27/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import SafariServices

class PayOnlineVC: UIViewController , SFSafariViewControllerDelegate {
    
    @IBOutlet weak var viewHeader: CardView!
   // @IBOutlet weak var webView: WKWebView!

   // var loader = UIAlertController()

     var strUrl = String()
    var tagForBack = 0
    var strViewComeFrom = String()
    // MARK: - --------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
      
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            let safariVC = SFSafariViewController(url: NSURL(string: self.strUrl)! as URL)
                  self.present(safariVC, animated: true, completion: nil)
                  safariVC.delegate = self
        }
          
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
//        let myURLString = strUrl
//        let url = URL(string: myURLString)
//        let request = URLRequest(url: url!)
//        webView.load(request)
//        webView.uiDelegate = self
//        webView.navigationDelegate = self
      
    }
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
        var backStatus = true
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: UserDashBoardVC.self) {
                backStatus = false
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        if(backStatus == true){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "UserDashBoardVC")as! UserDashBoardVC
            self.navigationController?.pushViewController(testController, animated: false)
        }
    }

      
    // MARK: - --------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        if(tagForBack == 0){
            let alert = UIAlertController(title: alertMessage, message: Alert_PaymentCancel, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
                self.navigationController?.popViewController(animated: true)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
            }))
            self.present(alert, animated: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func showAlert(strMessage : String , tag : Int){
        
        var alertTitle = alertMessage
        if(tag == 1){
            alertTitle = Alert_PaymentDoneTitle
        }else if (tag == 2){
            alertTitle = Alert_PaymentFailTitle
        }else if (tag == 3){
            alertTitle = Alert_PaymentPendingTitle
        }
        
        let alert = UIAlertController(title: alertTitle, message: strMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            if(self.strViewComeFrom == "PayBill" || (self.strViewComeFrom == "BalanceSecurityDeposite")){
                
                var backStatus = true
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: UserDashBoardVC.self) {
                        backStatus = false
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
                
                if(backStatus == false){
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "UserDashBoardVC")as! UserDashBoardVC
                    self.navigationController?.pushViewController(testController, animated: false)
                    
                }
                
            }
            else if (self.strViewComeFrom == "MakePaymentScreen"){
                if(tag == 2 || tag == 0){ // Fail / Error
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: MakePaymentScreenVC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }else if (tag == 3){ // Pending
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: UserDashBoardVC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }
                else if (tag == 1){ // Done
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: UserDashBoardVC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }
            }
            
        }))
        self.present(alert, animated: true)
    }
}
//
//// MARK :
////MARK : UIWebViewDelegate
//
//extension PayOnlineVC : UIWebViewDelegate{
//    func webViewDidStartLoad(_ webView: UIWebView) {
//       // dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
//    }
//
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        remove_dotLoader(controller: self)
//        let response =  webView.stringByEvaluatingJavaScript(from: "document.body.innerText")
//        if((response?.contains("Apply for New Connection"))! || (response?.contains("Forgot Password?"))!){
//            showAlert(strMessage: alertSomeError, tag: 0)
//        }else if ((response?.contains("GNGPL_Payment_Completed"))!){
//            showAlert(strMessage: Alert_PaymentDone, tag: 1)
//        }else if ((response?.contains("GNGPL_Payment_Fail"))!){
//            showAlert(strMessage: Alert_PaymentFail, tag: 2)
//        }
//        else if ((response?.contains("GNGPL_Payment_Pending"))!){
//            showAlert(strMessage: Alert_PaymentPending, tag: 3)
//        }
//        else if ((response?.contains("PHP Fatal error:"))!){
//            showAlert(strMessage: Alert_PaymentDone, tag: 1)
//        }
//    }
//
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        remove_dotLoader(controller: self)
//    }
//}
//extension PayOnlineVC: WKNavigationDelegate , WKUIDelegate{
//    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//        print("-----------didFail",error)
//        self.loader.dismiss(animated: false) {}
//
//    }
//    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        print("Started to load")
//        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
//        self.present(loader, animated: false, completion: nil)
//
//    }
//  
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        print("Finished loading")
//      
//        webView.evaluateJavaScript("document.body.innerText") { (response, error) in
//            let str = (response as Any)as! String
//            print(response)
//            if(str.contains("Apply for New Connection")) || (str.contains("Forgot Password?")){
//                self.showAlert(strMessage: alertSomeError, tag: 0)
//            }else if ((str.contains("GNGPL_Payment_Completed"))){
//                self.showAlert(strMessage: Alert_PaymentDone, tag: 1)
//            }else if ((str.contains("GNGPL_Payment_Fail"))){
//                self.showAlert(strMessage: Alert_PaymentFail, tag: 2)
//            }
//            else if ((str.contains("GNGPL_Payment_Pending"))){
//                self.showAlert(strMessage: Alert_PaymentPending, tag: 3)
//            }
//            else if ((str.contains("PHP Fatal error:"))){
//                self.showAlert(strMessage: Alert_PaymentDone, tag: 1)
//            }
//        }
//        self.loader.dismiss(animated: false) {}
//    }
//
//    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
//        print(error.localizedDescription)
//        self.loader.dismiss(animated: false) {}
//
//    }
////    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
////        let cred = URLCredential(trust: challenge.protectionSpace.serverTrust!)
////        completionHandler(.useCredential, cred)
////    }
//}
