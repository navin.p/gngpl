//
//  ListControllerVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/13/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class ListControllerVC: UIViewController {
   
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var view_ForHeader: CardView!

    var ary_CollectionData = NSMutableArray()
    var dictData = NSDictionary()
  var refresher:UIRefreshControl!

    
    
    // MARK: - ----------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)

        self.refresher = UIRefreshControl()
        self.collection!.alwaysBounceVertical = true
        self.refresher.tintColor = hexStringToUIColor(hex: primaryColor)
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.collection!.addSubview(refresher)
        let strOption = "\(dictData.value(forKey: "connection_name")!)"
        lblTitle.text = strOption
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // 4 for CNG , 5 for PNG, 6 Industrial, 7 Commercial, 10 Builder/Society,11 Apply New Connection
        if ary_CollectionData.count == 0 {
            let strOptionID = "\(dictData.value(forKey: "id")!)"
            self.call_GetListData_API(strID: strOptionID, tag: 1)
        }
       
    }
    
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        let strOptionID = "\(dictData.value(forKey: "id")!)"
        self.call_GetListData_API(strID: strOptionID, tag: 0)
    }
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {

            self.navigationController?.popViewController(animated: true)
    }
  
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_GetListData_API(strID : String , tag : Int)  {
        if !(isInternetAvailable()){
            self.showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)

        }else{
            
            //----1
            if tag == 1{
                  dotLoader(sender: UIButton() , controller : self, viewContain: UIView(), onView: "MainView")
            }
          
            //---2
            let dictData = NSMutableDictionary()
            dictData.setValue("content_pages", forKey: "request")
            dictData.setValue(strID, forKey: "id")

           
            WebService.callAPIBYPOST(parameter: dictData, url: URL_Content_Pages_Data) { (responce, status) in
                if tag == 1{
                   remove_dotLoader(controller: self)
                }else{
                    self.refresher.endRefreshing()
                }
                removeErrorView()
                
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let aryData  = (dict.value(forKey: "data")as! NSArray)
                        self.ary_CollectionData = NSMutableArray()
                        self.ary_CollectionData = aryData.mutableCopy()as! NSMutableArray
                      self.animateCollection()
                    }else{
                        self.showErrorWithImage( strTitle: "\(dict.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
}

// MARK: - ----------------UICollectionViewDelegate
// MARK: -

extension ListControllerVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ary_CollectionData.count
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListCell", for: indexPath as IndexPath) as! ListCell
        let dict = ary_CollectionData.object(at: indexPath.row)as! NSDictionary
        cell.ListCell_lbl_Title.text = "\(dict.value(forKey: "title")!)"
        let urlImage = "\(dict.value(forKey: "image_syspath")!)"
        cell.ListCell_Image.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collection.frame.size.width)  / 2, height:(self.collection.frame.size.width)  / 2)
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = ary_CollectionData.object(at: indexPath.row)as! NSDictionary
        let strpage_category = "\(dict.value(forKey: "page_category")!)"
        // 1 For description 2 or Form
        if strpage_category == "1" {
            DispatchQueue.main.async{
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "AboutVC")as! AboutVC
                testController.dict_Data =  dict
                testController.strViewComeFrom = "\(dict.value(forKey: "title")!)" as NSString
                self.navigationController?.pushViewController(testController, animated: true)
                
            }
        }else{
            if(dict_Login_Data.count != 0){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "UserDashBoardVC")as! UserDashBoardVC
                testController.strViewComeFrom = "DeshBoardSection"
                self.navigationController?.pushViewController(testController, animated: true)
            }else{
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
    }
    func animateCollection() {
        self.collection.reloadData()
        
        let cells =  self.collection.visibleCells
        let tableHeight: CGFloat =     self.collection.bounds.size.height
        
        for i in cells {
            let cell: UICollectionViewCell = i as UICollectionViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UICollectionViewCell = a as UICollectionViewCell
            UIView.animate(withDuration: 1.2, delay: 0.05 * Double(index), usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveLinear, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}

class ListCell: UICollectionViewCell {
    //DashBoard
    @IBOutlet weak var ListCell_Image: UIImageView!
    @IBOutlet weak var ListCell_lbl_Title: UILabel!
    
}
