//
//  QueriesVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/7/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData

class QueriesVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var btnTollfree: UIButton!

    var dictQuriesData = NSMutableDictionary()
    var expandedSections = Int()
    var arraySection = NSMutableArray()
    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        expandedSections = 999999
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)

        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        tvlist.backgroundColor = hexStringToUIColor(hex: primaryGrayColor)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        btnTollfree.backgroundColor = UIColor.white
        if(getQueriesDataFromLocal().count != 0){
            print(getQueriesDataFromLocal())
            dictQuriesData = NSMutableDictionary()
            dictQuriesData = (getQueriesDataFromLocal().object(at: 0) as AnyObject).mutableCopy() as! NSMutableDictionary
            self.setDataOnView(dictData: dictQuriesData)
            
        }else{
            self.call_Queries_API()
        }
    }
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actionOnTollFree(_ sender: UIButton) {
        let strNumber = "\((sender.titleLabel?.text!)!)".replacingOccurrences(of: "Toll Free - ", with: "")
        if strNumber.count > 7 {
            if callingFunction(number: strNumber as NSString){
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
            }
        }
    }
    
    // MARK: - --------------Local Data Base
    // MARK: -
    func getQueriesDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "Queries", strkey: "queries")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "queries") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    
    // MARK: - ---------------Data Set On View
    // MARK: -
    func setDataOnView(dictData : NSDictionary)  {
        print(dictData)
        if(dictData.count == 0){
                  self.showErrorWithImage( strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
        }
        btnTollfree.backgroundColor = hexStringToUIColor(hex: orangeColor)
        var strTollFree = "\(dictData.value(forKey: "toll_free_number")!)"
        if(strTollFree == ""){
            strTollFree = "N/A"
        }
        btnTollfree.setTitle("Toll Free - \(strTollFree == "" ? "N/A" : strTollFree)", for: .normal)
        arraySection = NSMutableArray()
        arraySection = (dictData.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
        self.tvlist.reloadData()
        
    }
    
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_Queries_API()  {
        if !(isInternetAvailable()){
            showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("get_faq", forKey: "request")
            dotLoader(sender: UIButton() , controller : self, viewContain: UIView(), onView: "MainView")

            WebService.callAPIBYPOST(parameter: dictData, url: URL_FAQ) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    print(responce)
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let arySaveData = NSMutableArray()
                        arySaveData.add(dict)
                        deleteAllRecords(strEntity:"Queries")
                        saveDataInLocalArray(strEntity: "Queries", strKey: "queries", data: arySaveData)
                        self.setDataOnView(dictData: dict)
                    }else{
                        self.showErrorWithImage( strTitle: "\(dict.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
}

// MARK:- UITableView Delegate & DataSource
// MARK:-
extension QueriesVC : UITableViewDelegate , UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int{
            return arraySection.count
           }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return expandedSections == section ?1 :0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tvlist.dequeueReusableCell(withIdentifier: "UserQuriesCell", for: indexPath as IndexPath) as! UserQuriesCell
        cell1.lblDetail.text = ((arraySection.object(at: indexPath.row) as! NSDictionary).value(forKey: "description") as! String)
        
        return cell1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
      
            return UITableView.automaticDimension;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let headerView = UIView()
        let headerTitle = UILabel()
        let imageView1 = UIImageView()
        
        let dict = arraySection.object(at: section)as! NSDictionary
        headerTitle.text = "\(section + 1). \(String(describing: dict.value(forKey: "title")!))"
        headerTitle.textColor = UIColor.black
        headerTitle.numberOfLines = 3
        headerView.backgroundColor = UIColor.white
        headerTitle.font = UIFont(name: primaryFontLatoBold, size: 16)

        let height = headerTitle.text?.heightForString(constraintedWidth: tableView.frame.width, font: headerTitle.font)
        
        headerView.frame = CGRect(x:0, y: 0, width: Int(tableView.frame.size.width), height: Int(height!))
        headerTitle.frame = CGRect(x:25, y: 8, width: Int(tableView.frame.size.width) - 90 , height: Int(headerView.frame.height - 16))
        imageView1.frame = CGRect(x:Int(headerTitle.frame.maxX + 12), y: Int(height!/2 - 20), width: 40 , height: 40)
        imageView1.contentMode = .center
            if (expandedSections == section) {
                imageView1.image = UIImage(named:"uparrow")
            } else {
                imageView1.image = UIImage(named:"downarrow")
            }
            let headerTapped = UITapGestureRecognizer(target: self, action: #selector(self.sectionHeaderTapped))
            headerView.tag = section
            headerView.addGestureRecognizer(headerTapped)
            headerView.addSubview(headerTitle)
            headerView.addSubview(imageView1)
            return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let dict = arraySection.object(at: section)as! NSDictionary
        let headerTitle = "\(section + 1). \(String(describing: dict.value(forKey: "title")!))"
        let height = headerTitle.heightForString(constraintedWidth: tableView.frame.width, font: UIFont(name: primaryFontLatoBold, size: 18)!)
        return expandedSections == section ?height:height
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.frame = CGRect(x:0, y: 0, width: Int(tableView.frame.size.width), height: 15)
        footerView.backgroundColor = hexStringToUIColor(hex: primaryGrayColor)
        return footerView
    }
//    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        view.alpha = 0.4
//        view.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            view.alpha = 1
//            view.transform = .identity
//        }
//    }
    @objc private func sectionHeaderTapped(gestureRecognizer: UITapGestureRecognizer) {
        
        if(expandedSections == (gestureRecognizer.view?.tag)!){
            expandedSections = 999999
        }else{
            expandedSections = (gestureRecognizer.view?.tag)!
        }
             self.tvlist.reloadData()
    }
   
}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class UserQuriesCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblDetail: UILabel!
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
