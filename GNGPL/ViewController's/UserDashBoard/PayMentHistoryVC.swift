//
//  PayMentHistoryVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/7/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class PayMentHistoryVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var tvlist: UITableView!
    var aryPaymentHistoryList = NSMutableArray()
    var refresher = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        tvlist.backgroundColor = hexStringToUIColor(hex: primaryGrayColor)
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)
        self.tvlist.reloadData()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.call_PaymentHistoryData_API(tag: 1)
    }
    
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        self.call_PaymentHistoryData_API(tag: 2)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_PaymentHistoryData_API(tag : Int)  {
        if !(isInternetAvailable()){
            self.showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)
            
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("payment_history_details", forKey: "request")
            if(dict_Login_Data.count != 0){
                dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
            }
            //----1
            if tag == 1{
                dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            }
            WebService.callAPIBYPOST(parameter: dictData, url: URL_payment_history) { (responce, status) in
                print(responce)
                //self.activityIndicator.isHidden = true
                tag == 1 ? remove_dotLoader(controller: self) :  self.refresher.endRefreshing()
                
                removeErrorView()
                
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        self.aryPaymentHistoryList = NSMutableArray()
                        self.aryPaymentHistoryList = (dict.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                    }else{
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        self.showErrorWithImage( strTitle: "\(dict.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }

}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PayMentHistoryVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryPaymentHistoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "PaymentHistoryCell", for: indexPath as IndexPath) as! PaymentHistoryCell
        let dict = removeNullFromDict(dict: (aryPaymentHistoryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        cell.lblTitle.text = "\(dict.value(forKey: "payment_detail")!)"
        cell.lblTransactionID.text = "Transaction ID - \(dict.value(forKey: "transaction_id")!)"
        cell.lblRS.text = "\u{20B9} \(dict.value(forKey: "amount")!)"
        cell.lblDate.text = "\(dict.value(forKey: "date_time")!)"

        if ("\(dict.value(forKey: "payment_mode")!)" == "1"){
            cell.lblPaymentMode.text = "Mode: Online"
            cell.lblDebitedFrom.text = "Payment Method: \(dict.value(forKey: "bank_name")!)"
        }else if ("\(dict.value(forKey: "payment_mode")!)" == "2"){
            cell.lblPaymentMode.text = "Mode: Cheque"
            cell.lblDebitedFrom.text = "Debited From: \(dict.value(forKey: "bank_name")!)"

        }else{
            cell.lblPaymentMode.text = ""
            cell.lblDebitedFrom.text = ""
        }
        
        if ("\(dict.value(forKey: "verfication_status")!)" == "1"){
            cell.lblStatus.text = "Completed"
            cell.lblStatus.textColor = hexStringToUIColor(hex: primaryColor)
        }else if ("\(dict.value(forKey: "verfication_status")!)" == "2"){
            cell.lblStatus.text = "Pending"
            cell.lblStatus.textColor = hexStringToUIColor(hex: orangeColor)
        }else if ("\(dict.value(forKey: "verfication_status")!)" == "3"){
            cell.lblStatus.text = "Fail"
            cell.lblStatus.textColor = UIColor.red
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
//    }
    
    

}

// MARK: - ----------------UserDashBoardCell
// MARK: -
class PaymentHistoryCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTransactionID: UILabel!
    @IBOutlet weak var lblRS: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDebitedFrom: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
