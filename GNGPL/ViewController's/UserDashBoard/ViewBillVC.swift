//
//  ViewBillVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/28/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class ViewBillVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var tvlist: UITableView!
     var aryViewBillList = NSMutableArray()
    var refresher = UIRefreshControl()
    
    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)
        self.call_ViewBillData_API(tag: 1)

      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        self.call_ViewBillData_API(tag: 2)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    

    // MARK: - ---------------API's Calling
    // MARK: -
    func call_ViewBillData_API(tag : Int)  {
        if !(isInternetAvailable()){
            self.showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)
            
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("view_bill", forKey: "request")
            //----1
            dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
            //----2
            if tag == 1{
                dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            }
            
            
            WebService.callAPIBYPOST(parameter: dictData, url: URL_view_bill) { (responce, status) in
                print(responce)
                //self.activityIndicator.isHidden = true
                tag == 1 ? remove_dotLoader(controller: self) :  self.refresher.endRefreshing()
                removeErrorView()
                
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        self.aryViewBillList = NSMutableArray()
                        self.aryViewBillList = (dict.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                    }else{
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)

                        self.showErrorWithImage( strTitle: "\(dict.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewDidLoad()
        
    }
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ViewBillVC : UITableViewDelegate, UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return aryViewBillList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "ViewBillCell", for: indexPath as IndexPath) as! ViewBillCell
        let dict = aryViewBillList.object(at: indexPath.row)as! NSDictionary
        cell.lblTitle.text = "\(dict.value(forKey: "bill_date")!)"
        cell.btnCheck.tag = indexPath.row
        cell.btnCheck.addTarget(self, action: #selector(pressButton(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "webViewVC")as! webViewVC
        testController.strViewComeFrom = "ViewBill"
        testController.dictBillData = (aryViewBillList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
//    }
    
    @objc func pressButton(_ button: UIButton) {
//        let testController = mainStoryboard.instantiateViewController(withIdentifier: "webViewVC")as! webViewVC
//
//        testController.dictBillData = (aryViewBillList.object(at: button.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary
//        self.navigationController?.pushViewController(testController, animated: false)
    }
    
}

// MARK: - ----------------UserDashBoardCell
// MARK: -
class ViewBillCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCheck: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
