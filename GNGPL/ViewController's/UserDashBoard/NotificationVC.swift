//
//  NotificationVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/16/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var tvlist: UITableView!
    var aryNotificationList = NSMutableArray()
    var strComeFrom = String()
    var refresher = UIRefreshControl()

    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        tvlist.backgroundColor = hexStringToUIColor(hex: primaryGrayColor)
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)
        self.call_Notification_API(tag: 1)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        self.call_Notification_API(tag: 2)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        if(strComeFrom == "UserDashBoard")
        {
            self.navigationController?.popViewController(animated: true)
        }else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
            (self.navigationController?.pushViewController(testController, animated: true))
        }
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.call_Notification_API(tag: 1)
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_Notification_API(tag : Int)  {
        if !(isInternetAvailable()){
            self.showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)
            
        }else{
           
            //----1
            if tag == 1{
                dotLoader(sender: UIButton() , controller : self, viewContain: UIView(), onView: "MainView")
            }
            //----2
            let dictData = NSMutableDictionary()
            dictData.setValue("notification_details", forKey: "request")
            dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")

            WebService.callAPIBYPOST(parameter: dictData, url: URL_notification_details) { (responce, status) in
                print(responce)
                tag == 1 ? remove_dotLoader(controller: self) :  self.refresher.endRefreshing()
                removeErrorView()
                
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        self.aryNotificationList = NSMutableArray()
                        self.aryNotificationList = (dict.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                        self.tvlist.reloadData()
                    }else{
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)

                        self.showErrorWithImage( strTitle: "\(dict.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension NotificationVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryNotificationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath as IndexPath) as! NotificationCell
        let dict = aryNotificationList.object(at: indexPath.row)as! NSDictionary
        cell.lblTitle.text = "\(dict.value(forKey: "title")!)"
        cell.lblDetail.text = "\(dict.value(forKey: "description")!)"
        cell.lblDate.text = "\(dict.value(forKey: "date")!)"
        cell.lblServiceName.text = "\(dict.value(forKey: "service_type")!)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
//    }
}

// MARK: - ----------------UserDashBoardCell
// MARK: -
class NotificationCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
