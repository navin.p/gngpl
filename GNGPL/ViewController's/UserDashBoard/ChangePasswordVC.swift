//
//  ChangePasswordVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/7/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
   
    // MARK: - ----------------IBOutlet
    // MARK: -
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var txtOLDPass: ACFloatingTextfield!
    @IBOutlet weak var txtNewPass: ACFloatingTextfield!
    @IBOutlet weak var txtCNewpass: ACFloatingTextfield!
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lbltitle: UILabel!

    var strComeFrom = String()
    var dictData = NSMutableDictionary()

    // MARK: - ----------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
          buttonEnable_OnView(tag: 1, btn: btnSubmit)
        if(strComeFrom == "Forgot Password"){ // From Forgot
            heightConstant.constant = 0.0
            txtOLDPass.isHidden = true
            lbltitle.text = strComeFrom
        }else{ // For Change Password
            heightConstant.constant = 55.0
            txtOLDPass.isHidden = false
        }
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.view.endEditing(true)
          if(strComeFrom == "Forgot Password"){
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: LoginVC.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
          }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func actiononSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        if(strComeFrom == "Forgot Password"){ // From Forgot
            if(validationForForgotPass()){
                call_ChangePasswordFormForgot_API(sender: sender)
            }
        }else{ // For Change Password
            if(validation()){
                call_ChangePassword_API(sender: sender)
            }
        }
    }
    // MARK: - --------------Validation
    // MARK: -
    func validation()-> Bool{
        if txtOLDPass.text?.count == 0 {
            txtOLDPass.showErrorWithText(errorText: "")
            txtOLDPass.errorLineColor = UIColor.red
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtOLDPass.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if ( txtNewPass.text?.count == 0){
            
            txtNewPass.showErrorWithText(errorText: "")
            txtNewPass.errorLineColor = UIColor.red
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtNewPass.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( (txtNewPass.text?.count)! < 6){
            txtNewPass.showErrorWithText(errorText:"" )
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PasswordLimit, viewcontrol: self)
            return false
        }
        else if ((txtCNewpass.text?.count)! == 0){
            txtCNewpass.showErrorWithText(errorText:"" )
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtCNewpass.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            
            return false
        }
        else if (txtCNewpass.text != txtNewPass.text!){
            txtCNewpass.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_CPassword, viewcontrol: self)
            return false
        }
        return true
    }
    
    func validationForForgotPass()-> Bool{
        if ( txtNewPass.text?.count == 0){
            txtNewPass.showErrorWithText(errorText: "")
            txtNewPass.errorLineColor = UIColor.red
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtNewPass.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( (txtNewPass.text?.count)! < 6){
            txtNewPass.showErrorWithText(errorText:"" )
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PasswordLimit, viewcontrol: self)
            return false
        }
        else if ((txtCNewpass.text?.count)! == 0){
            txtCNewpass.showErrorWithText(errorText:"" )
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtCNewpass.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if (txtCNewpass.text != txtNewPass.text!){
            txtCNewpass.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_CPassword, viewcontrol: self)
            return false
        }
        return true
    }
    
//    func buttonEnable()  {
//        if(txtOLDPass.text?.count != 0 && txtNewPass.text?.count != 0 && txtCNewpass.text?.count != 0){
//            buttonEnable_OnView(tag: 1, btn: btnSubmit)
//        }else{
//            buttonEnable_OnView(tag: 2, btn: btnSubmit)
//        }
//    }
    
    // MARK: - ---------------API's Calling
    // MARK: -
    
    func call_ChangePassword_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            
            if(dict_Login_Data.count != 0){
                dictData.setValue("change_password", forKey: "request")
                dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
                dictData.setValue(txtNewPass.text, forKey: "password")
                dictData.setValue(txtOLDPass.text, forKey: "oldpassword")
            }
            
            dotLoader(sender: btnSubmit , controller : self, viewContain: self.view, onView: "Button")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_ChangePassword) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    print(responce)
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        FTIndicator.showNotification(withTitle: alertMessage, message: (dict.value(forKey: "data")as! String))

                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: UserDashBoardVC.self) {
                                self.navigationController!.popToViewController(controller, animated: false)
                                break
                            }
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_ChangePasswordFormForgot_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictDatasend = NSMutableDictionary()
                dictDatasend.setValue("reset_password", forKey: "request")
                dictDatasend.setValue("\(dictData.value(forKey: "bp_number")!)", forKey: "bp_number")
                dictDatasend.setValue(txtNewPass.text, forKey: "password")
            print(dictDatasend)
            
            dotLoader(sender: btnSubmit , controller : self, viewContain: self.view, onView: "Button")
            WebService.callAPIBYPOST(parameter: dictDatasend, url: URL_Reset_password) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    print(responce)
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        FTIndicator.showNotification(withTitle: alertMessage, message: (dict.value(forKey: "msg")as! String))
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: LoginVC.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension ChangePasswordVC : UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 11)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
