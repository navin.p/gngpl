//
//  ReferAmountDetailVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 7/19/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class ReferAmountDetailVC: UIViewController {
  
    // MARK: - ---------------IBOutlet
    // MARK: -
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
   
    // MARK: - ---------------Variable
    // MARK: -
    var strAmount = String()
    
    
    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        lblAmount.text =  "\u{20B9} \(strAmount)"
        lblDetail.text = Alert_ReferEarnDetailMSG
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
  
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
}
