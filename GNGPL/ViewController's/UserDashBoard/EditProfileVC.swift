//
//  EditProfileVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/2/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class EditProfileVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewHeader1: CardView!
    @IBOutlet weak var imgView: UIImageView!

    @IBOutlet weak var lblPersonalInfo: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblKyc: UILabel!

    @IBOutlet weak var txtAdharNumber: UITextField!
    @IBOutlet weak var txtPanCardNumber: UITextField!
    @IBOutlet weak var lblAddressname: UILabel!
    @IBOutlet weak var txtFatherName: UITextField!
    @IBOutlet weak var txtLandLineNumber: UITextField!
    @IBOutlet weak var txtWhatsappNumber: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtFullName: UITextField!
    
    @IBOutlet weak var viewContain: UIView!
    @IBOutlet weak var viewForEmail: UIView!
    @IBOutlet weak var viewForWhatsApp: UIView!
    @IBOutlet weak var viewForLandline: UIView!
    @IBOutlet weak var viewForPanCard: UIView!
    
    @IBOutlet weak var btnEditWhatsapp: UIButton!
    @IBOutlet weak var btnEditEmail: UIButton!
    @IBOutlet weak var btnEditLandline: UIButton!
    @IBOutlet weak var btnEditPanCard: UIButton!

    var selectionTag = 0

    // MARK: - ---------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        viewHeader1.backgroundColor = hexStringToUIColor(hex: primaryColor)
        imgView.backgroundColor = hexStringToUIColor(hex: primaryColor)
        
        lblPersonalInfo.backgroundColor = hexStringToUIColor(hex: primaryGrayColor)
        lblAddress.backgroundColor = hexStringToUIColor(hex: primaryGrayColor)
        lblKyc.backgroundColor = hexStringToUIColor(hex: primaryGrayColor)
         setDataOnView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.imgView.layer.cornerRadius = self.imgView.frame.width /  2
        self.imgView.clipsToBounds = true
        self.imgView.layer.borderWidth = 2.0
        self.imgView.layer.borderColor = UIColor.white.cgColor
        self.imgView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.imgView.transform = .identity
            },
                       completion: nil)
        
        if(self.imgView.image == UIImage(named:"profile_icon")){
            imgView.contentMode = .center
        }else{
            imgView.contentMode = .scaleToFill
        }
        imgView.backgroundColor = hexStringToUIColor(hex: primaryColor)
   }

    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
   
    @IBAction func actiononEdit(_ sender: UIButton) {
        selectionTag = sender.tag
        if(sender.tag == 1){ // Email
            txtEmail.becomeFirstResponder()
            txtEmail.isUserInteractionEnabled = true
        } else if(sender.tag == 2){ // Whatsapp
            txtWhatsappNumber.becomeFirstResponder()
            txtWhatsappNumber.isUserInteractionEnabled = true

        }else if(sender.tag == 3){ // Landline
              txtLandLineNumber.becomeFirstResponder()
            txtLandLineNumber.isUserInteractionEnabled = true

        }else if(sender.tag == 4){ // PanCard
              txtPanCardNumber.becomeFirstResponder()
              txtPanCardNumber.isUserInteractionEnabled = true

        }
        
    }
    
    // MARK: - ---------------API's Calling
    // MARK: -
    
    func call_EditProfile_API(sender : UIButton , viewForActivity : UIView ,txt : UITextField)  {
          self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("edit_profile", forKey: "request")
            dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")

            dictData.setValue(txtEmail.text!, forKey: "email")
            dictData.setValue(txtWhatsappNumber.text, forKey: "whatsapp_number")
            dictData.setValue(txtLandLineNumber.text, forKey: "landline_number")
            dictData.setValue(txtPanCardNumber.text, forKey: "pen_number")
          
            dotLoader(sender: sender , controller : self, viewContain: viewForActivity, onView: "Button")

            print(dictData)
            WebService.callAPIBYPOST(parameter: dictData, url: URL_Edit) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    print(responce)
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                       txt.isUserInteractionEnabled = false
                       sender.setImage(UIImage(named: "check"), for: .normal)
                        let dictData  = (dict.value(forKey: "data")as! NSDictionary)

                        let arySaveData = NSMutableArray()
                        arySaveData.add(dictData)
                        deleteAllRecords(strEntity:"LoginData")
                        saveDataInLocalArray(strEntity: "LoginData", strKey: "logindata", data: arySaveData)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
   
    // MARK: - ---------------Extra Function
    // MARK: -
    
    
    func validation() -> Bool {
        if ( (txtEmail.text?.count)! > 0){
            if(validateEmail(email: txtEmail.text!)){
                return true
            }else{
                txtEmail.shake()
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EmailAddress, viewcontrol: self)

                return false
            }
        }
        
        if ((txtWhatsappNumber.text?.count)! > 0) {
            if ( (txtWhatsappNumber.text?.count)! <= 9){
                txtWhatsappNumber.shake()
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_Whatsapp_limit, viewcontrol: self)

                return false
            }
        }
        if(txtLandLineNumber.text?.count != 0){
            if ( (txtLandLineNumber.text?.count)! <= 9){
                txtLandLineNumber.shake()
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_landline_limit, viewcontrol: self)

                return false
            }
        }
        if(txtPanCardNumber.text?.count != 0){
            if ( (txtPanCardNumber.text?.count)! <= 9){
                txtPanCardNumber.shake()
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PanNumberValid, viewcontrol: self)

                return false
            }
        }
       return true
    }
    
    func setDataOnView() {
        
        let strName = "\(dict_Login_Data.value(forKey: "full_name")!)"
        let strEmail = "\(dict_Login_Data.value(forKey: "email")!)"
        let strMobileNumber = "\(dict_Login_Data.value(forKey: "mobile_number")!)"
        let strWhatsAppNumber = "\(dict_Login_Data.value(forKey: "whatsapp_number")!)"
        let strLandLineNumber = "\(dict_Login_Data.value(forKey: "landline_number")!)"
        let strFather = "\(dict_Login_Data.value(forKey: "father_or_husband_name")!)"
        
        let strhouse_numbert = "\(dict_Login_Data.value(forKey: "house_number")!)"
        let strStreet = "\(dict_Login_Data.value(forKey: "street")!)"
        let strTaluka = "\(dict_Login_Data.value(forKey: "taluka_name")!)"
        let strDistrict = "\(dict_Login_Data.value(forKey: "district_name")!)"
        let strState = "\(dict_Login_Data.value(forKey: "state")!)"
        let strPincode = "\(dict_Login_Data.value(forKey: "pincode")!)"
        var address = ""
        if(strhouse_numbert != ""){
            address = strhouse_numbert
        }
        if(strhouse_numbert != ""){
            address =  address + "," + strStreet
        }
        if(strTaluka != ""){
            address =  address + "," + strTaluka
        }
        if(strDistrict != ""){
            address =  address + "," + strDistrict
        }
        if(strState != ""){
            address =  address + "," + strState
        }
        if(strPincode != ""){
            address =  address + "," + strPincode
        }

        let strPanCard = "\(dict_Login_Data.value(forKey: "pen_number")!)"
        let strAadhar = "\(dict_Login_Data.value(forKey: "aadhar_number")!)"
        imgView.setImageWith(URL(string: dict_Login_Data.value(forKey: "profile_image_sys")as! String), placeholderImage: UIImage(named: "profile_icon"), options: SDWebImageOptions(rawValue: 1), progress: { (proess1, Intproess2) in
        }, completed: { (image, error, type, url) in
            image != nil ? self.imgView.contentMode = .scaleToFill : (self.imgView.contentMode = .center)
        }, usingActivityIndicatorStyle: .gray)
        
        txtFullName.text = strName
        txtEmail.text = strEmail
        txtMobileNumber.text = strMobileNumber
        txtWhatsappNumber.text = strWhatsAppNumber
        txtLandLineNumber.text = strLandLineNumber
        txtFatherName.text = strFather
        lblAddressname.text = address
        txtPanCardNumber.text = strPanCard
        txtAdharNumber.text = strAadhar
        txtFullName.isUserInteractionEnabled = false
        txtMobileNumber.isUserInteractionEnabled = false
        txtFatherName.isUserInteractionEnabled = false
        txtAdharNumber.isUserInteractionEnabled = false
        txtEmail.isUserInteractionEnabled = false
        txtWhatsappNumber.isUserInteractionEnabled = false
        txtLandLineNumber.isUserInteractionEnabled = false
        txtPanCardNumber.isUserInteractionEnabled = false
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension EditProfileVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtWhatsappNumber  {
          return (txtFiledValidation(textField: txtWhatsappNumber, string: string, returnOnly: "NUMBER", limitValue: 9))
            
        }
        if textField == txtEmail  {
            return  txtFiledValidation(textField: txtMobileNumber, string: string, returnOnly: "ALL", limitValue: 45)
        }
        if textField == txtLandLineNumber  {
           return (txtFiledValidation(textField: txtLandLineNumber, string: string, returnOnly: "NUMBER", limitValue: 9))
            
        }
       
        if textField == txtPanCardNumber  {
            return  panCardValidation(textField1: ACFloatingTextfield(), textField2: txtPanCardNumber, string: string, type: "2")
        }
       
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        if(txtEmail == textField){
            if(validateEmail(email: txtEmail.text!)){
                self.call_EditProfile_API(sender: btnEditEmail, viewForActivity: viewForEmail, txt: textField)
            }
        }
        if(txtPanCardNumber == textField){
            if((txtPanCardNumber.text?.count)! >= 10){
                self.call_EditProfile_API(sender: btnEditPanCard, viewForActivity: viewForPanCard, txt: textField)
            }
        }
        if(txtLandLineNumber == textField){
            if((txtLandLineNumber.text?.count)! >= 10){
                self.call_EditProfile_API(sender: btnEditLandline, viewForActivity: viewForLandline, txt: textField)
            }
        }
        if(txtWhatsappNumber == textField){
            if((txtWhatsappNumber.text?.count)! >= 10){
                self.call_EditProfile_API(sender: btnEditWhatsapp, viewForActivity: viewForWhatsApp, txt: textField)
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
        
        if(txtEmail == textField){
            if(validateEmail(email: txtEmail.text!)){
                self.call_EditProfile_API(sender: btnEditEmail, viewForActivity: viewForEmail, txt: textField)
            }
        }
        if(txtPanCardNumber == textField){
            if((txtPanCardNumber.text?.count)! >= 10){
                self.call_EditProfile_API(sender: btnEditPanCard, viewForActivity: viewForPanCard, txt: textField)
            }
        }
        if(txtLandLineNumber == textField){
            if((txtLandLineNumber.text?.count)! >= 10){
                self.call_EditProfile_API(sender: btnEditLandline, viewForActivity: viewForLandline, txt: textField)
            }
        }
        if(txtWhatsappNumber == textField){
            if((txtWhatsappNumber.text?.count)! >= 10){
                self.call_EditProfile_API(sender: btnEditWhatsapp, viewForActivity: viewForWhatsApp, txt: textField)
            }
        }
    }
}
