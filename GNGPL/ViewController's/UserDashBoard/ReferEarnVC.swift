//
//  ReferEarnVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/16/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData

class ReferEarnVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewForCode: CardView!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var btnReferandEarn: UIButton!
    @IBOutlet weak var imgGift: UIImageView!
    @IBOutlet weak var lblMsg: UILabel!

   var dict_Data = NSMutableDictionary()
    var strComeFrom = String()
    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        lblCode.textColor = hexStringToUIColor(hex: primaryColor)
        buttonEnable_OnView(tag: 1, btn: btnReferandEarn)
         if(getTerms_ConditionDataFromLocal(strEntity: "TermsConditionReferCode", strkey: "termsConditionReferCode").count != 0){
            
            let dataRefer = ((getDataFromLocal(strEntity: "TermsConditionReferCode", strkey: "termsConditionReferCode").object(at: 0) as AnyObject))
            
            dict_Data = ((((dataRefer as AnyObject).value(forKey: "termsConditionReferCode") as! NSArray).object(at: 0)) as AnyObject).mutableCopy() as! NSMutableDictionary
            
        self.lblMsg.text = Alert_Referbenefit_msg
        }else{
            call_TermsAndConditionsForReferCode_API()
        }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(dict_Login_Data.count != 0){
           lblCode.text = "\(dict_Login_Data.value(forKey: "refferal_code")!)"
        }
        
        DeviceType.IS_IPHONE_5 == true ? imgGift.contentMode = .scaleAspectFit : (imgGift.contentMode = .center)
     
        buttonEnable_OnView(tag: 1, btn: btnReferandEarn)
        viewForCode.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.viewForCode.transform = .identity
            }, completion: nil)
      
    }
    
    override func viewDidLayoutSubviews() {
        let border = CAShapeLayer();
        border.strokeColor = hexStringToUIColor(hex: primaryColor).cgColor
        border.fillColor = nil;
        border.lineDashPattern = [4, 4];
        border.path = UIBezierPath(rect: viewForCode.bounds).cgPath
        border.frame = viewForCode.bounds;
        viewForCode.layer.addSublayer(border);
    }
    // MARK: - --------------Local Data Base
    // MARK: -
    func getTerms_ConditionDataFromLocal(strEntity: String ,strkey : String )-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: strEntity, strkey: strkey)
        
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: strkey) ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }

    // MARK: - ---------------API Calling
    // MARK: -
    func call_TermsAndConditionsForReferCode_API()  {
        if !(isInternetAvailable()){
            self.showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            
            //----1
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            //---2
            let dictData = NSMutableDictionary()
            dictData.setValue("refer_earn", forKey: "request")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_refer_earn) { (responce, status) in
                remove_dotLoader(controller: self)
                removeErrorView()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let dictData  = (dict.value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        dictData.setValue(dict.value(forKey: "msg")as! String, forKey: "msg")
                        
                        let arySaveData = NSMutableArray()
                        arySaveData.add(dictData)
                        deleteAllRecords(strEntity:"TermsConditionReferCode")
                        saveDataInLocalArray(strEntity: "TermsConditionReferCode", strKey: "termsConditionReferCode", data: arySaveData)
                        self.dict_Data = NSMutableDictionary()
                        self.dict_Data = dictData.mutableCopy()as! NSMutableDictionary
                       self.lblMsg.text = Alert_Referbenefit_msg
                    }else{
                        self.showErrorWithImage(strTitle: "\(responce.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)
                        
                    }
                    
                }else{
                    self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        if(strComeFrom == "Alert"){
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: DashBoardVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func actiononReferAndEarn(_ sender: UIButton) {
        
        
        
        
       let msg = "Hi, I have registered for piped gas connection with only Rs.500.\nYou know piped gas 35% cheaper and much safer than LPG Cylinder.  Festival season offer is launched by  GNGPL in which they are giving gas bill discount worth Rs 300 for every successful reference.\nYou too can register for PNG Connection through GOA NATURAL GAS App.\nDownload the 'GOA NATURAL GAS' App now. Android users click\nhttps://play.google.com/store/apps/details?id=com.info.gngpl\niOS users click\nhttps://apps.apple.com/in/app/goa-natural-gas/id1477457698\nDo not forget to use my Referral Code during Registration."
        
        
        let textToShare = "Refer Code: \(lblCode.text!) \n\(AppName)"  + "\n\(msg)"
       
            
        let objectsToShare = [textToShare] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.setValue("'GOA NATURAL GAS' Refer Code: \(lblCode.text!)", forKey: "Subject")
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    @IBAction func actiononTernsAndCondition(_ sender: UIButton) {
        DispatchQueue.main.async{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AboutVC")as! AboutVC
            testController.dict_Data =  self.dict_Data 
            testController.strViewComeFrom = "Terms and Conditions "
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
}
