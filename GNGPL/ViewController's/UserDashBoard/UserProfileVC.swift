//
//  UserProfileVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/26/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import SafariServices
import CoreData
class UserProfileVC: UIViewController {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewHeader1: CardView!
    
    
    let arrysections = ["Payment","Security and Setting","Notifications","Recommendation","Support"]
    
    var arySecurityAndSetting = (["image" : "change_password", "title" : "Change Password"])
    var aryNotification = (["image" : "notification_2", "title" : "Push Notification"])
    var aryRecomendation = NSMutableArray()
    var arySupport = NSMutableArray()
      var aryPayMent = NSMutableArray()
    // MARK: - ----------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        viewHeader1.backgroundColor = hexStringToUIColor(hex: primaryColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0
        
        
        var dict0 = NSMutableDictionary()
        dict0.setValue("payment", forKey: "image")
        dict0.setValue("Payment History", forKey: "title")
        aryPayMent.add(dict0)

        dict0 = NSMutableDictionary()
        dict0.setValue("refer_amount", forKey: "image")
        dict0.setValue("Refer and Earn Amount", forKey: "title")
        aryPayMent.add(dict0)
        
        
        dict0 = NSMutableDictionary()
        dict0.setValue("share", forKey: "image")
        dict0.setValue("Share App", forKey: "title")
        aryRecomendation.add(dict0)

        dict0 = NSMutableDictionary()
        dict0.setValue("rate", forKey: "image")
        dict0.setValue("Rate Us", forKey: "title")
        aryRecomendation.add(dict0)
        
        dict0 = NSMutableDictionary()
        dict0.setValue("help", forKey: "image")
        dict0.setValue("Queries", forKey: "title")
        arySupport.add(dict0)
        
        dict0 = NSMutableDictionary()
        dict0.setValue("feedback", forKey: "image")
        dict0.setValue("Submit Complaint", forKey: "title")
        arySupport.add(dict0)

        dict0 = NSMutableDictionary()
        dict0.setValue("wesite", forKey: "image")
        dict0.setValue("Go to Website", forKey: "title")
        arySupport.add(dict0)
       
        self.tvlist.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.width /  2
        self.imgProfile.clipsToBounds = true
        self.imgProfile.layer.borderWidth = 2.0
        self.imgProfile.layer.borderColor = UIColor.white.cgColor
        self.imgProfile.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration:0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.imgProfile.transform = .identity
            },
                       completion: nil)
        if(getLoginDataFromLocal().count != 0){
            print(getLoginDataFromLocal())
            dict_Login_Data = NSMutableDictionary()
            dict_Login_Data = ((getLoginDataFromLocal() as NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary
            if (dict_Login_Data.count != 0){
                if(dict_Login_Data.count != 0){
                    imgProfile.setImageWith(URL(string: dict_Login_Data.value(forKey: "profile_image_sys")as! String), placeholderImage: UIImage(named: "profile_icon"), options: SDWebImageOptions(rawValue: 1), progress: { (proess1, Intproess2) in
                    }, completed: { (image, error, type, url) in
                        image != nil ? self.imgProfile.contentMode = .scaleToFill : (self.imgProfile.contentMode = .center)
                    }, usingActivityIndicatorStyle: .gray)
                    lblUserName.text = (dict_Login_Data.value(forKey: "full_name")as! String)
                }
            }
        }
        
        
        if(self.imgProfile.image == UIImage(named:"profile_icon")){
            imgProfile.contentMode = .center
        }else{
            imgProfile.contentMode = .scaleToFill
        }
        imgProfile.backgroundColor = hexStringToUIColor(hex: primaryColor)
    }
    // MARK: - --------------Local Data Base
    // MARK: -
    func getLoginDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "LoginData", strkey: "logindata")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "logindata") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func actiononEdit(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "EditProfileVC")as! EditProfileVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @objc func logOut()  {
        
        let alert = UIAlertController(title: alertMessage, message: alertLogout, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            self.call_LogOut_API()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
            print("Yay! You brought your towel!")
        }))
        self.present(alert, animated: true)
    }
    
    @objc func switchStateDidChange(_ sender:UISwitch){
        sender.isOn == true ? call_NotificationSatus_API(statusNoti: "1") : call_NotificationSatus_API(statusNoti: "0")
    }
    
    // MARK: - ----------------API's Calling
    // MARK: -
    func call_LogOut_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictDatasend = NSMutableDictionary()
            dictDatasend.setValue("logout_api", forKey: "request")
            dictDatasend.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
            dictDatasend.setValue("2", forKey: "login_status")
             dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callAPIBYPOST(parameter: dictDatasend, url: URL_logout) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        // let dictData  = (dict.value(forKey: "data")as! NSDictionary)
                        deleteAllRecords(strEntity:"ContactData")
                        deleteAllRecords(strEntity:"AboutData")
                        deleteAllRecords(strEntity:"TermsCondition")
                        deleteAllRecords(strEntity:"DropDownData")
                        deleteAllRecords(strEntity:"Dashboard")
                        deleteAllRecords(strEntity:"SavingCalculator")
                        deleteAllRecords(strEntity:"Queries")
                        deleteAllRecords(strEntity:"TermsConditionReferCode")
                        deleteAllRecords(strEntity:"LoginData")
                        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: DashBoardVC.self) {
                                self.navigationController!.popToViewController(controller, animated: false)
                                break
                            }
                        }
                        
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func call_NotificationSatus_API(statusNoti: String)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictDatasend = NSMutableDictionary()
            dictDatasend.setValue("update_notification", forKey: "request")
            dictDatasend.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
            dictDatasend.setValue(statusNoti, forKey: "notification_status")
            //  dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callAPIBYPOST(parameter: dictDatasend, url: URL_Update_notification) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        // let dictData  = (dict.value(forKey: "data")as! NSDictionary)
                        dict_Login_Data.setValue(statusNoti, forKey: "notification_status")
                        let arySaveData = NSMutableArray()
                        arySaveData.add(dict_Login_Data)
                        deleteAllRecords(strEntity:"LoginData")
                        saveDataInLocalArray(strEntity: "LoginData", strKey: "logindata", data: arySaveData)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ----------------SFSafariViewControllerDelegate
// MARK: -
extension UserProfileVC : SFSafariViewControllerDelegate{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        
    }
    func safariViewController(_: SFSafariViewController, didCompleteInitialLoad: Bool){
        
    }
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension UserProfileVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrysections.count + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 4 {
            return arySupport.count
        }  else if section == 3 {
            return aryRecomendation.count
        } else if section == 0 {
            return aryPayMent.count
        }else{
            return 1
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "UserProfile", for: indexPath as IndexPath) as! UserProfileCell
        cell.switchbtn.isHidden = true
        if indexPath.section == 0 {
            let dict = aryPayMent.object(at: indexPath.row)as! NSDictionary
            cell.lblTitle.text = (dict["title"] as! String)
            cell.imgView.image = UIImage(named:dict["image"]! as! String)
            return cell
            
        }
        else if indexPath.section == 1 {
            let dict = [arySecurityAndSetting][indexPath.row]
            cell.lblTitle.text = dict["title"]
            cell.imgView.image = UIImage(named:dict["image"]!)
            return cell
            
        }
        else if indexPath.section == 2 {
            let dict = [aryNotification][indexPath.row]
            cell.lblTitle.text = dict["title"]
            cell.imgView.image = UIImage(named:dict["image"]!)
            cell.switchbtn.isHidden = false
            cell.switchbtn.addTarget(self, action: #selector(self.switchStateDidChange(_:)), for: .valueChanged)
            if(dict_Login_Data.value(forKey: "notification_status") != nil){
                "\(dict_Login_Data.value(forKey: "notification_status")as! String)" == "1" ? cell.switchbtn.setOn(true, animated: false) : cell.switchbtn.setOn(false, animated: false)
            }
            return cell
            
        }
        else if indexPath.section == 3 {
            let dict = aryRecomendation.object(at: indexPath.row)as! NSDictionary
            cell.lblTitle.text = (dict["title"] as! String)
            cell.imgView.image = UIImage(named:dict["image"]! as! String)
            return cell
            
        }else if indexPath.section == 4 {
            let dict = arySupport.object(at: indexPath.row)as! NSDictionary
            cell.lblTitle.text = (dict["title"] as! String)
            cell.imgView.image = UIImage(named:dict["image"]! as! String)
            return cell
            
        }
        else  {
            let cell1 = tvlist.dequeueReusableCell(withIdentifier: "UserProfileBottom", for: indexPath as IndexPath) as! UserProfileCell
            cell1.btnLogout.tag = indexPath.row
            
            cell1.btnLogout.backgroundColor = hexStringToUIColor(hex: primaryGrayColor)
            
            cell1.btnLogout.addTarget(self, action:#selector(logOut), for: .touchUpInside)
            cell1.lblVersion.text = "Version " + application_Version
            
            return cell1
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Payment
        if (indexPath.section == 0){
            if(indexPath.row == 0){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "PayMentHistoryVC")as! PayMentHistoryVC
            self.navigationController?.pushViewController(testController, animated: true)
            }else if (indexPath.row == 1){
                
                
                if(nsud.value(forKey: "GNGPL_RefreshToken") != nil){
                    let dict = nsud.value(forKey: "GNGPL_RefreshToken")as! NSDictionary
                    let refer_earn_amount = "\(dict.value(forKey: "refer_earn_amount")!)"
                    if(refer_earn_amount != ""){
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReferAmountDetailVC")as! ReferAmountDetailVC
                        testController.strAmount = refer_earn_amount
                        self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_ReferCode, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_ReferCode, viewcontrol: self)

                }
                
              
                
            }
        }
            // Security
        else if(indexPath.section == 1){
            if(indexPath.row == 0){ // ChangePassword
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC")as! ChangePasswordVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
            //Recommendations
        else if indexPath.section == 3{
            if(indexPath.row == 1){ // Rating
                rateForApp(appId: application_ID) { (status) in }
            }
            else{ //Share
                
                let msg = "Download the 'GOA NATURAL GAS' App now. Android users click\nhttps://play.google.com/store/apps/details?id=com.info.gngpl\niOS users click\nhttps://apps.apple.com/in/app/goa-natural-gas/id1477457698"
                let textToShare = AppName + "\n" + msg
                let objectsToShare = [textToShare] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.setValue("Download the 'GOA NATURAL GAS' App now", forKey: "Subject")
                activityVC.popoverPresentationController?.sourceView = self.view
                    self.present(activityVC, animated: true, completion: nil)
            }
        }
            //Support
        else if indexPath.section == 4{
            if(indexPath.row == 0){ // Queries
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "QueriesVC")as! QueriesVC
                self.navigationController?.pushViewController(testController, animated: true)
                
            }
            else if(indexPath.row == 1){ // Submit Feedback
//                let testController = mainStoryboard.instantiateViewController(withIdentifier: "FeedBackVC")as! FeedBackVC
//                self.navigationController?.pushViewController(testController, animated: true)
//
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "webViewVC")as! webViewVC
                testController.strViewComeFrom = "Complaint"
              
                self.navigationController?.pushViewController(testController, animated: true)
                
                
            }else if(indexPath.row == 2){ // Go To WebSite
                let svc = SFSafariViewController(url: URL(string: Website)!)
                present(svc, animated: true, completion: nil)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section != 5 {
            return 40
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section != 5 {
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
            headerView.backgroundColor = hexStringToUIColor(hex: primaryGrayColor)
            let label = UILabel()
            label.frame = CGRect.init(x: 25, y: 5, width: headerView.frame.width-50, height: headerView.frame.height-10)
            label.text = arrysections[section]
            label.font =  UIFont(name: primaryFontLatoRegular, size: 15.0)
            label.textColor = UIColor.black
            headerView.addSubview(label)
            return headerView
        }else{
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 0))
            return headerView
            
        }
        
    }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
//    }
    
    
    
    
}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class UserProfileCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var switchbtn: UISwitch!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
