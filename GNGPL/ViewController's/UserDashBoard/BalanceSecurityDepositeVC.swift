//
//  BalanceSecurityDepositeVC.swift
//  GNGPL
//  Created by Navin Patidar on 1/16/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.

import UIKit

class BalanceSecurityDepositeVC: UIViewController {
   
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnInFull: UIButton!
    @IBOutlet weak var btnConvertEMI: UIButton!

    @IBOutlet weak var lblBalanceAmount: UILabel!
    @IBOutlet weak var lblBalanceDepositeAmount: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!

    @IBOutlet weak var heightDiscount: NSLayoutConstraint!
    var dict_BalanceAmount = NSMutableDictionary()
    var strMeterNumber = String()

    // MARK: - ---------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonEnable_OnView(tag: 1, btn: btnInFull)
        buttonEnable_OnView(tag: 1, btn: btnConvertEMI)

        self.viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        self.lblBalanceAmount.textColor = hexStringToUIColor(hex: primaryColor)
        self.lblBalanceDepositeAmount.textColor = hexStringToUIColor(hex: primaryColor)
        self.lblBalanceAmount.text = "\u{20B9} 00"
        self.lblBalanceDepositeAmount.text = "\u{20B9} 00"
        self.btnInFull.isHidden = true
        self.lblMessage.text = ""
        self.lblMessage.textColor = hexStringToUIColor(hex: orangeColor)
        self.btnConvertEMI.isHidden = true
        if(nsud.value(forKey: "GNGPL_RefreshToken") != nil){
            let dict = nsud.value(forKey: "GNGPL_RefreshToken")as! NSDictionary
            strMeterNumber = "\(dict.value(forKey: "meter_number")!)"
        }
        call_balance_security_deposit_API(tag: 1)
    }

    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actiononPayFullAmount(_ sender: UIButton) {
        let dictData = NSMutableDictionary()
        dictData.setValue("pay_balance_security_deposit", forKey: "request")
        dictData.setValue("\(dict_Login_Data.value(forKey: "application_id")!)", forKey: "application_id")
        dictData.setValue("\(strMeterNumber)", forKey: "meter_number")
        dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
        
        dictData.setValue("\(self.dict_BalanceAmount.value(forKey: "balance_amount")!)", forKey: "total_amount")
        dictData.setValue("1", forKey: "conn_payment_status")
        
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentModeSelectionVC")as! PaymentModeSelectionVC
        testController.strComeFrom = "BalanceSecurityDeposite"
        testController.dictPaymentData = dictData
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @IBAction func actiononConvertEMI(_ sender: UIButton) {
        self.call_GetEMIAmount_API(sender: sender)
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_GetEMIAmount_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            //----2
            let dictData = NSMutableDictionary()
            dictData.setValue("get_emi_amount", forKey: "request")
            dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
            dictData.setValue("\(strMeterNumber)", forKey: "meter_number")

            WebService.callAPIBYPOST(parameter: dictData, url: URL_Get_emi_amount) {
                (responce, status) in
                print(responce)
                remove_dotLoader(controller: self)
                removeErrorView()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        
                        let dictData = NSMutableDictionary()
                        dictData.setValue("pay_balance_security_deposit", forKey: "request")
                        dictData.setValue("\(dict_Login_Data.value(forKey: "application_id")!)", forKey: "application_id")
                        dictData.setValue("\(self.strMeterNumber)", forKey: "meter_number")
                        dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
                        dictData.setValue("\(dict.value(forKey: "remaining_amount")!)", forKey: "total_amount")
                        dictData.setValue("2", forKey: "conn_payment_status")
                        
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentModeSelectionVC")as! PaymentModeSelectionVC
                        testController.strComeFrom = "BalanceSecurityDeposite"
                        testController.dictPaymentData = dictData
                        self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func call_balance_security_deposit_API(tag : Int)  {
        if !(isInternetAvailable()){
            self.showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)
        }else{
            //----1
            if tag == 1{
                dotLoader(sender: UIButton() , controller : self, viewContain: UIView(), onView: "MainView")
            }
            //----2
            let dictData = NSMutableDictionary()
            dictData.setValue("security_deposit", forKey: "request")
            dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_balance_security_deposit) {
                (responce, status) in
                print(responce)
              remove_dotLoader(controller: self)
                removeErrorView()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        self.dict_BalanceAmount = NSMutableDictionary()
                        self.dict_BalanceAmount = (dict.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        self.lblDiscount.text = "\u{20B9}\(self.dict_BalanceAmount.value(forKey: "discount_amount")!)"
                       // self.dict_BalanceAmount.setValue("", forKey: "discount_amount")
                        if("\(self.dict_BalanceAmount.value(forKey: "discount_amount")!)".count == 0) || "\(self.dict_BalanceAmount.value(forKey: "discount_amount")!)" == "0"{
                             self.heightDiscount.constant = 0.0
                        }else{
                              self.heightDiscount.constant = 50.0
                        }
                        self.lblBalanceAmount.text = "\u{20B9} \(self.dict_BalanceAmount.value(forKey: "balance_amount")!)"
                        self.lblBalanceDepositeAmount.text = "\u{20B9} \(self.dict_BalanceAmount.value(forKey: "balance_security_deposit")!)"
                        if("\(self.dict_BalanceAmount.value(forKey: "balance_amount")!)" == "0" || ("\(self.dict_BalanceAmount.value(forKey: "confirmation_message")!)" == "1")){
                            self.btnInFull.isHidden = true
                            "\(self.dict_BalanceAmount.value(forKey: "confirmation_message")!)" == "1" ? self.lblMessage.text = Alert_PaymentUnderProgress : (self.lblMessage.text = "")
                           }else{
                            self.btnInFull.isHidden = false
                        }
                        // For EMI Button
                        
                        if("\(self.dict_BalanceAmount.value(forKey: "connection_payment_type")!)" == "3"){
                            self.btnConvertEMI.isHidden = false
                        }else{
                            self.btnConvertEMI.isHidden = true
                        }
                    }else{
                        self.showErrorWithImage( strTitle: "\(dict.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        call_balance_security_deposit_API(tag: 1)
    }
}

