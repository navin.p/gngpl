//
//  AddMeterReadingVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/27/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit


class AddMeterReadingVC: UIViewController {

    @IBOutlet weak var collection_View: UICollectionView!
    @IBOutlet weak var txtReading: ACFloatingTextfield!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!

     var ary_CollectionData = NSMutableArray()
    var imagePicker = UIImagePickerController()
    var strMeterNumber = String()
    var imgMeter = UIImageView()

    // MARK: - ----------------LIfe Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        txtReading.delegate = self
        
        if(nsud.value(forKey: "GNGPL_RefreshToken") != nil){
            let dict = nsud.value(forKey: "GNGPL_RefreshToken")as! NSDictionary
            let ReadingButtonHideShow = "\(dict.value(forKey: "reading_button")!)"
            let ReadingButtonMessage = Alert_reading_msg
            if(ReadingButtonHideShow == "1"){
                btnSubmit.isHidden = false
            }else{
               btnSubmit.isHidden = true
                let alert = UIAlertController(title: alertInfo, message: ReadingButtonMessage, preferredStyle: UIAlertController.Style.alert)
                // add the actions (buttons)
                alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                    self.navigationController?.popViewController(animated: true)

                }))
                self.present(alert, animated: true, completion: nil)
            }
        }else{
               btnSubmit.isHidden = true
        }
//        
        if(nsud.value(forKey: "GNGPL_RefreshToken") != nil){
            let dict = nsud.value(forKey: "GNGPL_RefreshToken")as! NSDictionary
            strMeterNumber = "\(dict.value(forKey: "meter_number")!)"
            
        }
}
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        collection_View.reloadData()
        btnCamera.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.btnCamera.transform = .identity
            },
                       completion: nil)
          buttonEnable_OnView(tag: 1, btn: btnSubmit)

    }
    // MARK: - ----------------IBAction
    // MARK: -
    
    @IBAction func actionOnCamera(_ sender: Any) {
        self.view.endEditing(true)
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            self.imagePicker.sourceType = .camera
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        } else {
            FTIndicator.showToastMessage(Alert_CameraAlert)
        }
    }
    @IBAction func actionONSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtReading.text?.count == 0 {
            txtReading.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_MeterReading, viewcontrol: self)
        }else{
        if (ary_CollectionData.count != 0) {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd HH:mm a"
            let someDateTime = formatter.string(from: Date())
            call_URL_Save_Meter_API(sender: sender , imgMeter : generateImageWithText(text: "\(someDateTime)"))
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_MeterImage, viewcontrol: self)
            }
        }
    }
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    func generateImageWithText(text: String) -> UIImage
    {
        let image = ary_CollectionData.object(at: 0)as! UIImage
        
        let imageView = UIImageView(image: image)
        imageView.backgroundColor = UIColor.clear
        
         imageView.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        
        let label = UILabel(frame: CGRect(x: 0, y: imageView.frame.size.height - 100.0, width: imageView.frame.size.width, height: 100))
        label.backgroundColor = UIColor.clear
        label.textAlignment = .right
        label.font = UIFont.boldSystemFont(ofSize: 45.0)

        label.textColor = UIColor.red
        label.text = text
        UIGraphicsBeginImageContextWithOptions(image.size, false, 0);
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imageWithText = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext();
        
        return imageWithText
    }
    // MARK: - --------------API Calling
    // MARK: -
  
    func call_URL_Save_Meter_API(sender : UIButton , imgMeter : UIImage )  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("save_meter_reading", forKey: "request") //1
            dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number") //2
            dictData.setValue("\(strMeterNumber)", forKey: "meter_number") //3
            dictData.setValue(txtReading.text!, forKey: "reading") //4
           
            print(dictData)
            //CNG/PNG
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            WebService.callAPIWithImage(parameter: dictData, url: URL_Save_meter_reading, image: imgMeter, fileName: "ReadingImage.jpg", withName: "reading_image_sys") { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    if (responce.value(forKey: "error")as! String == "1"){
                        FTIndicator.showNotification(withTitle: alertInfo, message: (responce.value(forKey: "data")as! String))
                      self.navigationController?.popViewController(animated: true)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(responce.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    
}


// MARK: - ----------------UICollectionViewDelegate
// MARK: -

extension AddMeterReadingVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ary_CollectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Reading", for: indexPath as IndexPath) as! ReadingCell
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MMM d, h:mm a"
        let result = formatter.string(from: date)
        let lblDate = UILabel()
        lblDate.frame = CGRect(x: 0, y: 100, width: self.collection_View.frame.width, height: 50)
        lblDate.text = result
        cell.reading_Image.image = ((ary_CollectionData[indexPath.row])as! UIImage)
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.collection_View.frame.size.width), height:(self.collection_View.frame.size.width))
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
   
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
            handleDiDselectViewTap(image: ary_CollectionData[indexPath.row]as! UIImage, tag: indexPath.row)
    }
    
    
    func handleDiDselectViewTap(image : UIImage ,tag : Int) {
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: Alert_Preview, style: .default , handler:{ (UIAlertAction)in
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
            testController.img = image
            self.navigationController?.pushViewController(testController, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default , handler:{ (UIAlertAction)in
     
            self.ary_CollectionData = NSMutableArray()
            self.collection_View.reloadData()
        }))
  
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: {
        })
    }
}
// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -

extension AddMeterReadingVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            if(ary_CollectionData.count != 0){
                ary_CollectionData.replaceObject(at: 0, with: image)
            }else{
                ary_CollectionData.add(image)
            }
            self.collection_View.reloadData()
            self.dismiss(animated: false) {
            }
        } else {
            self.dismiss(animated: false) {
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  false, completion: nil)
    }
   
}

// MARK: - ----------------ReadingCell
// MARK: -

class ReadingCell: UICollectionViewCell {
    @IBOutlet weak var reading_Image: UIImageView!
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension AddMeterReadingVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      

        return  txtFiledValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 12)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
      

    }
}
