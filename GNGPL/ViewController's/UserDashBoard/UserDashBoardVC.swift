//
//  UserDashBoardVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/26/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData

class UserDashBoardVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewHeader1: CardView!

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblBPID: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tvlist: UITableView!
    
    var aryListData = NSMutableArray()
    var strViewComeFrom = String()
     var loader = UIAlertController()
    // MARK: - ----------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        viewHeader1.backgroundColor = hexStringToUIColor(hex: primaryColor)

        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0
        aryListData.add("Balance Security Deposit")
        aryListData.add("View Bills")
        aryListData.add("Pay Bills")
        aryListData.add("Submit Meter Reading")
      //  aryListData.add("Referral Code")
        self.tvlist.reloadData()
        if(getLoginDataFromLocal().count != 0){
            print(getLoginDataFromLocal())
            dict_Login_Data = NSMutableDictionary()
            dict_Login_Data = ((getLoginDataFromLocal() as NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary
            
            if(dict_Login_Data.count != 0){
                
                imgProfile.setImageWith(URL(string: dict_Login_Data.value(forKey: "profile_image_sys")as! String), placeholderImage: UIImage(named: "card_pic"), options: SDWebImageOptions(rawValue: 1), progress: { (proess1, Intproess2) in
                    
                }, completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            
                lblUserName.text = (dict_Login_Data.value(forKey: "full_name")as! String)
                lblBPID.text = (dict_Login_Data.value(forKey: "bp_number")as! String)
            
            }
        }
        
     
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.call_RefreshToken_API()
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.width /  2
        self.imgProfile.clipsToBounds = true
        self.imgProfile.layer.borderWidth = 2.0
        self.imgProfile.layer.borderColor = UIColor.white.cgColor
        self.imgProfile.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration:0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.imgProfile.transform = .identity
            }, completion: nil)
    }
    
    func checkPaymentStatus()  {
        // For Security Alert Check
        
        if(strViewComeFrom == "LOGIN"){
            let payment_status = "\(dict_Login_Data.value(forKey: "payment_status")!)"
            if(payment_status == "1" || payment_status == "3"){ // For Payment Panding
                CheckPaymentConfrmation(status: payment_status)
            }
        }else{
            if(nsud.value(forKey: "GNGPL_RefreshToken") != nil){
                let dict = nsud.value(forKey: "GNGPL_RefreshToken")as! NSDictionary
                let payment_status = "\(dict.value(forKey: "payment_status")!)"
                if(payment_status == "1" || payment_status == "3"){ // For Payment Panding
                    CheckPaymentConfrmation(status: payment_status)
                }
            }
        }
    }
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        
        if(strViewComeFrom == "DeshBoardSection"){
            self.navigationController?.popViewController(animated: true)
        }else{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: DashBoardVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }

    }
    
    @IBAction func actionOnNotification(_ sender: UIButton) {
        DispatchQueue.main.async{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "NotificationVC")as! NotificationVC
           
            testController.strComeFrom = "UserDashBoard"
           self.navigationController?.pushViewController(testController, animated: true)

        }
    }
    @IBAction func actionOnProfile(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "UserProfileVC")as! UserProfileVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    
    // MARK: - --------------Extra Function
    // MARK: -
    func CheckPaymentConfrmation(status : String) {
        let alert = UIAlertController(title: alertInfo, message: status == "3" ? Alert_ChequePaymentUnderProgress : Alert_SecurityPayment, preferredStyle: UIAlertController.Style.alert)
        // add the actions (buttons)
        
        if(status != "3"){
            alert.addAction(UIAlertAction (title: "Make Payment", style: .default, handler: { (nil) in
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "MakePaymentScreenVC")as! MakePaymentScreenVC
                self.navigationController?.pushViewController(testController, animated: true)
                
            }))
            
            alert.addAction(UIAlertAction (title: "Referral Code", style: .default, handler: { (nil) in
                if("\(dict_Login_Data.value(forKey: "refferal_code")!)" != ""){
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReferEarnVC")as! ReferEarnVC
                    testController.strComeFrom = "Alert"
                    self.navigationController?.pushViewController(testController, animated: true)
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_ReferCode , viewcontrol: self)
                }
            }))
            alert.addAction(UIAlertAction (title: "DashBoard", style: .default, handler: { (nil) in
                
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: DashBoardVC.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }))
        }else{
            alert.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (nil) in
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
                self.navigationController?.pushViewController(testController, animated: true)
                
            }))
            
        }
      
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_RefreshToken_API()  {
        if !(isInternetAvailable()){
            let alert = UIAlertController(title: alertMessage, message: alertInternet, preferredStyle: UIAlertController.Style.alert)
            // add the actions (buttons)
            
          
                alert.addAction(UIAlertAction (title: "ok", style: .default, handler: { (nil) in
                    self.navigationController?.popViewController(animated: true)
                    
                }))
            self.present(alert, animated: true, completion: nil)
        
        }else{
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
           
            let dictData = NSMutableDictionary()
            dictData.setValue("refresh_tokens", forKey: "request")
            dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
            dictData.setValue(Platform, forKey: "platform")
           dictData.setValue("\(DeviceID)", forKey: "device_id")
            nsud.value(forKey: "GNGPL_FCM_Token") != nil ? dictData.setValue("\(nsud.value(forKey: "GNGPL_FCM_Token")!)", forKey: "device_token") : dictData.setValue("", forKey: "device_token")
            print(dictData)
            WebService.callAPIBYPOST(parameter: dictData, url: URL_refresh_tokens) { (responce, status) in
                print(responce)
                self.loader.dismiss(animated: false) {
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "error")as! String == "1"){
                            nsud.set(dict, forKey: "GNGPL_RefreshToken")
                            self.checkPaymentStatus()
                            if("\(dict.value(forKey: "login_status")!)" == "2"){
                                let alert = UIAlertController(title: alertMessage, message: "\(dict.value(forKey: "data")!)", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "LOGIN", style: .default, handler: { action in
                                    deleteAllRecords(strEntity:"ContactData")
                                    deleteAllRecords(strEntity:"AboutData")
                                    deleteAllRecords(strEntity:"TermsCondition")
                                    deleteAllRecords(strEntity:"DropDownData")
                                    deleteAllRecords(strEntity:"Dashboard")
                                    deleteAllRecords(strEntity:"SavingCalculator")
                                    deleteAllRecords(strEntity:"Queries")
                                    deleteAllRecords(strEntity:"TermsConditionReferCode")
                                    deleteAllRecords(strEntity:"LoginData")
                                    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                                    self.navigationController?.pushViewController(testController, animated: true)
                                }))
                                
                                self.present(alert, animated: true)
                            }
                        }
                    }
                }
             
            }
        }
    }
    
    
    // MARK: - --------------Local Data Base
    // MARK: -
    func getLoginDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "LoginData", strkey: "logindata")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "logindata") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension UserDashBoardVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "UserDashBoard", for: indexPath as IndexPath) as! UserDashBoardCell
        cell.lblTitle.text = "\(aryListData[indexPath.row])"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            DispatchQueue.main.async{
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "BalanceSecurityDepositeVC")as! BalanceSecurityDepositeVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }else if (indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3) {
            
            if(nsud.value(forKey: "GNGPL_RefreshToken") != nil){
                let dict = nsud.value(forKey: "GNGPL_RefreshToken")as! NSDictionary
                let meter_number = "\(dict.value(forKey: "meter_number")!)"
                let conn_payment_type = "\(dict.value(forKey: "conn_payment_type")!)"
                if(meter_number != ""){ //
                    
                    if(conn_payment_type == "3"){
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_BalanceSecurityPaymentMSG, viewcontrol: self)
                    }else{
                         if indexPath.row == 1 {
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ViewBillVC")as! ViewBillVC
                            self.navigationController?.pushViewController(testController, animated: true)
                        }
                      else  if (indexPath.row == 2) {
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PayBillVC")as! PayBillVC
                            self.navigationController?.pushViewController(testController, animated: true)
                        }else if indexPath.row == 3 {
                            
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddMeterReadingVC")as! AddMeterReadingVC
                            self.navigationController?.pushViewController(testController, animated: true)
                            
                        }
                    }
                    
                   
            }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_MeterNumber , viewcontrol: self)
            }
            }
        }
        else if indexPath.row == 4 {
            
            if("\(dict_Login_Data.value(forKey: "refferal_code")!)" != ""){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReferEarnVC")as! ReferEarnVC
                self.navigationController?.pushViewController(testController, animated: true)
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_ReferCode , viewcontrol: self)
  
            }
        }
      }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
//    }
}

// MARK: - ----------------UserDashBoardCell
// MARK: -
class UserDashBoardCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
