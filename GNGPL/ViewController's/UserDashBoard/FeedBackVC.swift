//
//  FeedBackVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/7/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData

//MARK:
//MARK: ---------------Protocol-----------------

protocol refreshFeedback_View : class{
    func refreshview(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ----------refreshFeedback_View_Delegate Methods----------

extension FeedBackVC: refreshFeedback_View {
    func refreshview(dictData: NSDictionary, tag: Int) {
        txtType.text = (dictData.value(forKey: "name")as! String)
        txtType.tag = Int("\(dictData.value(forKey: "id")!)")!
    }
}

//MARK:-
//MARK:- ---------Class FeedBackVC---------

class FeedBackVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var txtType: ACFloatingTextfield!
    @IBOutlet weak var txtTitle: ACFloatingTextfield!
    @IBOutlet weak var txtMessage: KMPlaceholderTextView!

    @IBOutlet weak var btnSubmit: UIButton!

    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
         buttonEnable_OnView(tag: 1, btn: btnSubmit)
        txtMessage.borderColor = txtTitle.lineColor
        txtMessage.placeholderColor = txtTitle.placeHolderColor
        txtMessage.textColor = txtTitle.textColor
    }
    
    // MARK: - ---------------Extra Function
    // MARK: -
//    func buttonEnable()  {
//        if(txtType.text?.count != 0 && txtTitle.text?.count != 0){
//            buttonEnable_OnView(tag: 1, btn: btnSubmit)
//        }else{
//            buttonEnable_OnView(tag: 2, btn: btnSubmit)
//        }
//    }
    // MARK: - --------------Validation
    // MARK: -
    func validation()-> Bool{
       if ( txtMessage.text?.count == 0){
            txtMessage.shake()
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtMessage.placeholder) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            
            return false
        }
        return true
    }
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actiononSubmit(_ sender: UIButton) {
        if(validation()){
            call_Feedback_API(sender: sender)
        }
    }
    @IBAction func actiononTypeOfFeedBack(_ sender: UIButton) {
        sender.tag = 31
        let  arylistofData = self.getDropDataFromLocal()as NSMutableArray
        
        print(arylistofData)
        if arylistofData.count != 0 {
          let  dictDropListData = arylistofData.object(at: 0)as! NSDictionary
            let  aryFeedback = (dictDropListData.value(forKey: "feedback_subject") as! NSArray).mutableCopy() as! NSMutableArray
            if aryFeedback.count != 0 {
                self.view.endEditing(true)
                let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
                vc.strTitle = ""
                vc.strTag = sender.tag
               vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    vc.modalTransitionStyle = .coverVertical
                    vc.handleFeedBack_View = self
                    vc.aryTBL = aryFeedback
                    self.present(vc, animated: false, completion: {})
       
            }
        }
            
        }
    // MARK: - --------------Local Data Base
    // MARK: -
    func getDropDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "DropDownData", strkey: "dropdata")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "dropdata") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_Feedback_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("save_feedback", forKey: "request")
            dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
            dictData.setValue("\(txtType.tag)", forKey: "subject")
            dictData.setValue(txtTitle.text, forKey: "title")
            dictData.setValue(txtMessage.text, forKey: "feedback")
          
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            
            WebService.callAPIBYPOST(parameter: dictData, url: URL_Feedback) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        FTIndicator.showNotification(withTitle: alertMessage, message: (dict.value(forKey: "data")as! String))

                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                               self.navigationController?.popViewController(animated: true)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension FeedBackVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 50)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
