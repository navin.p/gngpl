//
//  PayBillVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/16/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class PayBillVC: UIViewController {
    
    // MARK: - ---------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnPayBill: UIButton!
    @IBOutlet weak var lblBPNumber: UILabel!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet weak var lblBillMonths: UILabel!
    @IBOutlet weak var lblMeterNumber: UILabel!

    var dictPayBillData = NSMutableDictionary()
    var strMeterNumber = String()
    // MARK: - ---------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        buttonEnable_OnView(tag: 1, btn: btnPayBill)
      
        lblGrandTotal.text = "\u{20B9} 0.0"
      
        if(nsud.value(forKey: "GNGPL_RefreshToken") != nil){
            let dict = nsud.value(forKey: "GNGPL_RefreshToken")as! NSDictionary
            strMeterNumber = "\(dict.value(forKey: "meter_number")!)"
        }
        self.call_GetBillData_API()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
 

    }
  
    
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actiononPay(_ sender: UIButton) {
        
            let dictData = NSMutableDictionary()
            dictData.setValue("pay_monthly_bill", forKey: "request")
            dictData.setValue("\(dict_Login_Data.value(forKey: "application_id")!)", forKey: "application_id")
            dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
            dictData.setValue("\(strMeterNumber)", forKey: "meter_number")
            dictData.setValue("\(self.dictPayBillData.value(forKey: "grand_total")!)", forKey: "total_amount")
            dictData.setValue("\(self.dictPayBillData.value(forKey: "bill_id")!)", forKey: "bill_id")
            dictData.setValue("\(self.dictPayBillData.value(forKey: "connection_emi_amount")!)", forKey: "connection_emi_amount")
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentModeSelectionVC")as! PaymentModeSelectionVC
            testController.strComeFrom = "PayBill"
            testController.dictPaymentData = dictData
            self.navigationController?.pushViewController(testController, animated: true)

      
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_GetBillData_API()  {
        if !(isInternetAvailable()){
            self.showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("get_bill_details", forKey: "request")
            dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
            dictData.setValue("\(strMeterNumber)", forKey: "meter_number")
       
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_Get_bill_details) { (responce, status) in
                print(responce)
                remove_dotLoader(controller: self)
                removeErrorView()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        self.dictPayBillData = NSMutableDictionary()
                        self.dictPayBillData =  (dict.value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        self.setDataOnBill()
                    }else{
                        self.showErrorWithImage( strTitle: "\(dict.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }

    // MARK: - ---------------Extra Function
    // MARK: -
    func setDataOnBill(){
        lblBillMonths.text = "Bill Months - \(self.dictPayBillData.value(forKey: "bill_month")!)"
        lblBPNumber.text = "BP Number - \(self.dictPayBillData.value(forKey: "bp_number")!)"
        lblMeterNumber.text = "Meter Number - \(self.dictPayBillData.value(forKey: "meter_number")!)"
        lblGrandTotal.text = "\u{20B9} \(self.dictPayBillData.value(forKey: "grand_total")!)"
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        call_GetBillData_API()
    }
    
}
