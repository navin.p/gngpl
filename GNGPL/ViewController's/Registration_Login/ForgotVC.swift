//
//  ForgotVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/14/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class ForgotVC: UIViewController {
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var txtBPnumber: ACFloatingTextfield!

    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)

      
        buttonEnable_OnView(tag: 1, btn: btnSubmit)

        // Do any additional setup after loading the view.
    }
    

    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        if(validation()){
           call_ForgotPassword_API(sender: sender)
        }
    }
    
    // MARK: - --------------Validation
    // MARK: -
    func validation()-> Bool{
        if txtBPnumber.text?.count == 0 {
            
            txtBPnumber.showErrorWithText(errorText: "")
            txtBPnumber.errorLineColor = UIColor.red

            return false
        }
        return true
    }
    

    // MARK: - ---------------API's Calling
    // MARK: -
    
    func call_ForgotPassword_API(sender : UIButton)  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("forgot_password", forKey: "request")
            dictData.setValue(txtBPnumber.text!, forKey: "bp_number")
            
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_Forgot_password) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    print(responce)
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        FTIndicator.showNotification(withTitle: alertMessage, message: (dict.value(forKey: "msg")as! String))

                        let dictData  = (dict.value(forKey: "data")as! NSDictionary)
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "OTPVarifyVC")as! OTPVarifyVC
                        testController.dictData =  (dictData ).mutableCopy() as! NSMutableDictionary
                        testController.strViewComeFrom = "Forgot Password"
                        self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension ForgotVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 15)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
