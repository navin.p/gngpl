//
//  LoginVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/12/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class LoginVC: UIViewController {

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var tfCustomerID: ACFloatingTextfield!
    @IBOutlet weak var tfPassword: ACFloatingTextfield!
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var applyNewConnection: UIButton!

    // MARK: - --------------LifeCycle
    // MARK: -
   
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        applyNewConnection.backgroundColor = hexStringToUIColor(hex: primaryColor)
        let swipeFromRight = UISwipeGestureRecognizer(target: self, action: #selector(didSwipeLeft))
        swipeFromRight.direction = UISwipeGestureRecognizer.Direction.left
        self.viewDetail.addGestureRecognizer(swipeFromRight)
        buttonEnable_OnView(tag: 1, btn: btnLogin)
        buttonEnable_OnView(tag: 1, btn: applyNewConnection)
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
  
    @IBAction func actionOnRegistration(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "RegisterVC")as! RegisterVC
            self.navigationController?.pushViewController(testController, animated: false)
    }
    
    @IBAction func actionOnForgotPassword(_ sender: Any) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "ForgotVC")as! ForgotVC
      self.navigationController?.pushViewController(testController, animated: true)
      // FTIndicator.showToastMessage("Under Development")
    }
    
    @IBAction func actionLoginSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        if(self.validation()){
            self.call_Login_API(sender: sender)
        }
    }
    
    @IBAction func actionOnApplyForNewConnection(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "ApplyForNewConnectionVC")as! ApplyForNewConnectionVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    // MARK: - ---------------Swipe gesture selector function
    // MARK: -
    @objc func didSwipeLeft(gesture: UIGestureRecognizer) {
        //We can add some animation also
        DispatchQueue.main.async(execute: {
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "RegisterVC")as! RegisterVC
            self.navigationController?.pushViewController(testController, animated: false)
            
        })
    }
    
    // MARK: - --------------Validation
    // MARK: -
    func validation()-> Bool{
        if tfCustomerID.text?.count == 0 {
            
            tfCustomerID.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(tfCustomerID.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if ( tfPassword.text?.count == 0){
            
            tfPassword.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(tfPassword.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        return true
    }
//    func buttonEnable()  {
//        if(tfCustomerID.text?.count != 0 && tfPassword.text?.count != 0){
//             buttonEnable_OnView(tag: 1, btn: btnLogin)
//        }else{
//            buttonEnable_OnView(tag: 2, btn: btnLogin)
//        }
//    }
    // MARK: - ---------------API's Calling
    // MARK: -

    
    func call_Login_API(sender : UIButton)  {
        if !(isInternetAvailable()){
           FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("login_api", forKey: "request")
            dictData.setValue(tfCustomerID.text!, forKey: "bp_number")
            dictData.setValue(tfPassword.text, forKey: "password")
            dictData.setValue(Platform, forKey: "platform")
            dictData.setValue("\(DeviceID)", forKey: "device_id")
            nsud.value(forKey: "GNGPL_FCM_Token") != nil ? dictData.setValue("\(nsud.value(forKey: "GNGPL_FCM_Token")!)", forKey: "device_token") : dictData.setValue("", forKey: "device_token")
            
            dotLoader(sender: sender , controller : self, viewContain: viewDetail, onView: "Button")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_login) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    print(responce)
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let dictData  = (dict.value(forKey: "data")as! NSDictionary)
                        let arySaveData = NSMutableArray()
                        arySaveData.add(dictData)
                        deleteAllRecords(strEntity:"LoginData")
                        saveDataInLocalArray(strEntity: "LoginData", strKey: "logindata", data: arySaveData)
                        
                        if(self.getLoginDataFromLocal().count != 0){
                            print(self.getLoginDataFromLocal())
                            dict_Login_Data = NSMutableDictionary()
                            dict_Login_Data = ((self.getLoginDataFromLocal() as NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        }
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "UserDashBoardVC")as! UserDashBoardVC
                        testController.strViewComeFrom = "LOGIN"
                        self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    // MARK: - --------------Local Data Base
    // MARK: -
    func getLoginDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "LoginData", strkey: "logindata")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "logindata") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension LoginVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as! ACFloatingTextfield).errorLineColor = UIColor.lightGray
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == tfCustomerID){
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)

        }else if (textField == tfPassword){
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 15)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
}
