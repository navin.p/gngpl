//
//  OTPVarifyVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/14/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class OTPVarifyVC: UIViewController {
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var tf1: UITextField!
    @IBOutlet weak var tf2: UITextField!
    @IBOutlet weak var tf3: UITextField!
    @IBOutlet weak var tf4: UITextField!
    @IBOutlet weak var tf5: UITextField!
    @IBOutlet weak var tf6: UITextField!
    
    @IBOutlet weak var lblShowCounter: UILabel!
    @IBOutlet weak var lblClckHere: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet weak var btnresend: UIButton!
    
    @IBOutlet weak var viewHeader: CardView!

    var dictData = NSMutableDictionary()
    var count = 60
    var timer = Timer()
    var strViewComeFrom = String()
    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
      
//        tf1.layer.borderColor = UIColor.gray.cgColor
//        tf1.layer.borderWidth = 1.0
//        
//        tf2.layer.borderColor = UIColor.gray.cgColor
//        tf2.layer.borderWidth = 1.0
//        
//        tf3.layer.borderColor = UIColor.gray.cgColor
//        tf3.layer.borderWidth = 1.0
//        
//        tf4.layer.borderColor = UIColor.gray.cgColor
//        tf4.layer.borderWidth = 1.0
//        
//        tf5.layer.borderColor = UIColor.gray.cgColor
//        tf5.layer.borderWidth = 1.0
//        
//        tf6.layer.borderColor = UIColor.gray.cgColor
//        tf6.layer.borderWidth = 1.0
        lblShowCounter.text = "00" + ":" + "00"
        lblClckHere.isHidden = true
        btnresend.isHidden = true
        lblShowCounter.font = lblShowCounter.font.withSize(30)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounterValue), userInfo: nil, repeats: true)
        var myMutableString1 = NSMutableAttributedString()
        let attrs1 = [NSAttributedString.Key.font : lbltitle.font, NSAttributedString.Key.foregroundColor : UIColor.gray]
        
        myMutableString1 = NSMutableAttributedString(string: lbltitle.text!, attributes:attrs1 as [NSAttributedString.Key : Any])
        myMutableString1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location: 10, length: 11))
        lbltitle.attributedText = myMutableString1
        buttonEnable_OnView(tag: 1, btn: btnSubmit)
    }
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        if tf1.text?.count != 0 && tf2.text?.count != 0 && tf3.text?.count != 0 && tf4.text?.count != 0 && tf5.text?.count != 0 && tf6.text?.count != 0 {
            if self.strViewComeFrom == "Forgot Password"{
                self.call_OTPVerifyFor_ForgotPassword_API(sender: sender)
            }else{
                self.call_OTPVerify_API(sender: sender)
            }
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_OTP, viewcontrol: self)
        }
    }
    
    @IBAction func actionOnResendSMS(_ sender: Any) {
        if self.strViewComeFrom == "Forgot Password"{
            self.call_ResendOTP_ForForgotPassword_API()
        }else{
            self.call_ResendOTP_API()
        }
    }
    
    @objc func updateCounterValue() {
        if(count > 0){
            count = count - 1
            var minutes = String(count / 60)
            var seconds = String(count % 60)
            if count % 60 < 10{
                seconds = "0" + seconds
            }
            if count / 60 < 10{
                minutes = "0" + minutes
            }
            lblShowCounter.text = minutes + ":" + seconds
            lblClckHere.isHidden = true
            btnresend.isHidden = true
            lblShowCounter.font = lblShowCounter.font.withSize(30)
            
        }else{
            lblShowCounter.text = "If you did not received OTP then"
            lblClckHere.isHidden = false
            btnresend.isHidden = false
            lblShowCounter.font = lblShowCounter.font.withSize(15)
            timer.invalidate()
            var myMutableString2 = NSMutableAttributedString()
            let attrs2 = [NSAttributedString.Key.font : lblShowCounter.font, NSAttributedString.Key.foregroundColor : UIColor.gray]
            myMutableString2 = NSMutableAttributedString(string: lblShowCounter.text!, attributes:attrs2 as [NSAttributedString.Key : Any])
            myMutableString2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location: 24, length: 3))
            lblShowCounter.attributedText = myMutableString2
            
        }
        
    }
    
    
    
    
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_OTPVerifyFor_ForgotPassword_API(sender : UIButton)  {
        
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let strOTP1 = "\(tf1.text!)" + "\(tf2.text!)" + "\(tf3.text!)"
            let strOTP2 =  "\(tf4.text!)" + "\(tf5.text!)" + "\(tf6.text!)"
            let dictDatasend = NSMutableDictionary()
            dictDatasend.setValue("otp_verification", forKey: "request")
            dictDatasend.setValue("\(dictData.value(forKey: "bp_number")!)", forKey: "bp_number")
            dictDatasend.setValue(strOTP1 + strOTP2, forKey: "verification_otp")
            
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            WebService.callAPIBYPOST(parameter: dictDatasend, url: URL_Forgot_otp_verification) { (responce, status) in
                print(responce)
                remove_dotLoader(controller: self)
                if (status == "success"){
                    
                    let dict  =  removeNullFromDict (dict : (responce.value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let dictData  =  removeNullFromDict (dict : (dict.value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC")as! ChangePasswordVC
                               testController.dictData = dictData
                            testController.strComeFrom = "Forgot Password"
                            self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_ResendOTP_ForForgotPassword_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictDatasend = NSMutableDictionary()
            dictDatasend.setValue("resend_otp", forKey: "request")
            dictDatasend.setValue("\(dictData.value(forKey: "bp_number")!)", forKey: "bp_number")
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callAPIBYPOST(parameter: dictDatasend, url: URL_Resend_otp_pass) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        
                        self.timer = Timer()
                        self.count = 60
                        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounterValue), userInfo: nil, repeats: true)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    // self.viewDetail.isHidden = false
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_OTPVerify_API(sender : UIButton)  {
        
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let strOTP1 = "\(tf1.text!)" + "\(tf2.text!)" + "\(tf3.text!)"
            let strOTP2 =  "\(tf4.text!)" + "\(tf5.text!)" + "\(tf6.text!)"

            let dictDatasend = NSMutableDictionary()
            dictDatasend.setValue("otp_verification", forKey: "request")
            dictDatasend.setValue("\(dictData.value(forKey: "application_id")!)", forKey: "application_id")
            dictDatasend.setValue(strOTP1 + strOTP2, forKey: "verification_otp")
            if self.strViewComeFrom == "Register"{
                dictDatasend.setValue("2", forKey: "type")
            }else if (self.strViewComeFrom == "Connection"){
                dictDatasend.setValue("1", forKey: "type")
            }else if (self.strViewComeFrom == "Forgot Password"){
                dictDatasend.setValue("3", forKey: "type")
            }
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            WebService.callAPIBYPOST(parameter: dictDatasend, url: URL_Otp_Verify) { (responce, status) in
                print(responce)
                remove_dotLoader(controller: self)
                if (status == "success"){
                    
                    let dict  =  removeNullFromDict (dict : (responce.value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let dictData  =  removeNullFromDict (dict : (dict.value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary)

                        if(self.strViewComeFrom == "Connection"){
                            // Save Apply new Connection Data in Local DataBase
                            nsud.set(dict, forKey: "GNGPL_ApplyNewConnectionData")
                            nsud.synchronize()
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ThankyouVC")as! ThankyouVC
                            testController.dictDataForShowMessage = dict.mutableCopy() as! NSMutableDictionary
                            
                            testController.strTitle = "Thank You"
                             self.navigationController?.pushViewController(testController, animated: false)
                        }
                        else if self.strViewComeFrom == "Register"{
                            nsud.set(dictData, forKey: "GNGPL_Registration")
                            nsud.synchronize()
                            if("\(dict.value(forKey: "progress_status")!)" == "0"){
                                let testController = mainStoryboard.instantiateViewController(withIdentifier: "SetProfileVC")as! SetProfileVC
                                self.navigationController?.pushViewController(testController, animated: false)
                            }else if("\(dict.value(forKey: "progress_status")!)" == "1"){
                                let testController = mainStoryboard.instantiateViewController(withIdentifier: "UploadDocumentVC")as! UploadDocumentVC
                                self.navigationController?.pushViewController(testController, animated: false)
                            }
//                            else if("\(dict.value(forKey: "progress_status")!)" == "2"){
//                                let testController = mainStoryboard.instantiateViewController(withIdentifier: "MakePaymentScreenVC")as! MakePaymentScreenVC
//                                self.navigationController?.pushViewController(testController, animated: false)
//                            }
  
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func call_ResendOTP_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictDatasend = NSMutableDictionary()
            dictDatasend.setValue("resend_otp", forKey: "request")
            dictDatasend.setValue("\(dictData.value(forKey: "application_id")!)", forKey: "application_id")
            if self.strViewComeFrom == "Register"{
                dictDatasend.setValue("2", forKey: "type")
            }
            else if (self.strViewComeFrom == "Connection"){
                dictDatasend.setValue("1", forKey: "type")
            }
           
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callAPIBYPOST(parameter: dictDatasend, url: URL_resend_otp) { (responce, status) in
                remove_dotLoader(controller: self)
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        //let dictData  = (dict.value(forKey: "data")as! NSDictionary)
                        self.timer = Timer()
                        self.count = 60
                        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounterValue), userInfo: nil, repeats: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    // self.viewDetail.isHidden = false
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
//    func buttonEnable()  {
//         if tf1.text?.count != 0 && tf2.text?.count != 0 && tf3.text?.count != 0 && tf4.text?.count != 0 && tf5.text?.count != 0 && tf6.text?.count != 0{
//            buttonEnable_OnView(tag: 1, btn: btnSubmit)
//        }else{
//            buttonEnable_OnView(tag: 2, btn: btnSubmit)
//        }
//    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension OTPVarifyVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
        }
            if textField == tf1 {
                tf1.text = string
                if tf1.text?.count == 1 {
                    tf2.becomeFirstResponder()
                }
            }
            if textField == tf2 {
                tf2.text = string
                if tf2.text?.count == 1 {
                    tf3.becomeFirstResponder()
                }
            }
            
            if textField == tf3 {
                tf3.text = string
                if tf3.text?.count == 1 {
                    tf4.becomeFirstResponder()
                }
            }
            if textField == tf4 {
                tf4.text = string
                if tf4.text?.count == 1 {
                    tf5.becomeFirstResponder()
                }
            }
            if textField == tf5 {
                tf5.text = string
                if tf5.text?.count == 1 {
                    tf6.becomeFirstResponder()
                }
            }
            if textField == tf6 {
                tf6.text = string
                if tf6.text?.count == 1 {
                    self.view.endEditing(true)
                    if tf1.text?.count != 0 && tf2.text?.count != 0 && tf3.text?.count != 0 && tf4.text?.count != 0 && tf5.text?.count != 0 && tf6.text?.count != 0 {
                       // self.call_OTPVerify_API(sender: btnSubmit)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_OTP, viewcontrol: self)
                    }
                }
            }
        
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 0)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
}
