//
//  RegisterVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/12/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {
    
    
    @IBOutlet weak var txtApplicationNumber: ACFloatingTextfield!
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var txtReferCode: ACFloatingTextfield!
    
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var viewDetail: UIView!
    
    @IBOutlet weak var viewHeader: CardView!
    var dictRegisterData = NSMutableDictionary()
    // MARK: - --------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        //Swipe gesture for left and right
        let swipeFromLeft = UISwipeGestureRecognizer(target: self, action: #selector(didSwipeRight))
        swipeFromLeft.direction = UISwipeGestureRecognizer.Direction.right
        self.viewDetail.addGestureRecognizer(swipeFromLeft)
        if nsud.value(forKey: "GNGPL_ApplyNewConnectionData") != nil{
            let dict = nsud.value(forKey:"GNGPL_ApplyNewConnectionData")as! NSDictionary
            dictRegisterData = NSMutableDictionary()
            dictRegisterData  = (dict.value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary
            if(dictRegisterData.count != 0){
                txtMobileNumber.text = "\(dictRegisterData.value(forKey: "mobile_number")!)"
                txtApplicationNumber.text = "\(dictRegisterData.value(forKey: "application_id")!)"
            }
        }
        buttonEnable_OnView(tag: 1, btn: btnRegister)
    }
    
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func actionOnLogin(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
        self.navigationController?.pushViewController(testController, animated: false)
    }
    
    @IBAction func actionApplyForNewConnection(_ sender: Any) {
        DispatchQueue.main.async{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ApplyForNewConnectionVC")as! ApplyForNewConnectionVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    
    @IBAction func actionOnRegisterData(_ sender: UIButton) {
        self.view.endEditing(true)

        if (validation()){
            call_Registration_API(sender: sender)
        }
      
    }
    @IBAction func actionOnTermsAndConditions(_ sender: Any) {
        DispatchQueue.main.async{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AboutVC")as! AboutVC
            testController.strViewComeFrom = "Terms and Conditions"
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
    }
    @IBAction func actionOnReferCodeDetail(_ sender: Any) {
      showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: Alert_referral, viewcontrol: self)
    }
    
    // MARK: - --------------Validation
    // MARK: -
    
    func validation()-> Bool{
        if txtApplicationNumber.text?.count == 0 {
            txtApplicationNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtApplicationNumber.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if ( txtMobileNumber.text?.count == 0){
            txtMobileNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtMobileNumber.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)

            return false
        }else if ( (txtMobileNumber.text?.count)! <= 9){
            txtMobileNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_Mobile_limit, viewcontrol: self)

            return false
        }
        return true
    }
//    func buttonEnable()  {
//        if(txtApplicationNumber.text?.count != 0 && txtMobileNumber.text?.count != 0){
//             buttonEnable_OnView(tag: 1, btn: btnRegister)
//        }else{
//               buttonEnable_OnView(tag: 2, btn: btnRegister)
//        }
//    }
    // MARK: - ---------------Swipe gesture selector function
    // MARK: -
    
    @objc func didSwipeRight(gesture: UIGestureRecognizer) {
        // Add animation here
        DispatchQueue.main.async(execute: {
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
            self.navigationController?.pushViewController(testController, animated: false)
        })
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_Registration_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("register", forKey: "request")
            dictData.setValue(txtApplicationNumber.text!, forKey: "application_id")
            dictData.setValue(txtMobileNumber.text!, forKey: "mobile_number")
            dictData.setValue(txtReferCode.text!, forKey: "refferal_code")
            
            dictData.setValue(Platform, forKey: "platform")
            dictData.setValue("\(DeviceID)", forKey: "device_id")
            
            nsud.value(forKey: "GNGPL_FCM_Token") != nil ? dictData.setValue("\(nsud.value(forKey: "GNGPL_FCM_Token")!)", forKey: "device_token") : dictData.setValue("", forKey: "device_token")
            
            dotLoader(sender: sender , controller : self, viewContain: viewDetail, onView: "Button")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_register) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let dictData  = (dict.value(forKey: "data")as! NSDictionary)
                        FTIndicator.showNotification(withTitle: alertInfo, message: (dict.value(forKey: "msg")as! String))
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "OTPVarifyVC")as! OTPVarifyVC
                            testController.dictData =  (dictData ).mutableCopy() as! NSMutableDictionary
                            testController.strViewComeFrom = "Register"
                            self.navigationController?.pushViewController(testController, animated: true)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension RegisterVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtMobileNumber  {
            return  txtFiledValidation(textField: txtMobileNumber, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        if textField == txtApplicationNumber  {
            return  txtFiledValidation(textField: txtApplicationNumber, string: string, returnOnly: "ALL", limitValue: 15)
        }
        if textField == txtReferCode  {
            return  txtFiledValidation(textField: txtApplicationNumber, string: string, returnOnly: "ALL", limitValue: 15)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
