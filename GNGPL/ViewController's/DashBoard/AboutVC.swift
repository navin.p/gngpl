//
//  AboutVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/11/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import WebKit
import CoreData

class AboutVC: UIViewController {

    @IBOutlet weak var viewDetail: UIView!

    
    @IBOutlet weak var imgViewAbout: UIImageView!
    @IBOutlet weak var view_ForHeader: CardView!
    @IBOutlet weak var view_ForImage: UIView!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var heightConstant: NSLayoutConstraint!

    @IBOutlet weak var viewWeb: UIWebView!

    
    
    var strViewComeFrom = NSString()
    var dict_Data = NSDictionary()

    

    // MARK: - --------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)

        lblTitle.text = strViewComeFrom as String
        if(strViewComeFrom == "About us"){
            imgViewAbout.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            
            UIView.animate(withDuration:1.5,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 6.0,
                           options: .allowUserInteraction,
                           animations: { [weak self] in
                            self?.imgViewAbout.transform = .identity
                            
                },
                           completion: nil)
            
        }
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view_ForImage.layer.borderWidth = 0.0
        imgViewAbout.isHidden = false
        if(strViewComeFrom == "About us"){
            
            if(getTerms_ConditionDataFromLocal(strEntity: "AboutData", strkey: "about").count != 0){
                var dataAbout = NSMutableDictionary()
                dataAbout = ((getTerms_ConditionDataFromLocal(strEntity: "AboutData", strkey: "about")as NSArray).object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                
                self.viewWeb.loadHTMLString("\(dataAbout.value(forKey: "text")!)", baseURL: nil)
            
            }else{
                self.call_AboutUs_API()
            }
            self.heightConstant.constant = 175.0
            
        }else if (strViewComeFrom == "Terms and Conditions"){
            
            if(getTerms_ConditionDataFromLocal(strEntity: "TermsCondition", strkey: "termscondition").count != 0){
                print(getTerms_ConditionDataFromLocal(strEntity: "TermsCondition", strkey: "termscondition"))
                
                self.viewWeb.loadHTMLString("\((getTerms_ConditionDataFromLocal(strEntity: "TermsCondition", strkey: "termscondition").object(at: 0) as AnyObject).value(forKey: "text")!)", baseURL: nil)

            }else{
               self.call_TermsAndConditions_API()
            }
            self.heightConstant.constant = 10.0
        }
        else if (strViewComeFrom == "Terms and Conditions "){ // Space for identify view Come From Refrel Earn Screen
            self.viewWeb.loadHTMLString("\(dict_Data.value(forKey: "text")!)", baseURL: nil)
            self.heightConstant.constant = 10.0
        }
        
        else if (strViewComeFrom == "Help"){
            self.heightConstant.constant = 10.0
        }
        else{
            self.view_ForImage.layer.borderWidth = 5.0
            self.heightConstant.constant = 10.0
            
            self.viewWeb.loadHTMLString("\(dict_Data.value(forKey: "description")!)", baseURL: nil)

           // self.viewWeb.loadHTMLString("\(dict_Data.value(forKey: "description")!)", baseURL: nil)

        }
        self.view_ForImage.layer.cornerRadius =  self.view_ForImage.frame.height / 2
    
    }
    
    override func viewWillLayoutSubviews() {
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    
    // MARK: - --------------Local Data Base
    // MARK: -
    func getTerms_ConditionDataFromLocal(strEntity: String ,strkey : String )-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: strEntity, strkey: strkey)

        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: strkey) ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }


    // MARK: - ---------------API's Calling
    // MARK: -
    func call_AboutUs_API()  {
        if !(isInternetAvailable()){
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
           //----1
            dotLoader(sender: UIButton() , controller : self, viewContain: viewDetail, onView: "MainView")
          
            //----2
            let dictData = NSMutableDictionary()
            dictData.setValue("about_us", forKey: "request")
            
            WebService.callAPIBYPOST(parameter: dictData, url: URL_about_us) { (responce, status) in
                removeErrorView()
               remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let dictData  = (dict.value(forKey: "data")as! NSDictionary)
                          let arySaveData = NSMutableArray()
                        arySaveData.add(dictData)
                        deleteAllRecords(strEntity:"AboutData")
                        saveDataInLocalArray(strEntity: "AboutData", strKey: "about", data: arySaveData)
                        self.viewWeb.loadHTMLString("\(dictData.value(forKey: "text")!)", baseURL: nil)
                    }else{
                        self.showErrorWithImage(strTitle: "\(dict.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)
                    }

                }else{
                    self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)

                }
            }
        }
    }
    func call_TermsAndConditions_API()  {
        if !(isInternetAvailable()){
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)

        }else{
         
            //----1
            dotLoader(sender: UIButton() , controller : self, viewContain: viewDetail, onView: "MainView")
            //---2
            let dictData = NSMutableDictionary()
            dictData.setValue("terms_condition", forKey: "request")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_terms_conditions) { (responce, status) in
               remove_dotLoader(controller: self)
                removeErrorView()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let dictData  = (dict.value(forKey: "data")as! NSDictionary)
                        
                        let arySaveData = NSMutableArray()
                        arySaveData.add(dictData)
                        deleteAllRecords(strEntity:"TermsCondition")
                        saveDataInLocalArray(strEntity: "TermsCondition", strKey: "termscondition", data: arySaveData)
                        self.viewWeb.loadHTMLString("\(dictData.value(forKey: "text")!)", baseURL: nil)

                    }else{
                        self.showErrorWithImage(strTitle: "\(responce.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)
                    }
                    
                }else{
                    self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    

    
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
}
// MARK :
//MARK : UIWebViewDelegate
extension AboutVC : UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        remove_dotLoader(controller: self)
        
        
        
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        remove_dotLoader(controller: self)
    }
}
