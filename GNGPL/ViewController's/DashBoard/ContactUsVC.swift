//
//  ContactUsVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/11/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
import MessageUI

class ContactUsVC: UIViewController {
    
    
    // MARK: - --------------IBOutlet
    // MARK: -
    
    @IBOutlet weak var tvlist: UITableView!

    @IBOutlet weak var btnTollfreeNumber: UIButton!
  
    @IBOutlet weak var view_ForHeader: CardView!
    
    var dictContactData = NSMutableDictionary()
    let arrysections = ["Head Office","Counters","Address"]

    var aryHeadOffice = NSMutableArray()
    var aryCounter = NSMutableArray()
    var aryAddress = NSMutableArray()

    // MARK: - --------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        btnTollfreeNumber.backgroundColor = UIColor.white
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 50.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
     btnTollfreeNumber.backgroundColor = UIColor.white
        if(getContactDataFromLocal().count != 0){
            print(getContactDataFromLocal())
            dictContactData = NSMutableDictionary()
            dictContactData = (getContactDataFromLocal().object(at: 0) as AnyObject).mutableCopy() as! NSMutableDictionary
            self.setDataOnView(dictData: dictContactData)

        }else{
            self.call_ContactUs_API()
        }
    }

    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
       
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func actionOnTollFree(_ sender: UIButton) {
        let strNumber = "\((sender.titleLabel?.text!)!)".replacingOccurrences(of: "Toll Free - ", with: "")
        if strNumber.count > 7 {
            if callingFunction(number: strNumber as NSString){
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
            }
        }
    }
 
    
    // MARK: - --------------Local Data Base
    // MARK: -
    func getContactDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "ContactData", strkey: "contact")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "contact") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_ContactUs_API()  {
        if !(isInternetAvailable()){
            self.showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)

        }else{
      
            //----1
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
           //---2
            let dictData = NSMutableDictionary()
            dictData.setValue("contact_us", forKey: "request")
            
            WebService.callAPIBYPOST(parameter: dictData, url: URL_contact_us) { (responce, status) in
                remove_dotLoader(controller: self)
                removeErrorView()
                
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let dictData  = (dict.value(forKey: "data")as! NSDictionary)
                        
                        let arySaveData = NSMutableArray()
                        arySaveData.add(dictData)
                        deleteAllRecords(strEntity:"ContactData")
                        saveDataInLocalArray(strEntity: "ContactData", strKey: "contact", data: arySaveData)
                        self.setDataOnView(dictData: dictData)
                    }else{
                        self.showErrorWithImage( strTitle: "\(dict.value(forKey: "Success")as! String)", imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    // MARK: - ---------------Data Set On View
    // MARK: -
    func setDataOnView(dictData : NSDictionary)  {
        // TollFree
        if(dictData.count == 0){
               self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
        }
        var strTollFree = "\(dictData.value(forKey: "toll_free_number")!)"
        if(strTollFree == ""){
            strTollFree = "N/A"
        }
        btnTollfreeNumber.setTitle("Toll Free - \(strTollFree)", for: .normal)
      btnTollfreeNumber.backgroundColor = hexStringToUIColor(hex: orangeColor)
        //Head Office
        aryHeadOffice = NSMutableArray()
        let dictHeadOffice = NSMutableDictionary()
        dictHeadOffice.setValue("\(dictData.value(forKey: "customer_support")!)", forKey: "number")
        dictHeadOffice.setValue("Customer Care", forKey: "title")

        let dictHeadOffice1 = NSMutableDictionary()
        dictHeadOffice1.setValue("\(dictData.value(forKey: "billing_inquiries")!)", forKey: "number")
        dictHeadOffice1.setValue("Billing Inquiry", forKey: "title")
        
        let dictHeadOffice2 = NSMutableDictionary()
        dictHeadOffice2.setValue("\(dictData.value(forKey: "connection_inquiries")!)", forKey: "number")
        dictHeadOffice2.setValue("New Connection request", forKey: "title")
        
        let dictHeadOffice3 = NSMutableDictionary()
        dictHeadOffice3.setValue("\(dictData.value(forKey: "emergency_fire_number")!)", forKey: "number")
        dictHeadOffice3.setValue("Emergency in case of Fire/Leakage", forKey: "title")
        
        let dictHeadOffice4 = NSMutableDictionary()
        dictHeadOffice4.setValue("\(dictData.value(forKey: "email")!)", forKey: "number")
        dictHeadOffice4.setValue("Email", forKey: "title")
        
        aryHeadOffice.add(dictHeadOffice)
        aryHeadOffice.add(dictHeadOffice1)
        aryHeadOffice.add(dictHeadOffice2)
        aryHeadOffice.add(dictHeadOffice3)
        aryHeadOffice.add(dictHeadOffice4)

        //Counter Office
        aryCounter = NSMutableArray()

        let dictCounter = NSMutableDictionary()
        dictCounter.setValue("\(dictData.value(forKey: "counter_1")!)", forKey: "number")
        dictCounter.setValue("Counter - 1", forKey: "title")
        
        let dictCounter1 = NSMutableDictionary()
        dictCounter1.setValue("\(dictData.value(forKey: "counter_2")!)", forKey: "number")
        dictCounter1.setValue("Counter - 2", forKey: "title")
        
        let dictCounter2 = NSMutableDictionary()
        dictCounter2.setValue("\(dictData.value(forKey: "counter_3")!)", forKey: "number")
        dictCounter2.setValue("Counter - 3", forKey: "title")
        
        aryCounter.add(dictCounter)
        aryCounter.add(dictCounter1)
        aryCounter.add(dictCounter2)
        
      //Address Office
        aryAddress = NSMutableArray()
        let dictAddress = NSMutableDictionary()
        dictAddress.setValue("\(dictData.value(forKey: "address")!)", forKey: "number")
        dictAddress.setValue("address", forKey: "title")
        aryAddress.add(dictAddress)
       self.tvlist.reloadData()
        
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
    
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ContactUsVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrysections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return aryHeadOffice.count
        }  else if (section == 1){
             return aryCounter.count
        }else if (section == 2){
            return aryAddress.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath as IndexPath) as! ContactCell
        
        if indexPath.section == 0 {
            let dict = aryHeadOffice.object(at: indexPath.row)as! NSDictionary
            cell.lbl_Title.text = (dict["title"] as! String)
            cell.btn_number.setTitle((dict["number"] as! String) , for: .normal)
            return cell
            
        }
        else if indexPath.section == 1 {
            let dict = aryCounter.object(at: indexPath.row)as! NSDictionary
            cell.lbl_Title.text = (dict["title"] as! String)
            cell.btn_number.setTitle((dict["number"] as! String) , for: .normal)
            return cell
            
        }
        else if indexPath.section == 2 {
            let cell1 = tvlist.dequeueReusableCell(withIdentifier: "ContactCellAddress", for: indexPath as IndexPath) as! ContactCell
            let dict = aryAddress.object(at: indexPath.row)as! NSDictionary
            cell1.lblAddress.text = "\(dict.value(forKey: "number")!)"
            return cell1
        }
      return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let dict = aryHeadOffice.object(at: indexPath.row)as! NSDictionary
            let strNumber = "\((dict["number"] as! String))"
            if(indexPath.row == 4){
                
                 if MFMailComposeViewController.canSendMail() {
                let emailTitle = "GNGPL Support"
                let messageBody = ""
                let toRecipents = [strNumber]
                let mc: MFMailComposeViewController = MFMailComposeViewController()
                mc.mailComposeDelegate = self
                mc.setSubject(emailTitle)
                mc.setMessageBody(messageBody, isHTML: false)
                mc.setToRecipients(toRecipents)
                 self.present(mc, animated: true, completion: nil)
                 }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)

                }
            }else{
                if strNumber.count > 5 {
                    var ary = strNumber.components(separatedBy: ",")
                    if(ary.count > 1){
                        
                        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertController.Style.actionSheet)
                        
                        // add the actions (buttons)
                        for item in ary{
                            alert.addAction(UIAlertAction (title: "\(item)", style: .default, handler: { (nil) in
                                if callingFunction(number: "\(item)" as NSString){
                                }else{
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
                                    
                                }
                            }))
                        }
                        alert.addAction(UIAlertAction (title: "Dismiss", style: .default, handler: { (nil) in
                          
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        if callingFunction(number: strNumber as NSString){
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
                            
                        }
                    }
                    
                    
                    
                    
                }
            }
          
        }
        else if indexPath.section == 1 {
            let dict = aryCounter.object(at: indexPath.row)as! NSDictionary
            let strNumber = "\((dict["number"] as! String))"
            if strNumber.count > 5 {
                if callingFunction(number: strNumber as NSString){
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
                }
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section != 5 {
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
            headerView.backgroundColor = hexStringToUIColor(hex: primaryGrayColor)
            let label = UILabel()
            label.frame = CGRect.init(x: 12, y: 5, width: headerView.frame.width-24, height: headerView.frame.height-10)
            label.text = arrysections[section]
            label.font =  UIFont(name: primaryFontLatoRegular, size: 17.0)
            label.textColor = UIColor.black
            headerView.addSubview(label)
            return headerView
        }else{
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 0))
            return headerView
            
        }
        
    }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
//    }
    
    
}
// MARK: - --------------ContactCell
// MARK: -

class ContactCell: UITableViewCell {
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var btn_number: UIButton!
    @IBOutlet weak var lblAddress: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - --------------MFMailComposeViewControllerDelegate
// MARK: -

extension ContactUsVC : MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
        case MFMailComposeResult.failed.rawValue:
            print("Error: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
