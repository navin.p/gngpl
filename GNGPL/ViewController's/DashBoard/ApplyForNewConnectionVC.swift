//
//  ApplyForNewConnectionVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/14/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
//MARK:
//MARK: ---------------Protocol-----------------

protocol refreshCng_Png_Com_Ind_View : class{
    func refreshviewCng_Png(dictData : NSDictionary ,tag : Int)
    func refreshviewCom_Ind(dictData : NSDictionary ,tag : Int)
    func refreshviewBulider(dictData : NSDictionary ,tag : Int)
    
    func refreshviewSelectApplySection(dictData : NSDictionary ,tag : Int)
    
}
//MARK:-
//MARK:- ----------RefreshCng_Png_Com_Ind_View Delegate Methods----------

extension ApplyForNewConnectionVC: refreshCng_Png_Com_Ind_View {
    func refreshviewSelectApplySection(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        
        if tag == 15 {
            
            txtSelctTypeofConnection.text = (dictData.value(forKey: "name")as! String)
            txtSelctTypeofConnection.tag = Int("\(dictData.value(forKey: "id")!)")!
            selectionType =   txtSelctTypeofConnection.tag
            
            heightSocityName.constant = 0.0
            topSocityName.constant = 0.0
            self.setViewOnScroll()
        }
    }
    
    // 1 == SelectSociety , 2 == Constituency , 3 == city ,
    func refreshviewCng_Png(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        if tag == 0 {
            cng_Png_txtPrefix.text = dictData.value(forKey: "name")as? String
        }
        else if tag == 1 {
            cng_png_txtsociety.text = dictData.value(forKey: "name")as? String
            cng_png_txtsociety.tag = Int("\(dictData.value(forKey: "id")!)")!
            cng_png_txtsocietyNameOther.text = ""
            if(cng_png_txtsociety.tag == 0){
                heightSocityName.constant = 45.0
                topSocityName.constant = 18.0
            }else{
                heightSocityName.constant = 0.0
                topSocityName.constant = 0.0
            }
        }else if tag == 2 {
            cng_png_txtConstituency.text = dictData.value(forKey: "name")as? String
            cng_png_txtConstituency.tag = Int("\(dictData.value(forKey: "id")!)")!
            
            cng_png_txtTaluka.text = dictData.value(forKey: "taluka_name")as? String
            cng_png_txtTaluka.tag = Int("\(dictData.value(forKey: "taluka_id")!)")!
            
            cng_png_txtDistrict.text = dictData.value(forKey: "district_name")as? String
            cng_png_txtDistrict.tag = Int("\(dictData.value(forKey: "districts_id")!)")!
        }
        else if tag == 3 {
            cng_png_txtDistrict.text = dictData.value(forKey: "name")as? String
            cng_png_txtDistrict.tag = Int("\(dictData.value(forKey: "id")!)")!
        }
        else if tag == 4 {
            cng_png_txtTaluka.text = dictData.value(forKey: "name")as? String
            cng_png_txtTaluka.tag = Int("\(dictData.value(forKey: "id")!)")!
        }
    }
    func refreshviewCom_Ind(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        
        // 5 == Prefix  6 == Constituency , 7 == city , 8 == taluka
        if tag == 5 {
            com_ind_txtPrefix.text = dictData.value(forKey: "name")as? String
        }
        else if tag == 6 {
            com_ind_txtConstituency.text = dictData.value(forKey: "name")as? String
            com_ind_txtConstituency.tag = Int("\(dictData.value(forKey: "id")!)")!
            
            com_ind_txtTaluka.text = dictData.value(forKey: "taluka_name")as? String
            com_ind_txtTaluka.tag = Int("\(dictData.value(forKey: "taluka_id")!)")!
            
            com_ind_txtDistrict.text = dictData.value(forKey: "district_name")as? String
            com_ind_txtDistrict.tag = Int("\(dictData.value(forKey: "districts_id")!)")!
            
        }else if tag == 7 {
            com_ind_txtDistrict.text = dictData.value(forKey: "name")as? String
            
            com_ind_txtDistrict.tag = Int("\(dictData.value(forKey: "id")!)")!
            
        }else if tag == 8 {
            com_ind_txtTaluka.text = dictData.value(forKey: "name")as? String
            
            com_ind_txtTaluka.tag = Int("\(dictData.value(forKey: "id")!)")!
            
        }
    }
    
    func refreshviewBulider(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        
        // 9 == Prefix  10 == Constituency , 11 == city , 12 == Taluka
        if tag == 9 {
            bulider_txtPrefix.text = dictData.value(forKey: "name")as? String
        }
        else if tag == 10 {
            bulider_txtConstituency.text = dictData.value(forKey: "name")as? String
            bulider_txtConstituency.tag = Int("\(dictData.value(forKey: "id")!)")!
            bulider_txtTaluka.text = dictData.value(forKey: "taluka_name")as? String
            bulider_txtTaluka.tag = Int("\(dictData.value(forKey: "taluka_id")!)")!
            
            bulider_txtDistrict.text = dictData.value(forKey: "district_name")as? String
            bulider_txtDistrict.tag = Int("\(dictData.value(forKey: "districts_id")!)")!
            
        }else if tag == 11 {
            bulider_txtDistrict.text = dictData.value(forKey: "name")as? String
            bulider_txtDistrict.tag = Int("\(dictData.value(forKey: "id")!)")!
        }else if tag == 12 {
            bulider_txtTaluka.text = dictData.value(forKey: "name")as? String
            bulider_txtTaluka.tag = Int("\(dictData.value(forKey: "id")!)")!
        }
    }
}

//MARK: ---------------ApplyForNewConnectionVC-----------------
//MARK:

class ApplyForNewConnectionVC: UIViewController {
    @IBOutlet weak var scrollContain: UIScrollView!
    @IBOutlet weak var txtSelctTypeofConnection: UITextField!
    @IBOutlet weak var viewHeader: CardView!
    
    //-------------CNG/PNG-------------
    @IBOutlet var viewCNGAndPNG: UIView!
    @IBOutlet weak var cng_Png_txtPrefix: ACFloatingTextfield!
    
    @IBOutlet weak var cng_Png_txtFullName: ACFloatingTextfield!
    @IBOutlet weak var cng_png_txtMoileNumber: ACFloatingTextfield!
    @IBOutlet weak var cng_png_txtWhatsappNumber: ACFloatingTextfield!
    @IBOutlet weak var cng_png_txtLandLineNumber: ACFloatingTextfield!
    
    @IBOutlet weak var cng_png_txtsociety: ACFloatingTextfield!
    @IBOutlet weak var cng_png_txtsocietyNameOther: ACFloatingTextfield!
    
    @IBOutlet weak var cng_png_txtConstituency: ACFloatingTextfield!
    @IBOutlet weak var cng_png_txtHouseNumber: ACFloatingTextfield!
    @IBOutlet weak var cng_png_txtStreet: ACFloatingTextfield!
    @IBOutlet weak var cng_png_txtDistrict: ACFloatingTextfield!
    @IBOutlet weak var cng_png_txtState: ACFloatingTextfield!
    @IBOutlet weak var cng_png_txtTaluka: ACFloatingTextfield!
    @IBOutlet weak var cng_png_txtPinCode: ACFloatingTextfield!
    @IBOutlet weak var cng_png_btnSubmit: UIButton!
    
    
    @IBOutlet weak var heightSocityName: NSLayoutConstraint!
    
    @IBOutlet weak var topSocityName: NSLayoutConstraint!
    //-------------Com/Ind-------------
    
    @IBOutlet var viewComAndInd: UIView!
    @IBOutlet weak var com_ind_txtBusinessName: ACFloatingTextfield!
    @IBOutlet weak var com_ind_txtPrefix: ACFloatingTextfield!
    
    @IBOutlet weak var com_ind_txtPoc: ACFloatingTextfield!
    @IBOutlet weak var com_ind_txtMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var com_ind_txtWhatsappNumber: ACFloatingTextfield!
    @IBOutlet weak var com_ind_txtLandLineNumber: ACFloatingTextfield!
    @IBOutlet weak var com_ind_txtConstituency: ACFloatingTextfield!
    @IBOutlet weak var com_ind_txtAddress: ACFloatingTextfield!
    @IBOutlet weak var com_ind_txtTaluka: ACFloatingTextfield!
    @IBOutlet weak var com_ind_txtStreet: ACFloatingTextfield!
    @IBOutlet weak var com_ind_txtState: ACFloatingTextfield!
    
    
    @IBOutlet weak var com_ind_txtDistrict: ACFloatingTextfield!
    @IBOutlet weak var com_ind_txtPinCode: ACFloatingTextfield!
    @IBOutlet weak var com_ind_btnSubmit: UIButton!
    
    //-------------Bulider-------------
    
    @IBOutlet var viewBulider: UIView!
    @IBOutlet weak var bulider_btnSubmit: UIButton!
    @IBOutlet weak var bulider_txtPrefix: ACFloatingTextfield!
    
    @IBOutlet weak var bulider_txtBuliderName: ACFloatingTextfield!
    @IBOutlet weak var bulider_txtContactPerson: ACFloatingTextfield!
    @IBOutlet weak var bulider_txtMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var bulider_txtWhatsappNumber: ACFloatingTextfield!
    @IBOutlet weak var bulider_txtALandLineNumber: ACFloatingTextfield!
    
    
    @IBOutlet weak var bulider_txtNumberOfFlat: ACFloatingTextfield!
    @IBOutlet weak var bulider_txtConstituency: ACFloatingTextfield!
    @IBOutlet weak var bulider_txtAddress: ACFloatingTextfield!
    @IBOutlet weak var bulider_txtStreet: ACFloatingTextfield!
    @IBOutlet weak var bulider_txtDistrict: ACFloatingTextfield!
    @IBOutlet weak var bulider_txtTaluka: ACFloatingTextfield!
    @IBOutlet weak var bulider_txtState: ACFloatingTextfield!
    @IBOutlet weak var bulider_txtPincode: ACFloatingTextfield!
    
    var aryTbl = NSMutableArray()
    var selectionType = 0
    var selectionName = ""
    
    var aryprefix = NSMutableArray()
    var dictDropListData = NSDictionary()
    
    // MARK: - --------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        txtSelctTypeofConnection.tag = selectionType
        txtSelctTypeofConnection.text  = selectionName
        let dictPre = NSMutableDictionary()
        dictPre.setValue("Mr.", forKey: "name")
        aryprefix.add(dictPre)
        let dictPre1 = NSMutableDictionary()
        dictPre1.setValue("Ms.", forKey: "name")
        aryprefix.add(dictPre1)
        let dictPre2 = NSMutableDictionary()
        dictPre2.setValue("Mrs.", forKey: "name")
        aryprefix.add(dictPre2)
        
        var arylistofData = NSMutableArray()
        arylistofData = self.getDropDownDataFromLocal()
        print(arylistofData)
        
        if arylistofData.count != 0 {
            dictDropListData = arylistofData.object(at: 0)as! NSDictionary
        }else{
            self.call_DropDownList_API()
        }
        
        buttonEnable_OnView(tag: 1, btn: com_ind_btnSubmit)
        buttonEnable_OnView(tag: 1, btn: cng_png_btnSubmit)
        buttonEnable_OnView(tag: 1, btn: bulider_btnSubmit)
        
        heightSocityName.constant = 0.0
        topSocityName.constant = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.endEditing(true)
        self.setViewOnScroll()
    }
    override func viewWillLayoutSubviews() {
        
        
    }
    func setViewOnScroll()  {
        viewCNGAndPNG.removeFromSuperview()
        viewComAndInd.removeFromSuperview()
        viewBulider.removeFromSuperview()
        
        if (selectionType == 4 || selectionType == 5){
            viewCNGAndPNG.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewCNGAndPNG.frame.height + 20)
            scrollContain.addSubview(viewCNGAndPNG)
            scrollContain.contentSize = CGSize(width: self.view.frame.size.width, height: viewCNGAndPNG.frame.height)
            
        }
        else  if (selectionType == 6 || selectionType == 7){
            viewComAndInd.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewComAndInd.frame.height + 20)
            scrollContain.addSubview(viewComAndInd)
            scrollContain.contentSize = CGSize(width: self.view.frame.size.width, height: viewComAndInd.frame.height)
            
        }
        else  if (selectionType == 10){
            viewBulider.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: viewBulider.frame.height + 20)
            scrollContain.addSubview(viewBulider)
            scrollContain.contentSize = CGSize(width: self.view.frame.size.width, height: viewBulider.frame.height)
        }
    }
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSelectConnectionType(_ sender: UIButton) {
        sender.tag = 15
        
        var arylistofData = NSMutableArray()
        arylistofData = self.getDropDownDataFromLocal()
        if arylistofData.count != 0 {
            dictDropListData = arylistofData.object(at: 0)as! NSDictionary
        }
        
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "connection_type") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "Select Required Connection")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
    }
    
    // MARK: - --------------IBAction FOR CNG/PNG Form
    // MARK: -
    @IBAction func action_Cng_png_Prefix(_ sender: UIButton) {
        sender.tag = 0
        if dictDropListData.count != 0 {
            var aryname_title = NSMutableArray()
            aryname_title = (dictDropListData.value(forKey: "name_title") as! NSArray).mutableCopy() as! NSMutableArray
            if aryname_title.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryname_title, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
    }
    
    @IBAction func action_Cng_png_SelectSociety(_ sender: UIButton) {
        sender.tag = 1
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "society_or_appartment") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
    }
    
    @IBAction func action_Cng_png_Constituency(_ sender: UIButton) {
        sender.tag = 2
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "constituency") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
    }
    
    @IBAction func action_Cng_png_City(_ sender: UIButton) {
        sender.tag = 3
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "districts") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
    }
    
    @IBAction func actionCng_Png_Taluka(_ sender: UIButton) {
        sender.tag = 4
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "taluka") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
    }
    
    
    @IBAction func action_Cng_png_Submit(_ sender: UIButton) {
        self.view.endEditing(true)
        if validationForCNG_PNG() {
            self.call_URL_Save_connection_request_API(sender: sender, connectionType: txtSelctTypeofConnection.tag)
        }else{
        }
    }
    // MARK: - --------------IBAction FOR COM/IND Form
    // MARK: -
    @IBAction func action_Com_Ind_Prefix(_ sender: UIButton) {
        sender.tag = 5
        if dictDropListData.count != 0 {
            var aryname_title = NSMutableArray()
            aryname_title = (dictDropListData.value(forKey: "name_title") as! NSArray).mutableCopy() as! NSMutableArray
            if aryname_title.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryname_title, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
        
    }
    @IBAction func action_Com_Ind_Constituency(_ sender: UIButton) {
        sender.tag = 6
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "constituency") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "Select Constituency")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
        
    }
    
    @IBAction func action_Com_Ind_City(_ sender: UIButton) {
        sender.tag = 7
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "districts") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
        
    }
    
    @IBAction func action_Com_Ind_Taluka(_ sender: UIButton) {
        sender.tag = 8
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "taluka") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
    }
    @IBAction func action_Com_Ind_Submit(_ sender: UIButton) {
        self.view.endEditing(true)
        if validationForComm_Ind(){
            self.call_URL_Save_connection_request_API(sender: sender, connectionType: txtSelctTypeofConnection.tag)
        }
        else{
            
        }
    }
    // MARK: - --------------IBAction FOR Bulider Form
    // MARK: -
    
    @IBAction func action_Bulider_Prefix(_ sender: UIButton) {
        sender.tag = 9
        if dictDropListData.count != 0 {
            var aryname_title = NSMutableArray()
            aryname_title = (dictDropListData.value(forKey: "name_title") as! NSArray).mutableCopy() as! NSMutableArray
            if aryname_title.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryname_title, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
    }
    
    @IBAction func action_BuliderSelectConstituency(_ sender: UIButton) {
        sender.tag = 10
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "constituency") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
    }
    
    @IBAction func action_Bulider_City(_ sender: UIButton) {
        sender.tag = 11
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "districts") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
        
    }
    @IBAction func action_Bulider_Taluka(_ sender: UIButton) {
        sender.tag = 12
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "taluka") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
    }
    
    @IBAction func action_Bulider_Submit(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if validationForBulider(){
            self.call_URL_Save_connection_request_API(sender: sender, connectionType: txtSelctTypeofConnection.tag)
        }else{
            
        }
    }
    
    // MARK: - --------------Local Data Base
    // MARK: -
    func getDropDownDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "DropDownData", strkey: "dropdata")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "dropdata") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    func call_DropDownList_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("drop_down_list", forKey: "request")
            print(URL_Get_drop_down_list)
            dotLoader(sender: UIButton() , controller : self, viewContain: UIView(), onView: "MainView")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_Get_drop_down_list) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        deleteAllRecords(strEntity:"DropDownData")
                        saveDataInLocalArray(strEntity: "DropDownData", strKey: "dropdata", data: (dict.value(forKey: "data")as! NSArray).mutableCopy() as! NSMutableArray)
                    }else{
                        // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                    //  self.viewContain.isHidden = false
                }else{
                    //  FTIndicator.showToastMessage(alertSomeError)
                }
            }
        }
    }
    // MARK: - --------------Extra Method
    // MARK: -
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
        let vc: PopUpView = mainStoryboard.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleCng_Png_Com_Ind_View = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
    }
    
    
    // MARK: - --------------Validation
    // MARK: -
    
    func validationForCNG_PNG()-> Bool{
        if cng_Png_txtFullName.text?.count == 0 {
            cng_Png_txtFullName.showErrorWithText(errorText: "")
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(cng_Png_txtFullName.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if ( cng_png_txtMoileNumber.text?.count == 0){
            cng_png_txtMoileNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(cng_png_txtMoileNumber.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            
            return false
        }else if ( (cng_png_txtMoileNumber.text?.count)! <= 9){
            cng_png_txtMoileNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_Mobile_limit, viewcontrol: self)
            
            return false
        }
        else if (cng_png_txtWhatsappNumber.text?.count != 0){
            if ( (cng_png_txtWhatsappNumber.text?.count)! <= 9){
                cng_png_txtWhatsappNumber.showErrorWithText(errorText: "")
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_Whatsapp_limit, viewcontrol: self)
                
                return false
            }
        }
        
        if(cng_png_txtLandLineNumber.text?.count != 0){
            if ( (cng_png_txtLandLineNumber.text?.count)! <= 9){
                cng_png_txtLandLineNumber.showErrorWithText(errorText: "")
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_landline_limit, viewcontrol: self)
                return false
            }
        }
        
        if (cng_png_txtsociety.text?.count == 0){
            cng_png_txtsociety.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(cng_png_txtsociety.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            return false
        }
        
        if(cng_png_txtsociety.tag == 0){
            if(cng_png_txtsocietyNameOther.text?.count == 0){
                cng_png_txtsocietyNameOther.showErrorWithText(errorText: "")
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(cng_png_txtsocietyNameOther.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
                return false
            }
        }
        
        if ( cng_png_txtConstituency.text?.count == 0){
            cng_png_txtConstituency.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(cng_png_txtConstituency.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            return false
        }
        else if ( cng_png_txtHouseNumber.text?.count == 0){
            cng_png_txtHouseNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(cng_png_txtHouseNumber.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( cng_png_txtStreet.text?.count == 0){
            cng_png_txtStreet.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(cng_png_txtStreet.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( cng_png_txtTaluka.text?.count == 0){
            cng_png_txtTaluka.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(cng_png_txtTaluka.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( cng_png_txtDistrict.text?.count == 0){
            cng_png_txtDistrict.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(cng_png_txtDistrict.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( cng_png_txtState.text?.count == 0){
            cng_png_txtState.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(cng_png_txtState.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( cng_png_txtPinCode.text?.count == 0){
            cng_png_txtPinCode.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(cng_png_txtPinCode.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ((cng_png_txtPinCode.text?.count)! <= 5){
            cng_png_txtPinCode.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PinCode_limit, viewcontrol: self)
            return false
        }
        
        
        return true
    }
    func validationForComm_Ind()-> Bool{
        if com_ind_txtBusinessName.text?.count == 0 {
            com_ind_txtBusinessName.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(com_ind_txtBusinessName.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( com_ind_txtPoc.text?.count == 0){
            com_ind_txtPoc.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(com_ind_txtPoc.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( com_ind_txtMobileNumber.text?.count == 0){
            com_ind_txtMobileNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(com_ind_txtMobileNumber.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if ( (com_ind_txtMobileNumber.text?.count)! <= 9){
            com_ind_txtMobileNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_Mobile_limit, viewcontrol: self)
            return false
        }
            
        else if (com_ind_txtWhatsappNumber.text?.count != 0){
            if ( (com_ind_txtWhatsappNumber.text?.count)! <= 9){
                com_ind_txtWhatsappNumber.showErrorWithText(errorText: "")
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_Whatsapp_limit, viewcontrol: self)
                return false
            }
        }
        
        
        if(com_ind_txtLandLineNumber.text?.count != 0){
            if ( (com_ind_txtLandLineNumber.text?.count)! <= 9){
                com_ind_txtLandLineNumber.showErrorWithText(errorText: "")
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_landline_limit, viewcontrol: self)
                return false
            }
        }
        
        
        
        if(com_ind_txtConstituency.text?.count == 0){
            com_ind_txtConstituency.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(com_ind_txtConstituency.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            return false
        }
        else if ( com_ind_txtAddress.text?.count == 0){
            com_ind_txtAddress.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(com_ind_txtAddress.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( com_ind_txtStreet.text?.count == 0){
            com_ind_txtStreet.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(com_ind_txtStreet.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( com_ind_txtTaluka.text?.count == 0){
            com_ind_txtTaluka.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(com_ind_txtTaluka.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( com_ind_txtDistrict.text?.count == 0){
            com_ind_txtDistrict.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(com_ind_txtDistrict.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( com_ind_txtState.text?.count == 0){
            com_ind_txtState.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(com_ind_txtState.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( com_ind_txtPinCode.text?.count == 0){
            com_ind_txtPinCode.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(com_ind_txtPinCode.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( (com_ind_txtPinCode.text?.count)! <= 5){
            com_ind_txtPinCode.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PinCode_limit, viewcontrol: self)
            return false
        }
        return true
    }
    func validationForBulider()-> Bool{
        if bulider_txtBuliderName.text?.count == 0 {
            bulider_txtBuliderName.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(bulider_txtBuliderName.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( bulider_txtContactPerson.text?.count == 0){
            bulider_txtContactPerson.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(bulider_txtContactPerson.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            
            return false
        }
        else if ( bulider_txtMobileNumber.text?.count == 0){
            bulider_txtMobileNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(bulider_txtMobileNumber.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if ( (bulider_txtMobileNumber.text?.count)! <= 9){
            bulider_txtMobileNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_Mobile_limit, viewcontrol: self)
            return false
        }
            
        else if (bulider_txtWhatsappNumber.text?.count != 0){
            if ( (bulider_txtWhatsappNumber.text?.count)! <= 9){
                bulider_txtWhatsappNumber.showErrorWithText(errorText: "")
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_Whatsapp_limit, viewcontrol: self)
                return false
            }
        }
        
        
        if(bulider_txtALandLineNumber.text?.count != 0){
            if ( (bulider_txtALandLineNumber.text?.count)! <= 9){
                bulider_txtALandLineNumber.showErrorWithText(errorText: "")
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_landline_limit, viewcontrol: self)
                return false
            }
        }
        
        if ( bulider_txtNumberOfFlat.text?.count == 0){
            bulider_txtNumberOfFlat.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(bulider_txtNumberOfFlat.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( bulider_txtConstituency.text?.count == 0){
            bulider_txtConstituency.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(bulider_txtConstituency.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            return false
        } else if ( bulider_txtAddress.text?.count == 0){
            bulider_txtAddress.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(bulider_txtAddress.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            return false
        }
        else if ( bulider_txtStreet.text?.count == 0){
            bulider_txtStreet.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(bulider_txtStreet.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            return false
        }
        else if ( bulider_txtTaluka.text?.count == 0){
            bulider_txtTaluka.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(bulider_txtTaluka.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            return false
        }
        else if ( bulider_txtDistrict.text?.count == 0){
            bulider_txtDistrict.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(bulider_txtDistrict.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            return false
        }
        else if ( bulider_txtState.text?.count == 0){
            bulider_txtState.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(bulider_txtState.placeholder!) ".replacingOccurrences(of: "*", with: "")), viewcontrol: self)
            return false
        }
        else if ( bulider_txtPincode.text?.count == 0){
            bulider_txtPincode.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(bulider_txtPincode.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( (bulider_txtPincode.text?.count)! <= 5){
            bulider_txtPincode.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PinCode_limit, viewcontrol: self)
            return false
        }
        
        
        
        return true
    }
    //    func buttonEnable()  {
    //
    //        if (selectionType == 4 || selectionType == 5){
    //            if cng_Png_txtFullName.text?.count != 0 && cng_png_txtMoileNumber.text?.count != 0 {
    //                buttonEnable_OnView(tag: 1, btn: cng_png_btnSubmit)
    //            }else{
    //                buttonEnable_OnView(tag: 2, btn: cng_png_btnSubmit)
    //            }
    //
    //        }
    //        else  if (selectionType == 6 || selectionType == 7){
    //            if com_ind_txtBusinessName.text?.count != 0 && com_ind_txtPoc.text?.count != 0 && com_ind_txtMobileNumber.text?.count != 0 {
    //                buttonEnable_OnView(tag: 1, btn: com_ind_btnSubmit)
    //            }else{
    //                buttonEnable_OnView(tag: 2, btn: com_ind_btnSubmit)
    //            }
    //
    //
    //        }
    //        else  if (selectionType == 10){
    //            if bulider_txtBuliderName.text?.count != 0 && bulider_txtContactPerson.text?.count != 0 && bulider_txtMobileNumber.text?.count != 0 {
    //                buttonEnable_OnView(tag: 1, btn: bulider_btnSubmit)
    //            }else{
    //                buttonEnable_OnView(tag: 2, btn: bulider_btnSubmit)
    //            }
    //        }
    //    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_URL_Save_connection_request_API(sender : UIButton , connectionType : Int)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("save_connection_request", forKey: "request")
            //CNG/PNG
            if(connectionType == 4 || connectionType == 5){
                dictData.setValue(connectionType, forKey: "connection_type")
                dictData.setValue(cng_Png_txtPrefix.text!, forKey: "name_title")
                dictData.setValue(cng_Png_txtFullName.text!, forKey: "full_name")
                dictData.setValue(cng_png_txtMoileNumber.text!, forKey: "mobile_number")
                dictData.setValue(cng_png_txtWhatsappNumber.text!, forKey: "whatsapp_number")
                dictData.setValue(cng_png_txtLandLineNumber.text!, forKey: "landline_number")
                dictData.setValue("\(cng_png_txtsociety.tag)", forKey: "society_or_appartment")
                
                dictData.setValue("\(cng_png_txtsocietyNameOther.text!)", forKey: "society_other")
                dictData.setValue("\(cng_png_txtConstituency.tag)", forKey: "constituency")
                dictData.setValue(cng_png_txtHouseNumber.text!, forKey: "house_number")
                dictData.setValue(cng_png_txtStreet.text!, forKey: "street")
                dictData.setValue("\(cng_png_txtTaluka.tag)", forKey: "taluka")
                dictData.setValue("\(cng_png_txtDistrict.tag)", forKey: "district")
                dictData.setValue(cng_png_txtState.text!, forKey: "state")
                dictData.setValue(cng_png_txtPinCode.text!, forKey: "pincode")
                
                dictData.setValue("", forKey: "address")
                dictData.setValue("", forKey: "business_name")
                dictData.setValue("", forKey: "builder_or_society_name")
                dictData.setValue("", forKey: "contact_person")
                dictData.setValue("", forKey: "number_of_flats")
                
            }
                //Comm/Ind
            else if (connectionType == 6 || connectionType == 7){
                dictData.setValue(connectionType, forKey: "connection_type")
                dictData.setValue(com_ind_txtBusinessName.text!, forKey: "business_name")
                dictData.setValue(com_ind_txtPrefix.text!, forKey: "name_title")
                dictData.setValue(com_ind_txtPoc.text!, forKey: "contact_person")
                dictData.setValue(com_ind_txtMobileNumber.text!, forKey: "mobile_number")
                dictData.setValue(com_ind_txtWhatsappNumber.text!, forKey: "whatsapp_number")
                dictData.setValue(com_ind_txtLandLineNumber.text!, forKey: "landline_number")
                dictData.setValue("\(com_ind_txtConstituency.tag)", forKey: "constituemcy")
                dictData.setValue(com_ind_txtAddress.text!, forKey: "address")
                dictData.setValue(com_ind_txtStreet.text!, forKey: "street")
                dictData.setValue("\(com_ind_txtTaluka.tag)", forKey: "taluka")
                dictData.setValue("\(com_ind_txtDistrict.tag)", forKey: "district")
                dictData.setValue(com_ind_txtState.text!, forKey: "state")
                dictData.setValue(com_ind_txtPinCode.text!, forKey: "pincode")
                
                dictData.setValue("", forKey: "number_of_flats")
                dictData.setValue("", forKey: "society_or_appartment")
                dictData.setValue("", forKey: "society_other")
                
                dictData.setValue("", forKey: "builder_or_society_name")
                dictData.setValue("", forKey: "house_number")
                dictData.setValue("", forKey: "full_name")
            }
                //Bulider
            else if (connectionType == 10){
                dictData.setValue(connectionType, forKey: "connection_type")
                dictData.setValue("\(bulider_txtBuliderName.text!)", forKey: "builder_or_society_name")
                dictData.setValue(bulider_txtMobileNumber.text!, forKey: "mobile_number")
                dictData.setValue(bulider_txtPrefix.text!, forKey: "name_title")
                dictData.setValue(bulider_txtContactPerson.text!, forKey: "contact_person")
                dictData.setValue(bulider_txtWhatsappNumber.text!, forKey: "whatsapp_number")
                dictData.setValue(bulider_txtALandLineNumber.text!, forKey: "landline_number")
                dictData.setValue(bulider_txtNumberOfFlat.text!, forKey: "number_of_flats")
                dictData.setValue("\(bulider_txtConstituency.tag)", forKey: "constituemcy")
                dictData.setValue(bulider_txtAddress.text!, forKey: "address")
                dictData.setValue(bulider_txtStreet.text!, forKey: "street")
                dictData.setValue("\(bulider_txtTaluka.tag)", forKey: "taluka")
                dictData.setValue("\(bulider_txtDistrict.tag)", forKey: "district")
                dictData.setValue(bulider_txtState.text!, forKey: "state")
                dictData.setValue(bulider_txtPincode.text!, forKey: "pincode")
                
                dictData.setValue("", forKey: "society_or_appartment")
                dictData.setValue("", forKey: "society_other")
                
                dictData.setValue("", forKey: "house_number")
                dictData.setValue("", forKey: "business_name")
                dictData.setValue("", forKey: "full_name")
                
            }
            
            print(dictData)
            
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            
            WebService.callAPIBYPOST(parameter: dictData, url: URL_Save_connection_request) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let dictData  = (dict.value(forKey: "data")as! NSDictionary)
                        FTIndicator.showNotification(withTitle: alertInfo, message: (dict.value(forKey: "msg")as! String))
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "OTPVarifyVC")as! OTPVarifyVC
                            testController.dictData =  (dictData ).mutableCopy() as! NSMutableDictionary
                            testController.strViewComeFrom = "Connection"
                            self.navigationController?.pushViewController(testController, animated: true)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension ApplyForNewConnectionVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == cng_png_txtMoileNumber || textField == cng_png_txtWhatsappNumber || textField == com_ind_txtMobileNumber || textField == com_ind_txtWhatsappNumber || textField == bulider_txtMobileNumber || textField == bulider_txtWhatsappNumber  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        else if (textField == cng_png_txtPinCode  || textField == com_ind_txtPinCode || textField == bulider_txtPincode){
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 5)
        }
        else if (textField == bulider_txtNumberOfFlat){
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 3)
        }
        else if (textField == cng_png_txtHouseNumber){
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 7)
        }
        else if (textField == cng_png_txtLandLineNumber || textField == com_ind_txtLandLineNumber ||  textField == bulider_txtALandLineNumber){
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
        }
        else if (textField == cng_Png_txtFullName || textField == com_ind_txtPoc || textField == bulider_txtContactPerson) {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "CHAR", limitValue: 50)
        }
        
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 50)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
