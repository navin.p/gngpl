//
//  DashBoardVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/11/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation
import  AVKit
import Crashlytics

class DashBoardVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewPager: CPImageSlider!
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    @IBOutlet weak var collection_DashBoard: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var imgPlay: UIImageView!

    @IBOutlet weak var btnLogin_Profile: UIButton!
    @IBOutlet weak var lblLogin_Profile: UILabel!
    @IBOutlet weak var btnNewConnection: UIButton!
    @IBOutlet weak var lblNewConnection: UILabel!
    @IBOutlet weak var btnAbout: UIButton!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var btnContactUs: UIButton!
    @IBOutlet weak var lblContactUS: UILabel!
    @IBOutlet weak var btnApplyNewConnection: UIButton!
    @IBOutlet weak var lblApplyNewConnection: UILabel!
    
    
    var ary_CollectionData = NSMutableArray()
    var ary_SliderData = NSMutableArray()
    var ary_DropDownData = NSMutableArray()
    var dictNotificationData = NSMutableDictionary()
    // MARK: - --------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        imgPlay.isHidden = true
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        
        nsud.set("Yes", forKey: "WelcomeScreen")
        let collectionViewLayout = (self.collection_DashBoard.collectionViewLayout as! UICollectionViewFlowLayout)
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 0,left: 0, bottom: 0, right: 0)
        collectionViewLayout.scrollDirection = .vertical
        self.collection_DashBoard.collectionViewLayout = collectionViewLayout
        ary_CollectionData = NSMutableArray()
        collection_DashBoard.dataSource = self
        collection_DashBoard.delegate = self
        animationOnView()
        pageController.currentPageIndicatorTintColor = hexStringToUIColor(hex: primaryColor)
        pageController.tintColor = hexStringToUIColor(hex: orangeColor)
        
         let date = Date()
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "dd"
           let day  = dateFormatter.string(from: date)
               if((nsud.value(forKey: "GNGPL_VersionUpdateDate")) != nil){
                   if(Int(day) != Int((nsud.value(forKey: "GNGPL_VersionUpdateDate")!)as! String)){
                       checkUpdate()
                   }
               }else{
                      checkUpdate()
               }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(true)
        if(dictNotificationData.count != 0){
            CheckNotiFicationDataAndRedirectOnParticulerView(dict: dictNotificationData)
        }else{
            viewPager.startAutoPlay()
            if(getLoginDataFromLocal().count != 0){
                print(getLoginDataFromLocal())
                dict_Login_Data = NSMutableDictionary()
                dict_Login_Data = ((getLoginDataFromLocal() as NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary
                if (dict_Login_Data.count != 0){
                    lblLogin_Profile.text = "Profile"
                    btnLogin_Profile.setImage(UIImage(named: "dashboard_profile"), for: .normal)
                    btnApplyNewConnection.isEnabled = false
                    btnApplyNewConnection.alpha = 0.9
                    lblApplyNewConnection.alpha = 0.9
                    
                    self.call_RefreshToken_API()
                }else{
                    dict_Login_Data = NSMutableDictionary()
                    lblLogin_Profile.text = "Login"
                    btnLogin_Profile.setImage(UIImage(named: "login"), for: .normal)
                    btnApplyNewConnection.isEnabled = true
                    btnApplyNewConnection.alpha = 1.0
                    lblApplyNewConnection.alpha = 1.0
                    
                }
                
            }else{
                dict_Login_Data = NSMutableDictionary()
                lblLogin_Profile.text = "Login"
                btnLogin_Profile.setImage(UIImage(named: "login"), for: .normal)
                btnApplyNewConnection.isEnabled = true
                btnApplyNewConnection.alpha = 1.0
                lblApplyNewConnection.alpha = 1.0
                
            }
            
            if ary_CollectionData.count == 0 {
                call_DashBoard_API()
            }
            call_DropDownList_API()
        }
    }
    
    
    
    
    
    override func viewWillLayoutSubviews() {
        self.collection_DashBoard.layoutIfNeeded()
        if DeviceType.IS_IPHONE_5 {
            heightConstant.constant = 140.0
        }else if DeviceType.IS_IPHONE_6 {
            heightConstant.constant = 180.0
        }
        else if DeviceType.IS_IPHONE_6P {
            heightConstant.constant = 190.0
        }else if DeviceType.IS_IPHONE_X {
            heightConstant.constant = 220.0
        }else if (DeviceType.IS_IPAD ){
            heightConstant.constant = 350.0

        }else{
            heightConstant.constant = 150.0
        }
    }
    //MARK:
    //MARK: checkUpdate
    func checkUpdate() {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let day  = dateFormatter.string(from: date)
        nsud.set(day, forKey: "GNGPL_VersionUpdateDate")
        
        VersionCheck.shared.checkAppStore() { isNew, version , message in
            if(isNew!){
                let alert = UIAlertController(title: "New Version Available", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { action in
                    guard let url = URL(string: application_ID) else { return }
                    UIApplication.shared.open(url)
                    
                }))
                alert.addAction(UIAlertAction(title: "Skip", style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    func setSliderImages(aryimageData : NSMutableArray) {
        ary_SliderData = NSMutableArray()
        ary_SliderData = aryimageData
        viewPager.images =  ary_SliderData
        viewPager.delegate = self
        viewPager.autoSrcollEnabled = true
        viewPager.enableArrowIndicator = false
        viewPager.enablePageIndicator = false
        viewPager.enableSwipe = true
        pageController.numberOfPages = ary_SliderData.count
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnAbout(_ sender: UIButton) {
       // Crashlytics.sharedInstance().crash()

        viewPager.stopAutoPlay()

        sender.alpha = 0.4
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AboutVC")as! AboutVC
            testController.strViewComeFrom = "About us"
            self.navigationController?.pushViewController(testController, animated: true)

        }
    }
    @IBAction func actionOnContactus(_ sender: UIButton) {
        viewPager.stopAutoPlay()

        sender.alpha = 0.4
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ContactUsVC")as! ContactUsVC
            self.navigationController?.pushViewController(testController, animated: true)
            
        }
    }
    @IBAction func actionOnLogin(_ sender: UIButton) {
        viewPager.stopAutoPlay()
        sender.alpha = 0.4
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            if(self.lblLogin_Profile.text != "Login"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "UserDashBoardVC")as! UserDashBoardVC
                self.navigationController?.pushViewController(testController, animated: true)
            }else{
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
    }
    @IBAction func actionOnApplyNewConnection(_ sender: UIButton) {
        viewPager.stopAutoPlay()

        sender.alpha = 0.4
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ApplyForNewConnectionVC")as! ApplyForNewConnectionVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    
    // MARK: - --------------CheckNotificationData
    // MARK: -
   // type = 1 for PayBill
    func CheckNotiFicationDataAndRedirectOnParticulerView(dict : NSMutableDictionary) {
        var strMeterNumber = ""
        if(nsud.value(forKey: "GNGPL_RefreshToken") != nil){
            let dict = nsud.value(forKey: "GNGPL_RefreshToken")as! NSDictionary
            strMeterNumber = "\(dict.value(forKey: "meter_number")!)"
        }
        var strBPNumber = ""
        if(dict_Login_Data.count != 0){
            strBPNumber = "\(dict_Login_Data.value(forKey: "bp_number")!)"
        }

        if(strMeterNumber != "" && strBPNumber != ""){
            if(dict.count != 0){
                if("\(dict.value(forKey: "type")!)" == "1"){
                    dictNotificationData = NSMutableDictionary()
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "PayBillVC")as! PayBillVC
                    self.viewPager.stopAutoPlay()
                    self.navigationController?.pushViewController(testController, animated: true)
                }else{
                    dictNotificationData = NSMutableDictionary()
                    self.viewWillAppear(true)
                }
            }
        }else{
              dictNotificationData = NSMutableDictionary()
            self.viewWillAppear(true)
        }
    }
   
    // MARK: - --------------LogOut Clear Data from App
    // MARK: -
    func ClearDataFromApplication(alertDetailMessage : String){
        let alert = UIAlertController(title: alertMessage, message: "\(alertDetailMessage)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "LOGIN", style: .default, handler: { action in
            deleteAllRecords(strEntity:"ContactData")
            deleteAllRecords(strEntity:"AboutData")
            deleteAllRecords(strEntity:"TermsCondition")
            deleteAllRecords(strEntity:"DropDownData")
            deleteAllRecords(strEntity:"Dashboard")
            deleteAllRecords(strEntity:"SavingCalculator")
            deleteAllRecords(strEntity:"Queries")
            deleteAllRecords(strEntity:"TermsConditionReferCode")
            deleteAllRecords(strEntity:"LoginData")
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
            self.viewPager.stopAutoPlay()
            self.navigationController?.pushViewController(testController, animated: true)
        }))
        self.present(alert, animated: true)
    }
    // MARK: - --------------animationOnView
    // MARK: -
    func animationOnView()  {
        btnLogin_Profile.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        btnAbout.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        btnContactUs.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        btnNewConnection.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)

        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.btnLogin_Profile.transform = .identity
                        self?.btnAbout.transform = .identity
                        self?.btnContactUs.transform = .identity
                        self?.btnNewConnection.transform = .identity

            },
                       completion: nil)
    }
    
    // MARK: - --------------Local Data Base
    // MARK: -
    func getLoginDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "LoginData", strkey: "logindata")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "logindata") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_RefreshToken_API()  {
        if !(isInternetAvailable()){
          //  showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("refresh_tokens", forKey: "request")
            dictData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
            dictData.setValue(Platform, forKey: "platform")
            dictData.setValue("\(DeviceID)", forKey: "device_id")
            nsud.value(forKey: "GNGPL_FCM_Token") != nil ? dictData.setValue("\(nsud.value(forKey: "GNGPL_FCM_Token")!)", forKey: "device_token") : dictData.setValue("", forKey: "device_token")
            print("Refresh Token Call : \(dictData)" )
            WebService.callAPIBYPOST(parameter: dictData, url: URL_refresh_tokens) { (responce, status) in
                print(responce)
               
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        nsud.set(dict, forKey: "GNGPL_RefreshToken")
                        
                        if("\(dict.value(forKey: "login_status")!)" == "2"){

                            self.ClearDataFromApplication(alertDetailMessage: "\(dict.value(forKey: "data")!)")
                        }
                    }
                }
            }
        }
    }
    
    
    
    func call_DashBoard_API()  {
        if !(isInternetAvailable()){
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("dashboard_data", forKey: "request")
            //     self.activityIndicator.isHidden = false
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_Dashboard) { (responce, status) in

                print(responce)
                //self.activityIndicator.isHidden = true
                remove_dotLoader(controller: self)
                removeErrorView()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        self.ary_CollectionData = NSMutableArray()
                        self.ary_SliderData = NSMutableArray()
                        self.ary_CollectionData  = ((dict.value(forKey: "data")as! NSDictionary).value(forKey: "connection_type") as! NSArray).mutableCopy() as! NSMutableArray
                        self.setSliderImages(aryimageData: ((dict.value(forKey: "data")as! NSDictionary).value(forKey: "slider_images") as! NSArray).mutableCopy() as! NSMutableArray)
                        let arySaveData = NSMutableArray()
                        arySaveData.add((dict.value(forKey: "data")as! NSDictionary))
                        deleteAllRecords(strEntity:"Dashboard")
                        saveDataInLocalArray(strEntity: "Dashboard", strKey: "dashboard", data: arySaveData)
                        self.collection_DashBoard.reloadData()
                    }else{
                        self.showErrorWithImage(strTitle: "\(responce.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)

                    }
                }else{
                    self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    
    func call_DropDownList_API()  {
        if !(isInternetAvailable()){
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("drop_down_list", forKey: "request")
            print(URL_Get_drop_down_list)
            WebService.callAPIBYPOST(parameter: dictData, url: URL_Get_drop_down_list) { (responce, status) in
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        deleteAllRecords(strEntity:"DropDownData")
                        saveDataInLocalArray(strEntity: "DropDownData", strKey: "dropdata", data: (dict.value(forKey: "data")as! NSArray).mutableCopy() as! NSMutableArray)
                        
                    }else{
                        //FTIndicator.showToastMessage(alertSomeError)
                        
                        // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                    //  self.viewContain.isHidden = false
                }else{
                    //FTIndicator.showToastMessage(alertSomeError)
                }
            }
        }
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
}

// MARK: - ----------------CPSliderDelegate
// MARK: -
extension  DashBoardVC  : CPSliderDelegate{
    func sliderImageIndex(slider: CPImageSlider, index: Int) {
        pageController.currentPage = index
        let dictPlayer = self.ary_SliderData.object(at: index)as! NSDictionary
        if("\(dictPlayer.value(forKey: "video_url")!)" != ""){
            imgPlay.isHidden = false
        }else{
            imgPlay.isHidden = true
        }
    }
    
    func sliderImageTapped(slider: CPImageSlider, index: Int) {
        // Create a new player
        
        let dictPlayer = self.ary_SliderData.object(at: index)as! NSDictionary
        var urlImage = ""
        if("\(dictPlayer.value(forKey: "video_url")!)" != ""){
            urlImage = "\(dictPlayer.value(forKey: "video_url")!)"
            let aryStr = urlImage.split(separator: "=")
            if(aryStr.count == 2){
                urlImage = String(aryStr[1])
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "PlayerVC") as! PlayerVC
                vc.strURl = urlImage
                self.present(vc, animated: true, completion: {})
            }
        }
    }
}
// MARK: - ----------------UICollectionViewDelegate
//CALayer -

extension DashBoardVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ary_CollectionData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardCell", for: indexPath as IndexPath) as! DashboardCell
        
        let dict =  ary_CollectionData[indexPath.row]as! NSDictionary
        
        cell.dashBoard_lbl_Title.text = "\(dict.value(forKey: "connection_name")!)"
        let urlImage = "\(dict.value(forKey: "icon_syspath")!)"
        
        cell.dashBoard_Image.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), usingActivityIndicatorStyle: .gray)
        
        if(DeviceType.IS_IPHONE_5){
            cell.dashBoard_Image.frame = CGRect(x: cell.frame.width/2 - 23, y: cell.frame.height/2 - 40, width: 46, height: 46)
            cell.dashBoard_lbl_Title.frame = CGRect(x: 5, y: cell.dashBoard_Image.frame.maxY + 08, width: cell.frame.width - 10, height: 32)
            cell.dashBoard_lbl_Title.font = UIFont(name: cell.dashBoard_lbl_Title.font.fontName, size: 14.0)
            
        }else if (DeviceType.IS_IPHONE_6){
            cell.dashBoard_Image.frame = CGRect(x: cell.frame.width/2 - 30, y: cell.frame.height/2 - 45, width: 60, height: 60)
            cell.dashBoard_lbl_Title.frame = CGRect(x: 5, y: cell.dashBoard_Image.frame.maxY + 08, width: cell.frame.width - 10, height: 35)
            cell.dashBoard_lbl_Title.font = UIFont(name: cell.dashBoard_lbl_Title.font.fontName, size: 15.0)
        }
        else if (DeviceType.IS_IPHONE_6P){
            cell.dashBoard_Image.frame = CGRect(x: cell.frame.width/2 - 32, y: cell.frame.height/2 - 47, width: 64, height: 64)
            cell.dashBoard_lbl_Title.frame = CGRect(x: 5, y: cell.dashBoard_Image.frame.maxY + 10, width: cell.frame.width - 10, height: 35)
            cell.dashBoard_lbl_Title.font = UIFont(name: cell.dashBoard_lbl_Title.font.fontName, size: 16.0)
        }
        else if (DeviceType.IS_IPHONE_X ){
            cell.dashBoard_Image.frame = CGRect(x: cell.frame.width/2 - 35, y: cell.frame.height/2 - 50, width: 70, height: 70)
            cell.dashBoard_lbl_Title.frame = CGRect(x: 5, y: cell.dashBoard_Image.frame.maxY + 10, width: cell.frame.width - 10, height: 35)
            cell.dashBoard_lbl_Title.font = UIFont(name: cell.dashBoard_lbl_Title.font.fontName, size: 17.0)
        }else if (DeviceType.IS_IPAD ){
            cell.dashBoard_Image.frame = CGRect(x: cell.frame.width/2 - 45, y: cell.frame.height/2 - 60, width: 90, height: 90)
            cell.dashBoard_lbl_Title.frame = CGRect(x: 5, y: cell.dashBoard_Image.frame.maxY + 10, width: cell.frame.width - 10, height: 35)
            cell.dashBoard_lbl_Title.font = UIFont(name: cell.dashBoard_lbl_Title.font.fontName, size: 20.0)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collection_DashBoard.frame.size.width)  / 3, height:(self.collection_DashBoard.frame.size.height)  / 2 )
      
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.alpha = 0.4
        cell!.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            cell!.alpha = 1
            cell!.transform = .identity
        }) { (result) in
            let dict =  self.ary_CollectionData[indexPath.row]as! NSDictionary
            let strview_on_app = "\(dict.value(forKey: "view_on_app")!)"
            if strview_on_app == "1" { // Calculator
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "PNGDomesticCalculatorVC")as! PNGDomesticCalculatorVC
                self.viewPager.stopAutoPlay()
                self.navigationController?.pushViewController(testController, animated: true)
                
              // FTIndicator.showToastMessage("Under development")
            }
           else if strview_on_app == "3" { // GO Description
                DispatchQueue.main.async{
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "AboutVC")as! AboutVC
                    testController.dict_Data =  dict
                    testController.strViewComeFrom = "\(dict.value(forKey: "connection_name")!)" as NSString
                    self.viewPager.stopAutoPlay()
                    self.navigationController?.pushViewController(testController, animated: true)
                }
                
            }
                else{    // Sub View
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "ListControllerVC")as! ListControllerVC
                testController.dictData =  dict
                self.viewPager.stopAutoPlay()
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
    }
}

class DashboardCell: UICollectionViewCell {
    //DashBoard
    @IBOutlet weak var dashBoard_Image: UIImageView!
    @IBOutlet weak var dashBoard_lbl_Title: UILabel!
}
