//
//  ChequeInfoScreenVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 6/5/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class ChequeInfoScreenVC: UIViewController {
   
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var tfHolderName: ACFloatingTextfield!
    @IBOutlet weak var tfBankName: ACFloatingTextfield!
    @IBOutlet weak var tfBranchName: ACFloatingTextfield!
    @IBOutlet weak var tfChequeno: ACFloatingTextfield!

    var dictPaymentData = NSMutableDictionary()
    var strComeFrom = ""

 
    // MARK: - -------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonEnable_OnView(tag: 1, btn: btnSubmit)
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        lblAmount.text = "\u{20B9} \(dictPaymentData["total_amount"]!)"
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actiononContinue(_ sender: UIButton) {
        if(validation()){
            
            if(strComeFrom == "PayBill"){
                dictPaymentData.setValue("\(tfHolderName.text!)", forKey: "ac_holder_name")
                dictPaymentData.setValue("\(tfBankName.text!)", forKey: "bank_name")
                dictPaymentData.setValue("\(tfChequeno.text!)", forKey: "cheque_no")
                dictPaymentData.setValue("\(tfBranchName.text!)", forKey: "branch_name")
               call_PayBill_API(sender: sender)
              

            } else if(strComeFrom == "BalanceSecurityDeposite"){
                dictPaymentData.setValue("\(tfHolderName.text!)", forKey: "ac_holder_name")
                dictPaymentData.setValue("\(tfBankName.text!)", forKey: "bank_name")
                dictPaymentData.setValue("\(tfChequeno.text!)", forKey: "cheque_no")
                dictPaymentData.setValue("\(tfBranchName.text!)", forKey: "branch_name")
                call_SecurityDeposite_API(sender: sender)
            }
            else {
                dictPaymentData.setValue("\(tfHolderName.text!)", forKey: "ac_holder_name")
                dictPaymentData.setValue("\(tfBankName.text!)", forKey: "bank_name")
                dictPaymentData.setValue("\(tfChequeno.text!)", forKey: "cheque_no")
                dictPaymentData.setValue("\(tfBranchName.text!)", forKey: "branch_name")
                call_SavePayment_ON_Server_API(sender: sender)
            }
           
        }
    }
    
    // MARK: - --------------Validation
    // MARK: -

    func validation()-> Bool{
        if tfHolderName.text?.count == 0 {
            tfHolderName.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(tfHolderName.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if (tfChequeno.text?.count == 0){
            tfChequeno.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(tfChequeno.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if (tfBankName.text?.count == 0){
            tfBankName.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(tfBankName.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if (tfBranchName.text?.count == 0){
            tfBranchName.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(tfBranchName.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        return true
    }
    
    // MARK: - --------------API Calling
    // MARK: -
    func call_SavePayment_ON_Server_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callAPIBYPOST(parameter: dictPaymentData, url: URL_SavePayment) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: DashBoardVC.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "\(dict.value(forKey: "payment_message")!)", viewcontrol: self)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_PayBill_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            WebService.callAPIBYPOST(parameter: dictPaymentData, url: URL_Pay_monthly_bill) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "UserDashBoardVC")as! UserDashBoardVC
                        self.navigationController?.pushViewController(testController, animated: true)
                     
                FTIndicator.showToastMessage("\(dict.value(forKey: "data")as! String)")
             }else{
                 
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func call_SecurityDeposite_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callAPIBYPOST(parameter: dictPaymentData, url: URL_Pay_balance_security_deposit) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        FTIndicator.showToastMessage("\(dict.value(forKey: "data")as! String)")

                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: UserDashBoardVC.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }

}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension ChequeInfoScreenVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

            return (txtFiledValidation(textField: textField, string: string, returnOnly: "All", limitValue: 85))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as! ACFloatingTextfield).errorLineColor = UIColor.lightGray
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
