//
//  TermsAndConditionVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/13/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData

//MARK:
//MARK: ---------------Protocol-----------------

protocol refreshSetProfile_View : class{
    func refreshview(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ----------RefreshCng_Png_Com_Ind_View Delegate Methods----------

extension SetProfileVC: refreshSetProfile_View {
    func refreshview(dictData: NSDictionary, tag: Int) {
        if(tag == 21){
            txtSocietyname.text = (dictData.value(forKey: "name")as! String)
            txtSocietyname.tag = Int("\(dictData.value(forKey: "id")!)")!
        }else if(tag == 22){
            txtCity.text = (dictData.value(forKey: "name")as! String)
            txtCity.tag = Int("\(dictData.value(forKey: "id")!)")!
        }else if(tag == 23){
            txtTaluka.text = (dictData.value(forKey: "name")as! String)
            txtTaluka.tag = Int("\(dictData.value(forKey: "id")!)")!
        }
    }
}

class SetProfileVC: UIViewController ,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var txtFullName: ACFloatingTextfield!
    @IBOutlet weak var txtFatherName: ACFloatingTextfield!
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var txtWhatsappNumber: ACFloatingTextfield!
    @IBOutlet weak var txtLendLineNumber: ACFloatingTextfield!
    
    @IBOutlet weak var txtFlatNumber: ACFloatingTextfield!
    @IBOutlet weak var txtStreet: ACFloatingTextfield!
    @IBOutlet weak var txtSocietyname: ACFloatingTextfield!
    @IBOutlet weak var txtSocietyOthername: ACFloatingTextfield!

    @IBOutlet weak var txtArea: ACFloatingTextfield!
    @IBOutlet weak var txtTaluka: ACFloatingTextfield!
    @IBOutlet weak var txtCity: ACFloatingTextfield!
    @IBOutlet weak var txtPinCode: ACFloatingTextfield!
    @IBOutlet weak var txtPanNumber: ACFloatingTextfield!
    @IBOutlet weak var txtAadharNumber: ACFloatingTextfield!
    @IBOutlet weak var txtEmailAddress: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    @IBOutlet weak var txtCPassword: ACFloatingTextfield!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewHeader1: CardView!

    
    @IBOutlet weak var btnSelectSocity: UIButton!
    @IBOutlet weak var btnTaluka: UIButton!
    @IBOutlet weak var btnDistrict: UIButton!

    
    
    @IBOutlet weak var heightSocityName: NSLayoutConstraint!
    @IBOutlet weak var topSocityName: NSLayoutConstraint!
    
    var imagePicker = UIImagePickerController()
    var dictDropListData = NSDictionary()
    var strProfileImageName = ""
    var strApplicationID = ""
    
    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        viewHeader1.backgroundColor = hexStringToUIColor(hex: primaryColor)
         imgProfile.backgroundColor = hexStringToUIColor(hex: primaryColor)
     
        
        imgProfile.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = self  
        imgProfile.addGestureRecognizer(tap)
        setDataOnView()
    }
    
    override func viewWillLayoutSubviews() {
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.width /  2
        self.imgProfile.clipsToBounds = true
        self.imgProfile.layer.borderWidth = 2.0
        self.imgProfile.layer.borderColor = UIColor.white.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.imgProfile.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.imgProfile.transform = .identity
            },
                       completion: nil)
        if(self.imgProfile.image == UIImage(named:"profile_icon")){
            imgProfile.contentMode = .center
        }else{
            imgProfile.contentMode = .scaleAspectFill
        }
        
        if(txtSocietyname.tag == 0){
            heightSocityName.constant = 45.0
            topSocityName.constant = 18.0
        }else{
            heightSocityName.constant = 0.0
            topSocityName.constant = 0.0
        }
    }
    
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnDone(_ sender: UIButton) {
        //saveImage()
        if(validation()){
            call_URL_Save_Profile_API(sender: sender)
        }else{
            viewWillLayoutSubviews()
        }
    }
    @IBAction func actionOnCamera(_ sender: UIButton) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = self
        sender.addGestureRecognizer(tap)
    }
    @IBAction func actionOnBack(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: RegisterVC.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    // MARK: - --------------IBAction For Drop Down
    // MARK: -
    @IBAction func actionOnCity(_ sender: UIButton) {
        sender.tag = 22
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "districts") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
        
    }
    
    @IBAction func actionOnSociety(_ sender: UIButton) {
        sender.tag = 21
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "society_or_appartment") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
        
        
    }
    @IBAction func action__Taluka(_ sender: UIButton) {
        sender.tag = 23
        if dictDropListData.count != 0 {
            var aryConstituency = NSMutableArray()
            aryConstituency = (dictDropListData.value(forKey: "taluka") as! NSArray).mutableCopy() as! NSMutableArray
            if aryConstituency.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryConstituency, strTitle: "")
            }else{
                FTIndicator.showToastMessage(alertDataNotFound)
            }
        }
    }
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: Alert_SelectOptionProfile, message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            
            if(UIImagePickerController .isSourceTypeAvailable(.camera)){
                self.imagePicker.sourceType = .camera
                self.imagePicker.delegate = self
                
                self.present(self.imagePicker, animated: true, completion: nil)
            } else {
                //   "You don't have camera"
            }
        }))
        if(imgProfile.image != nil || imgProfile.image != UIImage(named:"profile_icon")){
            alert.addAction(UIAlertAction(title: Alert_Preview, style: .default, handler:{ (UIAlertAction)in
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
                testController.img = self.imgProfile.image!
                self.navigationController?.pushViewController(testController, animated: true)
                
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: {
        })
    }
    
    // MARK: - --------------Extra Method
    // MARK: -
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
        self.view.endEditing(true)
        
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleSetProfile_View = self
            vc.aryTBL = aryData
            self.present(vc, animated: true, completion: {})
        }
    }
    // MARK: - --------------Validation
    // MARK: -
    
    func validation()-> Bool{
        self.view.endEditing(true)
        if imgProfile.image == UIImage(named: "profile_icon") {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_ProfileImage, viewcontrol: self)
            return false
        }
        else if txtFullName.text?.count == 0 {
            txtFullName.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtFullName.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }else if ( txtFatherName.text?.count == 0){
            txtFatherName.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtFatherName.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( txtMobileNumber.text?.count == 0){
            txtMobileNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtMobileNumber.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
            
        else if ( (txtMobileNumber.text?.count)! <= 9){
            txtMobileNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_Mobile_limit, viewcontrol: self)
            return false
        }
            
        else if ((txtWhatsappNumber.text?.count)! > 0) {
            if ( (txtWhatsappNumber.text?.count)! <= 9){
                txtWhatsappNumber.showErrorWithText(errorText: "")
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_Whatsapp_limit, viewcontrol: self)

                return false
            }
        }
        if(txtLendLineNumber.text?.count != 0){
            if ( (txtLendLineNumber.text?.count)! <= 9){
                txtLendLineNumber.showErrorWithText(errorText: "")
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_landline_limit, viewcontrol: self)
                return false
            }
        }

        
        if ( txtSocietyname.text?.count == 0){
            txtSocietyname.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtSocietyname.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( txtFlatNumber.text?.count == 0){
            txtFlatNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtFlatNumber.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( txtStreet.text?.count == 0){
            txtStreet.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtStreet.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)

            return false
        }
            
        else if ( txtArea.text?.count == 0){
            txtArea.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtArea.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( txtTaluka.text?.count == 0){
            txtTaluka.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtTaluka.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)

            return false
        }
        else if ( txtCity.text?.count == 0){
            txtCity.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtCity.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            return false
        }
        else if ( txtPinCode.text?.count == 0){
            txtPinCode.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtPinCode.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)

            return false
        }
      
           else if ( (txtPinCode.text?.count)! <= 5){
                txtPinCode.showErrorWithText(errorText: "")
             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PinCode_limit, viewcontrol: self)

                return false
            }
        
        else if ( (txtPanNumber.text?.count)! > 0){
            if ( (txtPanNumber.text?.count)! <= 9){
                txtPanNumber.showErrorWithText(errorText: "")
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PanNumberValid, viewcontrol: self)

                return false
            }
        }
        
        if ( txtAadharNumber.text?.count == 0){
            txtAadharNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtAadharNumber.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)

            return false
        }
        else if ( (txtAadharNumber.text?.count)! < 12){
            txtAadharNumber.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_AadharNumberValid, viewcontrol: self)
            return false
        }
            
        else if ( (txtEmailAddress.text?.count)! > 0){
            if  !(validateEmail(email: txtEmailAddress.text!)){
               // return true
                txtEmailAddress.showErrorWithText(errorText: "")
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EmailAddress, viewcontrol: self)
                return false
            }
        }
        if ( (txtPassword.text?.count)! == 0){
            txtPassword.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtPassword.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)

            return false
        }
        else if ((txtPassword.text?.count)! < 6){
            txtPassword.showErrorWithText(errorText:"" )
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PasswordLimit, viewcontrol: self)

            return false
        }else if ((txtCPassword.text?.count)! == 0){
            txtCPassword.showErrorWithText(errorText:"" )
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ("\(txtCPassword.placeholder!) ".replacingOccurrences(of: "*", with: "") + Alert_Required), viewcontrol: self)
            
            return false
        }
        else if (txtCPassword.text != txtPassword.text!){
            txtCPassword.showErrorWithText(errorText: "")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_CPassword, viewcontrol: self)
            return false
        }
        return true
    }
    
//    func buttonEnable()  {
//
//    if (imgProfile.image != UIImage(named: "profile_icon")) && txtFullName.text?.count != 0 && txtFatherName.text?.count != 0 && txtMobileNumber.text?.count != 0 && txtSocietyname.text?.count != 0 && txtFlatNumber.text?.count != 0 && txtStreet.text?.count != 0 && txtArea.text?.count != 0 && txtTaluka.text?.count != 0 && txtCity.text?.count != 0 && txtPinCode.text?.count != 0 && txtAadharNumber.text?.count != 0 && txtPassword.text?.count != 0  && txtCPassword.text?.count != 0{
//    buttonEnable_OnView(tag: 1, btn: btnDone)
//    }else{
//    buttonEnable_OnView(tag: 2, btn: btnDone)
//        }
//
//    }
    // MARK: - --------------API Calling
    // MARK: -
    
    
    
    
    func call_URL_Save_Profile_API(sender : UIButton)  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("set_profile", forKey: "request") //1
            dictData.setValue(txtFullName.text!, forKey: "full_name") //2
            dictData.setValue(txtFatherName.text!, forKey: "father_or_husband_name") //3
            dictData.setValue(txtMobileNumber.text!, forKey: "mobile_number") //4
            dictData.setValue(txtFlatNumber.text!, forKey: "house_number")//5
            dictData.setValue(txtStreet.text!, forKey: "street")//6
            dictData.setValue("\(txtSocietyname.tag)", forKey: "society_or_appartment")//7
            dictData.setValue(txtArea.text!, forKey: "area")//8
            dictData.setValue("\(txtTaluka.tag)", forKey: "taluka")//9
            dictData.setValue("\(txtCity.tag)", forKey: "district")//10
            dictData.setValue(txtPinCode.text!, forKey: "pincode")//11
            dictData.setValue(txtPanNumber.text!, forKey: "pen_number")//12
            dictData.setValue(txtAadharNumber.text!, forKey: "aadhar_number")//13
            dictData.setValue(txtEmailAddress.text!, forKey: "email")//14
            dictData.setValue(txtPassword.text!, forKey: "password")//15
            dictData.setValue(strApplicationID, forKey: "application_id")//16
            dictData.setValue(txtWhatsappNumber.text!, forKey: "whatsapp_number")//17
            dictData.setValue(txtLendLineNumber.text!, forKey: "landline_number")//18
            dictData.setValue(txtSocietyOthername.text!, forKey: "society_other")//19
            print(dictData)
            //CNG/PNG
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callAPIWithImage(parameter: dictData, url: URL_Save_Profile, image: self.imgProfile.image!, fileName: "profile.jpg", withName: "profile_image_sys") { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    if (responce.value(forKey: "error")as! String == "1"){
                        FTIndicator.showNotification(withTitle: alertInfo, message: (responce.value(forKey: "data")as! String))
 
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "UploadDocumentVC")as! UploadDocumentVC
                            self.navigationController?.pushViewController(testController, animated: true)
                        }
                        
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(responce.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_DropDownList_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("drop_down_list", forKey: "request")
            print(URL_Get_drop_down_list)
            dotLoader(sender: UIButton() , controller : self, viewContain: UIView(), onView: "MainView")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_Get_drop_down_list) { (responce, status) in
                remove_dotLoader(controller: self)
                
                if (status == "success"){
                    
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        
                        deleteAllRecords(strEntity:"DropDownData")
                        saveDataInLocalArray(strEntity: "DropDownData", strKey: "dropdata", data: (dict.value(forKey: "MyTreeList")as! NSArray).mutableCopy() as! NSMutableArray)
                        
                    }else{
                        
                        // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                    //  self.viewContain.isHidden = false
                }else{
                    //  FTIndicator.showToastMessage(alertSomeError)
                }
            }
        }
    }
    // MARK: - --------------SetDataOnView
    // MARK: -
    func setDataOnView() {
        buttonEnable_OnView(tag: 1, btn: btnDone)

        var arylistofData = NSMutableArray()
        arylistofData = self.getProfileDataFromLocal()
        
        print(arylistofData)
        if arylistofData.count != 0 {
            dictDropListData = arylistofData.object(at: 0)as! NSDictionary
        }else{
            self.call_DropDownList_API()
        }
        
        if nsud.value(forKey: "GNGPL_Registration") != nil{
            let dict = nsud.value(forKey:"GNGPL_Registration")as! NSDictionary
            print(dict)
            if(dict.count != 0){
                txtFullName.text = "\(dict.value(forKey: "full_name")!)"
                txtFatherName.text =  ""
                txtMobileNumber.text = "\(dict.value(forKey: "mobile_number")!)"
                txtLendLineNumber.text = "\(dict.value(forKey: "landline_number")!)"
                txtWhatsappNumber.text = "\(dict.value(forKey: "whatsapp_number")!)"
                
                txtFlatNumber.text = "\(dict.value(forKey: "house_number")!)"
                txtStreet.text = "\(dict.value(forKey: "street")!)"
                
                txtSocietyname.text = "\(dict.value(forKey: "society_or_appartment_name")!)"
                txtSocietyOthername.text = "\(dict.value(forKey: "society_other")!)"
                if(("\(dict.value(forKey: "society_or_appartment")!)") == ""){
                    txtSocietyname.tag = 0
                }else{
                    txtSocietyname.tag =  Int("\(dict.value(forKey: "society_or_appartment")!)")!
                }
                
                
                
                txtTaluka.text = "\(dict.value(forKey: "taluka_name")!)"
                if(("\(dict.value(forKey: "taluka")!)") == ""){
                    txtTaluka.tag = 0
                }else{
                    txtTaluka.tag =  Int("\(dict.value(forKey: "taluka")!)")!
                }
                
                txtCity.text = "\(dict.value(forKey: "district_name")!)"
                if(("\(dict.value(forKey: "district")!)") == ""){
                    txtCity.tag = 0
                }else{
                    txtCity.tag =  Int("\(dict.value(forKey: "district")!)")!
                }
                
                
                
                txtSocietyOthername.isUserInteractionEnabled = false
                txtPinCode.text = "\(dict.value(forKey: "pincode")!)"
                txtPanNumber.text = ""
                txtAadharNumber.text = ""
                txtEmailAddress.text = ""
                strApplicationID = "\(dict.value(forKey: "application_id")!)"
                txtFatherName.text = ""
                txtPassword.text = ""
                txtCPassword.text = ""
                txtArea.text = ""

                
                // For User Intersection Close
                if(txtFullName.text?.count != 0){
                    txtFullName.isUserInteractionEnabled = false
                }
                if(txtMobileNumber.text!.count != 0){
                    txtMobileNumber.isUserInteractionEnabled = false
                }
                if(txtSocietyname.text!.count != 0){
                    txtSocietyname.isUserInteractionEnabled = false
                    btnSelectSocity.isUserInteractionEnabled = false
                }
                if(txtFlatNumber.text!.count != 0){
                    txtFlatNumber.isUserInteractionEnabled = false
                }
                if(txtStreet.text!.count != 0){
                    txtStreet.isUserInteractionEnabled = false
                }
                if(txtArea.text!.count != 0){
                    txtArea.isUserInteractionEnabled = false
                }
                if(txtTaluka.text!.count != 0){
                    txtTaluka.isUserInteractionEnabled = false
                    btnTaluka.isUserInteractionEnabled = false
                }
                if(txtCity.text!.count != 0){
                    txtCity.isUserInteractionEnabled = false
                    btnDistrict.isUserInteractionEnabled = false
                }
                if(txtPinCode.text!.count != 0){
                    txtPinCode.isUserInteractionEnabled = false
                }
                
                // self.imgProfile.image = UIImage(named: "info_1")
            }
        }
    }
    // MARK: - --------------Local Data Base
    // MARK: -
    func getProfileDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "DropDownData", strkey: "dropdata")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "dropdata") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
}

// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -

extension SetProfileVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.originalImage] as? UIImage {
            self.imgProfile.contentMode = .scaleAspectFill
            self.imgProfile.image = image
            self.strProfileImageName = getUniqueString()
            self.dismiss(animated: false) {
            }
        } else {
            self.dismiss(animated: false) {
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  false, completion: nil)
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension SetProfileVC : UITextFieldDelegate{

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFullName  {
            return  txtFiledValidation(textField: txtFullName, string: string, returnOnly: "CHAR", limitValue: 50)
        }
        if textField == txtFatherName  {
            return  txtFiledValidation(textField: txtFatherName, string: string, returnOnly: "ALL", limitValue: 50)
        }
        if textField == txtMobileNumber  {
            return  txtFiledValidation(textField: txtMobileNumber, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        if textField == txtWhatsappNumber  {
            return  txtFiledValidation(textField: txtWhatsappNumber, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        if textField == txtLendLineNumber  {
            return  txtFiledValidation(textField: txtLendLineNumber, string: string, returnOnly: "NUMBER", limitValue: 11)
        }
        if textField == txtFlatNumber  {
            return  txtFiledValidation(textField: txtFlatNumber, string: string, returnOnly: "ALL", limitValue: 5)
        }
        if textField == txtStreet  {
            return  txtFiledValidation(textField: txtStreet, string: string, returnOnly: "ALL", limitValue: 50)
        }
        if textField == txtArea  {
            return  txtFiledValidation(textField: txtArea, string: string, returnOnly: "ALL", limitValue: 50)
        }
        if textField == txtPinCode  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 5)
        }
        if textField == txtPanNumber  {
            return  panCardValidation(textField1: txtPanNumber, textField2: UITextField(), string: string, type: "1")
        }
        if textField == txtAadharNumber  {
            return  txtFiledValidation(textField: txtAadharNumber, string: string, returnOnly: "NUMBER", limitValue: 11)
        }
        if textField == txtPassword ||  textField == txtCPassword {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 11)
        }
     
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
