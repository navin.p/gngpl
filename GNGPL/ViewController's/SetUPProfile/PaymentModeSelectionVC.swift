//
//  PaymentModeSelectionVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 6/5/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class PaymentModeSelectionVC: UIViewController {
    
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblAmount: UILabel!

    var indexForSelection = 0
    var ary_TblListData = NSMutableArray()
    var dictPaymentData = NSMutableDictionary()
    var strComeFrom = ""
    
    // MARK: - -------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        print(dictPaymentData)
        if("\(dict_Login_Data.value(forKey: "bp_number")!)" == BPNumberStatic){
            ary_TblListData = [["id":"2","title":"By Cheque","subtitle":"Please carry cheque details."]]
        }else{
        ary_TblListData = [["id":"1","title":"Online Payment","subtitle":"Pay using Debit/Credit Card , Internet Banking , BHIM UPI , Wallets etc."],["id":"2","title":"By Cheque","subtitle":"Please carry cheque details."]]
        }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        buttonEnable_OnView(tag: 1, btn: btnSubmit)
        tvlist.tableFooterView = UIView()
        lblAmount.text = "\u{20B9} \(dictPaymentData["total_amount"]!)"
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actiononContinue(_ sender: UIButton) {
        let dict = ary_TblListData[indexForSelection]as! NSDictionary
        dictPaymentData.setValue("\(dict["id"]as! String)", forKey: "payment_mode")
        dictPaymentData.setValue("\(dict_Login_Data.value(forKey: "bp_number")!)", forKey: "bp_number")
        // For Online
        if("\(dict["id"]as! String)" == "1"){
            if(self.strComeFrom == "BalanceSecurityDeposite"){
                call_OnlinePayMentForBalanceSecurity_API(sender: sender)
            }else if(self.strComeFrom == "MakePaymentScreen") {
                call_SaveMakePayment_ON_Server_API(sender: sender)
            }else{
                call_OnlinePayMentBill_API(sender: sender)
            }
        }
       // For Cheque
        else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ChequeInfoScreenVC")as! ChequeInfoScreenVC
            testController.dictPaymentData = dictPaymentData
            testController.strComeFrom = self.strComeFrom
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    
    
    // MARK: - ---------------API's Calling
    // MARK: -
    
    func call_SaveMakePayment_ON_Server_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{

            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callAPIBYPOST(parameter: dictPaymentData, url: URL_SavePayment) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "PayOnlineVC")as! PayOnlineVC
                        testController.strViewComeFrom = self.strComeFrom
                        testController.strUrl = "\(dict.value(forKey: "data")!)"
                        self.navigationController?.pushViewController(testController, animated: true)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    
    func call_OnlinePayMentBill_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            print(dictPaymentData)
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            
            WebService.callAPIBYPOST(parameter: dictPaymentData, url: URL_Pay_monthly_bill) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "PayOnlineVC")as! PayOnlineVC
                        testController.strViewComeFrom = self.strComeFrom
                        testController.strUrl = "\(dict.value(forKey: "data")!)"
                        self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_OnlinePayMentForBalanceSecurity_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            print(dictPaymentData)
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            
            WebService.callAPIBYPOST(parameter: dictPaymentData, url: URL_Pay_balance_security_deposit) { (responce, status) in
                remove_dotLoader(controller: self)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "PayOnlineVC")as! PayOnlineVC
                        testController.strViewComeFrom = self.strComeFrom
                        testController.strUrl = "\(dict.value(forKey: "data")!)"
                        self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "data")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PaymentModeSelectionVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "MakePaymentSelectionCell", for: indexPath as IndexPath) as! MakePaymentSelectionCell
        let dict = ary_TblListData[indexPath.row]as! NSDictionary
        cell.lblTitle.text = (dict["title"]as! String)
        cell.lblSubTitle.text = (dict["subtitle"]as! String)
        if(indexPath.row == indexForSelection){
            cell.btnCheckBox.setImage(UIImage(named: "radio_1"), for: .normal)
            //cell.alpha = 0.8
        }else{
            cell.btnCheckBox.setImage(UIImage(named: "radio_2"), for: .normal)
           // cell.alpha = 1.0
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexForSelection = indexPath.row
        self.tvlist.reloadData()
    }
    
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
//    }
    
}
// MARK: - ----------------MakePaymentSelectionCell
// MARK: -
class MakePaymentSelectionCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
