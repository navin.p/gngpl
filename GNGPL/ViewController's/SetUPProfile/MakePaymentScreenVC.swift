//
//  MakePaymentScreenVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/26/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData

class MakePaymentScreenVC: UIViewController {

    var ary_TblListData = NSMutableArray()
    var indexForSelection = 0
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
         tvlist.tableFooterView = UIView()
        
        if(getPaymentDataFromLocal().count != 0){
            print(getPaymentDataFromLocal())
            
            self.ary_TblListData = NSMutableArray()
            
            let aryTemp = ((getPaymentDataFromLocal().object(at: 0) as AnyObject).value(forKey: "payment_data_arr") as! NSArray).mutableCopy() as! NSMutableArray
            
            for item in aryTemp{
                if("\((item as AnyObject).value(forKey: "view_status")!)" == "1"){
                     self.ary_TblListData.add(item)
                }
            }
           
            self.tvlist.reloadData()

        }else{
            self.call_MakePaymentData_API()
        }
        
        buttonEnable_OnView(tag: 1, btn: btnSubmit)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - --------------IBAction
    // MARK: -
  
    
    @IBAction func actionOnContinue(_ sender: UIButton) {
        if ary_TblListData.count != 0 {
            let dictData = NSMutableDictionary()
            let dictPaymentData = ary_TblListData.object(at: indexForSelection)as! NSDictionary
            if(dictPaymentData.count != 0){
                if dict_Login_Data.count != 0{
                    print(dict_Login_Data)
                    if(dict_Login_Data.count != 0){
                        dictData.setValue("connection_payment", forKey: "request")
                        dictData.setValue("\(dictPaymentData.value(forKey: "id")!)", forKey: "connection_payment_type")
                        dictData.setValue("\(dict_Login_Data.value(forKey: "application_id")!)", forKey: "application_id")
                        dictData.setValue("\(dictPaymentData.value(forKey: "amount")!)", forKey: "total_amount")
                       // dictData.setValue("1", forKey: "total_amount")
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentModeSelectionVC")as! PaymentModeSelectionVC
                        testController.strComeFrom = "MakePaymentScreen"
                        testController.dictPaymentData = dictData
                        self.navigationController?.pushViewController(testController, animated: true)
                    }
                }
            }
          
          //  call_SavePayment_ON_Server_API(sender: sender)
        }
    }

    
    @IBAction func actiononBack(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
        
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_MakePaymentData_API()  {
        if !(isInternetAvailable()){
            self.showErrorWithImage( strTitle: alertInternet, imgError: strInternetImage)

        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue("dashboard_data", forKey: "request")
            //     self.activityIndicator.isHidden = false
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callAPIBYPOST(parameter: dictData, url: URL_Dashboard) { (responce, status) in
                print(responce)
                //self.activityIndicator.isHidden = true
                remove_dotLoader(controller: self)
                removeErrorView()

                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "error")as! String == "1"){
                        self.ary_TblListData = NSMutableArray()
                        let aryTemp = ((dict.value(forKey: "data")as! NSDictionary).value(forKey: "payment_data_arr") as! NSArray).mutableCopy() as! NSMutableArray
                        for item in aryTemp{
                            if("\((item as AnyObject).value(forKey: "view_status")!)" == "1"){
                                self.ary_TblListData.add(item)
                            }
                        }
                        self.tvlist.reloadData()
                    }else{
                        self.showErrorWithImage( strTitle: "\(responce.value(forKey: "data")as! String)", imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage( strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    
    

    
    // MARK: - --------------Local Data Base
    // MARK: -
    func getPaymentDataFromLocal()-> NSMutableArray   {
        let aryTemp = getDataFromLocal(strEntity: "Dashboard", strkey: "dashboard")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "dashboard") ?? 0)
            }
        }
        if aryList.count !=  0{
            return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
            
        }else{
            return NSMutableArray()
        }
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MakePaymentScreenVC : UITableViewDelegate, UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ary_TblListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "MakePayment", for: indexPath as IndexPath) as! MakePaymentCell
        let dict = ary_TblListData[indexPath.row]as! NSDictionary
        
        
        cell.lblTitle.text = (dict["title"]as! String).replacingOccurrences(of: "/n", with: "\n")
        cell.lblTitle.text = cell.lblTitle.text?.replacingOccurrences(of: "\n", with: "\n")
        cell.lblSubTitle.text = (dict["description"]as! String).replacingOccurrences(of: "/n", with: "\n")
        cell.lblSubTitle.text = cell.lblSubTitle.text?.replacingOccurrences(of: "\n", with: "\n")

        cell.lblRS.text = "\u{20B9} \(dict["amount"]!)"
        if(indexPath.row == indexForSelection){
            cell.btnCheckBox.setImage(UIImage(named: "radio_1"), for: .normal)
        }else{
        cell.btnCheckBox.setImage(UIImage(named: "radio_2"), for: .normal)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         indexForSelection = indexPath.row
        self.tvlist.reloadData()
    }

    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 0.5) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
//    }
    
}
// MARK: - ----------------MakePaymentCell
// MARK: -
class MakePaymentCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblRS: UILabel!
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
