//
//  ThankyouVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/25/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class ThankyouVC: UIViewController {
    
    @IBOutlet weak var lblApplicationNumber: UILabel!
    @IBOutlet weak var view_Congratulation: UIView!
    @IBOutlet weak var lbl_CongratulationMsg: UILabel!
    @IBOutlet weak var lbl_SubMsg: UILabel!
    @IBOutlet weak var img_Congratulation: UIImageView!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var imgGift: UIImageView!

    var dictDataForShowMessage = NSMutableDictionary()
    var strTitle = String()
    
    //MARK:
    //MARK: ---------------LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        buttonEnable_OnView(tag: 1, btn: btnContinue)
        lblTitle.text = strTitle
        if(strTitle == "Thank You"){
            view_Congratulation.isHidden = true
            let dictData  = (dictDataForShowMessage.value(forKey: "data")as! NSDictionary)
            lblMessage.text = (dictDataForShowMessage.value(forKey: "msg") as! String)
            lblApplicationNumber.text = "Application Number - \(dictData.value(forKey: "application_id")!)"
        }else if(strTitle == "Congratulations"){
            view_Congratulation.isHidden = false
            lbl_CongratulationMsg.text = "\((dictDataForShowMessage.value(forKey: "data") as! String)) \n\n\((dictDataForShowMessage.value(forKey: "middel_msg") as! String))"
            lbl_SubMsg.text = (dictDataForShowMessage.value(forKey: "bottom_msg") as! String)
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imgGift.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.imgGift.transform = .identity
            },
                       completion: nil)

    }
    
 
    //MARK:
    //MARK: ---------------IBAction
  
    @IBAction func actionOnContinue(_ sender: UIButton) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
}
