//
//  UploadDocumentVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/13/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class UploadDocumentVC: UIViewController {

    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    
    var imagePicker = UIImagePickerController()
    var aryListData = NSMutableArray()
    var tagForSelection = 0
    // MARK: - --------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 100.0
        aryListData  = [[ "title":"Identity Proof","Subtitle":"(Either PAN Card / Passport / Voter Id/ Driving License / Ration Card / Aadhar Card)","image":"address_prof","type":"Default"],[ "title":"Ownership of Establishment Proof","Subtitle":"(Copy of Sale Deed Agreement / Water Bill / Electricity Bill / Society Certificate etc)","image":"id_prof","type":"Default"],[ "title":"Other Documents","image":"other","type":"Default","Subtitle":"NOC from lawful owner in pescribed format in case of rented premises"]]
        buttonEnable_OnView(tag: 1, btn: btnSubmit)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if( validationForImage() == 0) {
            self.reloadWithAnimation()
        }else{
            tvlist.reloadData()
        }
    }
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: RegisterVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func actionOnDone(_ sender: Any) {
        if( validationForImage() == 0) || (validationForImage() == 1) {
            if(validationForImage() == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectAddressDocument, viewcontrol: self)
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_ID_proof, viewcontrol: self)
            }
        }
        else{
           call_UploadDocumentAPI()
        }
    }
    
    func previewImage(image : UIImage)  {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
        testController.img = image
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    // MARK: - --------------API Calling
    // MARK: -
    func call_UploadDocumentAPI()  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            
            let imgAddress =  (aryListData.object(at: 0)as! NSDictionary).value(forKey: "type")as! String
            let imgID =  (aryListData.object(at: 1)as! NSDictionary).value(forKey: "type")as! String
            let imgOther =  (aryListData.object(at: 2)as! NSDictionary).value(forKey: "type")as! String

            let aryImages = NSMutableArray()
          
            if (imgAddress == "Default") || (imgID == "Default")  {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectAddressDocument, viewcontrol: self)
            }else{
                if(imgAddress != "Default"){
                    let dict1 = NSMutableDictionary()
                    dict1.setValue("Address\(getUniqueString()).jpg", forKey: "imageName")
                    dict1.setValue("address_proof_syspath", forKey: "fileName")
                    dict1.setValue((aryListData.object(at: 0)as! NSDictionary).value(forKey: "image")as! UIImage, forKey: "image")
                    aryImages.add(dict1)
                    
                }
                if (imgID != "Default"){
                    let dict1 = NSMutableDictionary()
                    dict1.setValue("IDProof\(getUniqueString()).jpg", forKey: "imageName")
                    dict1.setValue("id_proof_syspath", forKey: "fileName")
                    dict1.setValue((aryListData.object(at: 1)as! NSDictionary).value(forKey: "image")as! UIImage, forKey: "image")
                    aryImages.add(dict1)
                }
                if (imgOther != "Default"){
                    let dict1 = NSMutableDictionary()
                    dict1.setValue("Other\(getUniqueString()).jpg", forKey: "imageName")
                    dict1.setValue("other_doc_syspath", forKey: "fileName")
                    dict1.setValue((aryListData.object(at: 2)as! NSDictionary).value(forKey: "image")as! UIImage, forKey: "image")
                    aryImages.add(dict1)
                }
           
                dotLoader(sender: btnSubmit , controller : self, viewContain: self.view, onView: "Button")

         
                let dictData = NSMutableDictionary()
                dictData.setValue("upload_document", forKey: "request")
                if nsud.value(forKey: "GNGPL_Registration") != nil{
                    let dict = nsud.value(forKey:"GNGPL_Registration")as! NSDictionary
                    print(dict)
                    if(dict.count != 0){
                        dictData.setValue("\(dict.value(forKey: "application_id")!)", forKey: "application_id")
                    }
                }
                WebService.callAPIWithMultiImage(parameter: dictData, url: URL_uploadDocument, imageArray: aryImages) { (responce, status) in
                    remove_dotLoader(controller: self)
                    if (status == "success"){
                        if (responce.value(forKey: "error")as! String == "1"){
                            FTIndicator.showNotification(withTitle: alertMessage, message: (responce.value(forKey: "data")as! String))
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                
                                let testController = mainStoryboard.instantiateViewController(withIdentifier: "ThankyouVC")as! ThankyouVC
                                testController.strTitle = "Congratulations"
                                testController.dictDataForShowMessage = responce.mutableCopy() as! NSMutableDictionary
                                self.navigationController?.pushViewController(testController, animated: true)
                                
                            }
                            
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(responce.value(forKey: "data")as! String)", viewcontrol: self)
                        }
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                
            }
        }
    }
//    func buttonEnable()  {
//
//        if( validationForImage() != 0) || (validationForImage() != 1) {
//            btnSubmit.isHidden = false
//            buttonEnable_OnView(tag: 1, btn: btnSubmit)
//        }else{
//            buttonEnable_OnView(tag: 2, btn: btnSubmit)
//        }
//
//    }
}
// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -

extension UploadDocumentVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.originalImage] as? UIImage {
            let dict = (aryListData[tagForSelection]as! NSDictionary).mutableCopy() as! NSMutableDictionary
            dict.setValue(image, forKey: "image")
            dict.setValue("System", forKey: "type")
             aryListData.replaceObject(at: tagForSelection, with: dict)
            self.tvlist.reloadData()
            self.dismiss(animated: false) {
            }
        } else {
            self.dismiss(animated: false) {
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  false, completion: nil)
    }
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension UploadDocumentVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "UploadData", for: indexPath as IndexPath) as! UploadDataCell
        let dict = aryListData[indexPath.row]as! NSDictionary
        cell.lblTitle.text = (dict["title"]as! String)
        cell.lblSubTitle.text = (dict["Subtitle"]as! String)
        if((dict["type"]as! String) == "Default"){
            cell.imgView?.image  = UIImage(named:(dict["image"]as! String))

        }else{
            cell.imgView?.image  = (dict["image"]as! UIImage)
        }

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tagForSelection = indexPath.row
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            if(UIImagePickerController .isSourceTypeAvailable(.camera)){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.present(self.imagePicker, animated: true, completion: nil)
            } else {
                //   "You don't have camera"
            }
        }))
        let dict = aryListData[indexPath.row]as! NSDictionary
        if((dict["type"]as! String) == "System"){
            alert.addAction(UIAlertAction(title: Alert_Preview, style: .default, handler:{ (UIAlertAction)in
                self.previewImage(image:  (dict["image"]as! UIImage))
            }))
        }
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: {
        })
    }
    func reloadWithAnimation() {
        tvlist.reloadData()
        let tableViewHeight = tvlist.bounds.size.height
        let cells = tvlist.visibleCells
        
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.5, delay: 0.08 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
    func validationForImage() -> Int{
        var countValue = 0
        for (_, item) in aryListData.enumerated() {
            let strtype = (item as AnyObject).value(forKey: "type")as! String
            if(strtype == "System"){
                countValue = countValue + 1
            }
        }
        return countValue
    }
}

// MARK: - ----------------Upload Document Cell
// MARK: -
class UploadDataCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!

    @IBOutlet weak var imgView: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
